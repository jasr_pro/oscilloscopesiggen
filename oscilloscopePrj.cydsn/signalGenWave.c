/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
 
#include <stdio.h>
#include "psocUtilities.h"
#include "signalGenWave.h"

#define SIGGEN_WAVEFORM_NO_SINE_FULLRANGE

uint32_t SigGenWaveGetFreqMaxHzForNumSamplesNoSquare(uint16_t numSamplesPerCycle);

/* Reference data for SINE wave form (0º to 360º - 128 samples - 0 to 255 
*/

const tSigGenData sineWaveRef[] = {
   128, 131, 134, 137, 140, 143, 146, 149, 152, 155, 158, 162, 165, 167, 170, 173,
   176, 179, 182, 185, 188, 190, 193, 196, 198, 201, 203, 206, 208, 211, 213, 215,
   218, 220, 222, 224, 226, 228, 230, 232, 234, 235, 237, 238, 240, 241, 243, 244,    
   245, 246, 248, 249, 250, 250, 251, 252, 253, 253, 254, 254, 254, 255, 255, 255,
   255, 255, 255, 255, 254, 254, 254, 253, 253, 252, 251, 250, 250, 249, 248, 246,
   245, 244, 243, 241, 240, 238, 237, 235, 234, 232, 230, 228, 226, 224, 222, 220,
   218, 215, 213, 211, 208, 206, 203, 201, 198, 196, 193, 190, 188, 185, 182, 179,
   176, 173, 170, 167, 165, 162, 158, 155, 152, 149, 146, 143, 140, 137, 134, 131,
   128, 124, 121, 118, 115, 112, 109, 106, 103, 100,  97,  93,  90,  88,  85,  82,
    79,  76,  73,  70,  67,  65,  62,  59,  57,  54,  52,  49,  47,  44,  42,  40,
    37,  35,  33,  31,  29,  27,  25,  23,  21,  20,  18,  17,  15,  14,  12,  11,
    10,   9,   7,   6,   5,   5,   4,   3,   2,   2,   1,   1,   1,   0,   0,   0,
     0,   0,   0,   0,   1,   1,   1,   2,   2,   3,   4,   5,   5,   6,   7,   9,
    10,  11,  12,  14,  15,  17,  18,  20,  21,  23,  25,  27,  29,  31,  33,  35,
    37,  40,  42,  44,  47,  49,  52,  54,  57,  59,  62,  65,  67,  70,  73,  76,
    79,  82,  85,  88,  90,  93,  97, 100, 103, 106, 109, 112, 115, 118, 121, 124
};



/* Determine the number of samples that best suits for the freq */
int8_t getWaveSamplesForSignal( tSigGenWaveType  sigType, uint32_t freqHz, tsigWaveInfo *signalInfo, tSigGenData *sigVec)
{
   int8_t retVal = OK_RESULT;
   uint16_t numSamples = 0;
   uint64_t temp64;
    
   if ( sigType == SIG_GEN_WAVE_SQUARE_DIGITAL)
   {
      if (  freqHz <= MAX_FREQ_HZ_SUPPORTED_DIG_SQUARE)
      {
        signalInfo->numSamples = 0; /* Don't care */
        signalInfo->sampleTimeNs = NUM_NS_PER_SECOND/(2U*freqHz);
      }
      else
      {
        retVal = ERROR_RESULT;
        signalInfo->numSamples = 0;
        signalInfo->sampleTimeNs = WAVE_SIGGEN_MIN_SAMPLE_TIME_NS;
        
      }
   }
   else if  (sigType == SIG_GEN_WAVE_SQUARE)
   {
      /* In square wave the max frequency will be MAX_GEN_SAMPLE_PER_SEC/2 */
      if (  freqHz <= MAX_FREQ_HZ_SUPPORTED_NO_SQUARE ) /*Analog Square waveform uses same limits that other analog signals*/
      {
        signalInfo->numSamples = 2U;
        signalInfo->sampleTimeNs = NUM_NS_PER_SECOND/(2U*freqHz);
        #ifdef SIGGEN_WAVEFORM_NO_SINE_FULLRANGE
        sigVec[0] = 0; /* from 0-255 (neg/pos)*/
        #else
        sigVec[0] = HALF_SIG_VAL; /* half range (pos)*/
        #endif
        sigVec[1] = MAX_SIG_VAL;
        
      }
      else
      {
         retVal = ERROR_RESULT;         
      }
   }
   else
   {
      /*For the rest of the waveforms types, the number of samples will be calculated
        on base of the frequency desired, limiting the maximum frequency allowed         
      */
      if (freqHz > MAX_FREQ_HZ_SUPPORTED_NO_SQUARE )
      {    
        /* Limiting this to the maximum frequency but return error */
        retVal = ERROR_RESULT;
        numSamples = MIN_NUM_SAMPLES;
        signalInfo->numSamples = 0;
        signalInfo->sampleTimeNs = WAVE_SIGGEN_MIN_SAMPLE_TIME_NS;
      }
      else
      {
        /* Determine the most appropiate number of samples for the frequency desired */
        numSamples = MIN_NUM_SAMPLES;
        while (( numSamples != MAX_NUM_SAMPLES) && 
               ( SigGenWaveGetFreqMaxHzForNumSamplesNoSquare(numSamples*2) > freqHz)){
            numSamples = numSamples * 2;            
        }
        /* At this point if using the minimum sample time and 
           the number calculated of samples we will be at a 
           frequency above the desired frequency */
          signalInfo->numSamples = numSamples;
          temp64 =  (uint64_t)NUM_NS_PER_SECOND / (freqHz*numSamples);
          signalInfo->sampleTimeNs = (uint32_t)temp64;
      }
          
      if (retVal == OK_RESULT)
      {
        /* Fill the signal vector with data according to the waveform selected */ 
        retVal = fillSignalBufferFromNumSamples( sigType, numSamples, sigVec);
      }
       
    
   }
    
   return retVal;
}

/* Fill the signal vector with data according to the waveform indicated and the number of samples
   Only Valid wave forms and the number of samples is a power of 2 number in the range of
   [MIN_NUM_SAMPLES - MAX_NUM_SAMPLES]. If is SIG_GEN_WAVE_SQUARE valid are from 2 - MAX_NUM_SAMPLES.
*/
int8_t fillSignalBufferFromNumSamples( tSigGenWaveType  sigType, uint16_t numSamples, tSigGenData *sigVec)
{
  uint16_t step;
  int8_t retVal = ERROR_RESULT;
  uint16_t numSamplesRef = MAX_NUM_SAMPLES;
  uint16_t idx, iterMax;
  tSigGenData  sigValTemp;
    
  /* Check that the number of samples is valid {[MIN_NUM_SAMPLES, 16, 32, 64, 128, MAX_NUM_SAMPLES]*/  
  while ((retVal == (int8_t)ERROR_RESULT) && (numSamplesRef >= MIN_NUM_SAMPLES))
  {
    if ( numSamples == numSamplesRef )
    {
        retVal = OK_RESULT;        
    }        
    numSamplesRef = numSamplesRef/2;
    
  }
   
  /*Check for Square wave form */
  if ((sigType == SIG_GEN_WAVE_SQUARE) && (retVal == (int8_t)ERROR_RESULT))
  {
     if (((numSamples % 2)== 0) &&
         (numSamples >= 2) && (numSamples <= MAX_NUM_SAMPLES))
     {
       retVal = OK_RESULT;
     }
  }

  /* Fill the buffer now */
  if (retVal == OK_RESULT)
  {
    if ( sigType != SIG_GEN_WAVE_TRIANGLE)
    {
      /*Get the step using the total number of steps */   
      step = (MAX_SIG_VAL)/(numSamples-1);
    }
    else
    {
      /*In the case of the Triangle, the step is calculated using
        the half of the steps as in half of steps it goes up and 
        in the other half it goes down. 
        In this type of signal the last element should match the 
        index 1. This is neded to avoid duplicate the value when
        starting a new cycle.
                x
              x   x
            x       x
          x           x   x
        x               x 
        0 1 2 3 4 5 6 7 0 1
        */
      step = (MAX_SIG_VAL+1)/(numSamples/2);
    }

    switch (sigType)
    {
        
       case SIG_GEN_WAVE_SQUARE:
       {
          for (idx = 0; idx < numSamples/2; idx++)
          {
            #ifdef SIGGEN_WAVEFORM_NO_SINE_FULLRANGE
            sigVec[idx] = 0; /*from 0-255 (neg/pos)*/
            #else    
            sigVec[idx] = HALF_SIG_VAL; /* half range (pos)*/
            #endif
            sigVec[(numSamples/2)+idx] = MAX_SIG_VAL;
          }
          break;
       }
       case SIG_GEN_WAVE_SAW:
       {
          printf("@fillSignalBufferFromNumSamples() - SIG_GEN_WAVE_SAW\r\n");
          for (idx = 0; idx < numSamples; idx++)
          {
            #ifdef SIGGEN_WAVEFORM_NO_SINE_FULLRANGE  
            sigVec[idx]=idx * step;  /*SAW from 0-255 (neg/pos)*/
            #else
            sigVec[idx]=HALF_SIG_VAL+((idx * step)/2);  /*SAW from 128-255 (pos)*/
            #endif
          }
          break;
       }
       case SIG_GEN_WAVE_INV_SAW:
       {
          printf("@fillSignalBufferFromNumSamples() - SIG_GEN_WAVE_INV_SAW\r\n");
          for (idx = 0; idx < numSamples; idx++)
          {
            #ifdef SIGGEN_WAVEFORM_NO_SINE_FULLRANGE
            sigVec[idx]=(numSamples-idx-1) * step;  /* SAW from 0-255 (neg/pos)*/
            #else
            sigVec[idx]=HALF_SIG_VAL+(((numSamples-idx-1) * step)/2); /*SAW from 128-255 (pos)*/
            #endif
          }
          break;
       }
       case SIG_GEN_WAVE_SINE:
       {
         printf("@fillSignalBufferFromNumSamples() - SIG_GEN_WAVE_SINE\r\n");
         for (idx = 0; idx < numSamples; idx++)
         {
           sigVec[idx]= sineWaveRef[idx*step];
         }
         break;
       }
       case SIG_GEN_WAVE_TRIANGLE:
       {
          printf("@fillSignalBufferFromNumSamples() - SIG_GEN_WAVE_TRIANGLE \r\n");
          iterMax = (numSamples/2);
          for (idx = 0; idx < iterMax; idx++)
          {
            #ifdef SIGGEN_WAVEFORM_NO_SINE_FULLRANGE
            sigValTemp = idx*step;  /*Triangle from 0-255 (neg/pos)*/              
            #else
            sigValTemp = HALF_SIG_VAL+((idx * step)/2);  /*Triangle from 128-255 (pos)*/                
            #endif
            sigVec[idx]= sigValTemp;
            if ((idx != 0) && (idx != iterMax))  
            {
              sigVec[numSamples-idx]= sigValTemp;
            }
            sigVec[iterMax]= MAX_SIG_VAL;
          }
          
           
          break;
       }
       default:
       {
         printf("@fillSignalBufferFromNumSamples() - Default\r\n");
         retVal = ERROR_RESULT;
       }
     }
   }
  return retVal;
}

uint32_t SigGenWaveGetFreqMaxHzForNumSamplesNoSquare(uint16_t numSamplesPerCycle)
{
    uint32_t freqMaxHz;
    
    /* Get the maximum frequency allowed for the number of samples 
       asumming that will be used the minimum sample time */
    
    freqMaxHz = WAVE_SIGGEN_MAX_SAMPLE_PER_SEC/numSamplesPerCycle;
    
    return freqMaxHz;
}
  

/* [] END OF FILE */

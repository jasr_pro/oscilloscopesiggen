/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "psocUtilities.h"
#include "lcdMcuFriend9342.h"
#include "lcdMcuFriend9342Wrap.h"
#include "plotWave.h"
#include "projectDisplay.h"
#include "displayFont5x7.h"
#include "stdio.h"

/*Create the box in menu for field is selected. 
  cfgValActive is used to indicate if we entered in the field 
               configuration mode. In this mode we can select 
               the digit of the field to be configured.
  cfgDigitActive is used to indicate that we entered in the field 
                 digit configuration mode. In this mode we can
                 increase/decrease the digit selected.
*/
void createBoxForVal(tMenuFieldSetCfgSt *menuCfgSt, uint8_t fieldNum, bool cfgValActive, bool cfgDigitActive, uint16_t xCord, uint16_t yCord);

const char menuHeader[] = MENU_REC_HEADER_TEXT;
const char menuHeaderCnt[] = MENU_REC_HEADER_NO_FIT_TEXT;
const char menuFieldHeader[] = MENU_FIELD_HEADER_TEXT;
const char menuFieldHeaderCnt[] = MENU_FIELD_HEADER_NO_FIT_TEXT;
const char emptyChar[]=" ";

/*Fill with empty strings using the color and number of chars provided
  This will use the current position in which the character is located.
  The parameter color should be the color of the text background.
*/
void displayFillBlankSpaces(uint8_t numChars, uint16_t color)
{
   uint8_t idx;
   uint16_t otherColor = (color == COLOR_BLACK) ? COLOR_WHITE : COLOR_BLACK; 
    
   for(idx=0; idx <numChars; idx++)
   { 
     displaySetTextColor(otherColor, color);
     displayPrint(emptyChar);
   }
}

/* Initialize display */
uint8_t prjDisplayInit(void)
{
   
   /* Initial state of the pins used by LCD */ 
   lcdWriteDataToPort(0xFF);      
   READ_SET;
   WRITE_SET;
   CS_SET;
   RESET_SET;
   DATA_CMD_SET;
    
   lcdMcuFriend9342Start();
   return 0; 
}



/* This will show a menu with text in a color specified.
   This also will show in a color specified the option
   specified. If there are more options than the ones that
   can fit in the screen (based on the specified text size),
   will show only the ones that fit using as reference the
   option selected. If clear is set, the screen won't be clear
   and the lines won't be cleared either.
     _ Coordenate to start writting text 
    |
    v__________
   | Text      |
   |_Gap_______|

*/
void displayReactangleMenu(tMenuCfgSt *menuCfgSt)
{
    int8_t retVal = OK_RESULT;
    uint16_t sizeCharPixels = Y_PIXELS_NUM_FROM_TEXT_SIZE(menuCfgSt->textSize) + MENU_GAP_BETWEEN_OPTIONS;
    uint8_t numOptionsFit;
    uint8_t startOption;
    uint8_t lastOption;
    uint8_t idx;
    int16_t yCord;
    uint8_t length;
    uint8_t substract;
    
    if ( menuCfgSt->itemSelected >= menuCfgSt->numItems )
    {
       return; 
    }
    
    /* Clear screen */
    if (menuCfgSt->clearFlag)
    {
      fillScreen(menuCfgSt->colorBackGround);
      drawRect(0, 0, LCD_WIDTH, LCD_HEIGHT, menuCfgSt->colorText);
    }
    /* Display the header */
    yCord = MENU_GAP_BETWEEN_OPTIONS_HALF;
    displaySetTextCursor(MENU_XCORD_TO_START, yCord);
    displaySetTextColor(menuCfgSt->colorText, menuCfgSt->colorBackGround);
    displaySetTextSize(menuCfgSt->textSize);
   
    /* Get how many options can fit in a single screen (remove one line for the header) */
    numOptionsFit = (LCD_HEIGHT - sizeCharPixels)/(sizeCharPixels+1);
    
    /* Check if all options fit in one single screen*/    
    if ( menuCfgSt->numItems > numOptionsFit)
    {
       /* No all items in the menu fits in the same window. Getting which ones can fit */       
       if (menuCfgSt->itemSelected < numOptionsFit) /*Item selected is in 1st screen?*/
       {
         startOption = 0;
       }
       else if (menuCfgSt->itemSelected == (menuCfgSt->numItems-1)) /* Last element*/
       {
         startOption = menuCfgSt->itemSelected - numOptionsFit + 1;
       }
       else
       {
          /* Try to keep the selected item centered.
             The start item will be calculated based on the index of the item
             and substracting half of the items that fit it screen. If the number
             of itmes that fit in the screen is odd, the substact another item.
           */
          substract = (numOptionsFit>>1) + (numOptionsFit % 2);
          startOption = menuCfgSt->itemSelected-substract;
       } 
       lastOption = startOption + numOptionsFit - 1;
       if (menuCfgSt->clearFlag)
       {
         displayPrint(menuHeaderCnt);
       }
    }
    else
    {
        /* Options fit in menu screen */
        startOption = 0;
        lastOption = menuCfgSt->numItems-1;
        if (menuCfgSt->clearFlag)
        { 
          displayPrint(menuHeader);
        }
    }
    /* Draw a line below the header text */
    if (menuCfgSt->clearFlag)
    {
      drawFastHLine(1, sizeCharPixels, LCD_WIDTH, menuCfgSt->colorText);
    }
    /* Now showing menu */
    printf("NumoptionsFit: %d startOption:%d last:%d sizeCharPixels: %d\r\n", numOptionsFit, startOption, lastOption,sizeCharPixels);
    for (idx = startOption; idx <= lastOption; idx++)
    {
      if (idx == menuCfgSt->itemSelected)
      {
        displaySetTextColor(menuCfgSt->colorOpSelect, menuCfgSt->colorBackGround);
      }
      else
      {
        displaySetTextColor(menuCfgSt->colorText, menuCfgSt->colorBackGround);
      }
    
      yCord += sizeCharPixels;  /*Y cord is the position in the top of the text*/
      displaySetTextCursor(MENU_XCORD_TO_START, yCord );      
      if (menuCfgSt->clearFlag)
      {
        drawFastHLine(1, yCord+sizeCharPixels-MENU_GAP_BETWEEN_OPTIONS_HALF, LCD_WIDTH, menuCfgSt->colorText);
      }
      displayPrint(menuCfgSt->menuTextIdx[idx].itemText);
    
      /* If the screen was not cleared and there length of the text is 
         smaller than the maximum fill with empty spaces to avoid seeing
         characters from previous text
      */
      length =  (uint8_t)strlen(menuCfgSt->menuTextIdx[idx].itemText);   
      if ((length < menuCfgSt->maxTextLen) && (!(menuCfgSt->clearFlag)))
      {         
         displayFillBlankSpaces(menuCfgSt->maxTextLen-length, menuCfgSt->colorBackGround );  
      }
      
    }  
}

/* Fill with 0s at the begining of the string based on the total number of digits of the field */
void completeStringWithNumDigitsFieldValue( char *str, uint8_t numDigit)
{
   uint8_t len = (uint8_t)strlen(str);
   uint8_t diff;
   uint8_t idx1;
   uint8_t pos;
   
   if ((len < numDigit) && (numDigit <= MAX_NUM_CHARS_FIELD_VAL-1))
   {
     diff = numDigit - len;
     
      for (idx1 = 0; idx1 < len; idx1++)
      {
        pos = len-1-idx1;
        /* For each one of the chars */
        str[pos+diff] = str[pos];
      }
      /* Filling with 0s the beggining of the string */
      for (idx1 = 0; idx1 < diff; idx1++)
      {
         str[idx1] = '0';
      }
      str[numDigit] = '\0';  /*Adding end of string */
  }
}

/*  */
void createBoxForVal(tMenuFieldSetCfgSt *menuCfgSt, uint8_t fieldNum, bool cfgValActive, bool cfgDigitActive, uint16_t xCord, uint16_t yCord)
{
    uint16_t sqrWidth = X_PIXELS_NUM_FROM_TEXT_SIZE(menuCfgSt->textSize) + 4;
    uint16_t sqrHeight = Y_PIXELS_NUM_FROM_TEXT_SIZE(menuCfgSt->textSize) + 6;
    uint16_t realTextSizeAdvance = X_PIXELS_NUM_ADVANCE_CHAR_FROM_TEXT_SIZE(menuCfgSt->textSize);
    uint8_t numDigits;
    uint8_t maxNumDigits;
    uint8_t iter;
    uint16_t color;
    bool fieldSelectedActive;
    uint8_t digitNum;
    
    char  fieldValStr[MAX_NUM_CHARS_FIELD_VAL];
    
    if (menuCfgSt->fieldInfoCfgSt[fieldNum].fieldCfg->hasValue)
    {
      numDigits = menuCfgSt->fieldInfoCfgSt[fieldNum].fieldCfg->numDigits;
      maxNumDigits = menuCfgSt->maxValueLen;
    
      snprintf(fieldValStr, numDigits+1, 
               "%d", menuCfgSt->fieldInfoCfgSt[fieldNum].currentValue); /* value: 160 -> c[0] = '1', c[1] = '6'  c[2]= '0' */
        /*Complete the string with the number of 0s at the begining of the string */
        completeStringWithNumDigitsFieldValue(fieldValStr , numDigits);
    
      fieldSelectedActive = ((fieldNum == menuCfgSt->fieldSelected) && (cfgValActive == true));
    
      /* Clear field box to start over */
      color = menuCfgSt->colorBackGround;
      fillRect(xCord, yCord, LCD_WIDTH-xCord-2, sqrHeight, color);
    
      /* Create field value box */
      if (!fieldSelectedActive)
      {
        color =  (fieldNum == menuCfgSt->fieldSelected) ? menuCfgSt->colorOpSelect : menuCfgSt->colorText;
        drawRect(xCord, yCord, (maxNumDigits*realTextSizeAdvance)+4, sqrHeight, color);
        xCord++;
      }
      for (iter = 0; iter < numDigits; iter++)
      { 
        digitNum = numDigits-iter-1; /* charSelect is the digit selected <..2 1 0>, but string is <0 1 2 ..>
                                        so this will handle the conversion */
        displaySetTextCursor(xCord+2, yCord+3);
        if (fieldSelectedActive)
        {   
          /* If this is the digit selected, the color depends on the digit config state */  
          if (menuCfgSt->fieldInfoCfgSt[fieldNum].charSel == digitNum)
          {            
            color = cfgDigitActive ? menuCfgSt->colorDigitSelect : menuCfgSt->colorOpSelect;
          }
          else
          {
            color = menuCfgSt->colorOpSelect;                         
          }
          displaySetTextColor(color, menuCfgSt->colorBackGround);          
          displayWrite(fieldValStr[iter]);
          if (menuCfgSt->fieldInfoCfgSt[fieldNum].charSel == digitNum)
          {
            if (cfgDigitActive)
            {
              drawRect(xCord, yCord, sqrWidth, sqrHeight, menuCfgSt->colorDigitSelect);
            }
            else
            {
              drawRect(xCord, yCord, sqrWidth, sqrHeight, menuCfgSt->colorOpSelect);  
            }
          }
          xCord += sqrWidth;
        }
        else
        {  /* Config mode is not set so just print value. The color depends on the field slection */
           displaySetTextColor(color, menuCfgSt->colorBackGround); 
           displayWrite(fieldValStr[iter]);
           xCord += realTextSizeAdvance;
        }
        
      }
    }
    
}


/* Field Setup Menu 
   This will have the form of this:
    Field1:
        xxxxxx
    Field2:
        yyyyyy 
*/
void displayFieldSetupMenu(tMenuFieldSetCfgSt *menuCfgSt, bool cfgValActive, bool cfgDigitActive)
{

    int8_t retVal = OK_RESULT;
    uint16_t sizeCharPixels = Y_PIXELS_NUM_FROM_TEXT_SIZE(menuCfgSt->textSize) + MENU_GAP_BETWEEN_OPTIONS;
    uint8_t numOptionsFit;
    uint8_t startOption;
    uint8_t lastOption;
    uint8_t idx;
    uint8_t charIdx;
    int16_t yCord;
    uint8_t length;
    uint8_t substract;
    bool fieldSelected = false;
    char  fieldValStr[MAX_NUM_CHARS_FIELD_VAL];
    
    if ( menuCfgSt->fieldSelected >= menuCfgSt->numItems )
    {
       return; 
    }
    
    /* Clear screen */
    if (menuCfgSt->clearFlag)
    {
      fillScreen(menuCfgSt->colorBackGround);
      drawRect(0, 0, LCD_WIDTH, LCD_HEIGHT, menuCfgSt->colorText);
    }
    /* Display the header */
    yCord = MENU_GAP_BETWEEN_OPTIONS_HALF;
    displaySetTextCursor(MENU_XCORD_TO_START, yCord);
    displaySetTextColor(menuCfgSt->colorText, menuCfgSt->colorBackGround);
    displaySetTextSize(menuCfgSt->textSize);
   
    /* Get how many options can fit in a single screen (remove one line for the header) */
    numOptionsFit = (LCD_HEIGHT - sizeCharPixels)/(sizeCharPixels+1);
    
    /* Check if all options fit in one single screen*/    
    if ( menuCfgSt->numItems > numOptionsFit)
    {
       /* No all items in the menu fits in the same window. Getting which ones can fit */       
       if (menuCfgSt->fieldSelected < numOptionsFit) /*Item selected is in 1st screen?*/
       {
         startOption = 0;
       }
       else if (menuCfgSt->fieldSelected == (menuCfgSt->numItems-1)) /* Last element*/
       {
         startOption = menuCfgSt->fieldSelected - numOptionsFit + 1;
       }
       else
       {
          /* Try to keep the selected item centered.
             The start item will be calculated based on the index of the item
             and substracting half of the items that fit it screen. If the number
             of itmes that fit in the screen is odd, the substact another item.
           */
          substract = (numOptionsFit>>1) + (numOptionsFit % 2);
          startOption = menuCfgSt->fieldSelected-substract;
       } 
       lastOption = startOption + numOptionsFit - 1;
       if (menuCfgSt->clearFlag)
       {
         displayPrint(menuHeaderCnt);
       }
    }
    else
    {
        /* Options fit in menu screen */
        startOption = 0;
        lastOption = menuCfgSt->numItems-1;
        if (menuCfgSt->clearFlag)
        { 
          displayPrint(menuHeader);
        }
    }
    /* Draw a line below the header text */
    if (menuCfgSt->clearFlag)
    {
      drawFastHLine(1, sizeCharPixels, LCD_WIDTH, menuCfgSt->colorText);
    }
    /* Now showing menu */
    printf("NumoptionsFit: %d startOption:%d last:%d sizeCharPixels: %d\r\n", numOptionsFit, startOption, lastOption,sizeCharPixels);
    for (idx = startOption; idx <= lastOption; idx++)
    {
      if (idx == menuCfgSt->fieldSelected)
      {
        displaySetTextColor(menuCfgSt->colorOpSelect, menuCfgSt->colorBackGround);
        fieldSelected = true;
      }
      else
      {
        displaySetTextColor(menuCfgSt->colorText, menuCfgSt->colorBackGround);
        fieldSelected = false;
      }
    
      yCord += sizeCharPixels;  /*Y cord is the position in the top of the text*/
      displaySetTextCursor(MENU_XCORD_TO_START, yCord );      
      if (menuCfgSt->clearFlag)
      {
        drawFastHLine(1, yCord+sizeCharPixels-MENU_GAP_BETWEEN_OPTIONS_HALF, LCD_WIDTH, menuCfgSt->colorText);
      }
     
      displayPrint(menuCfgSt->fieldInfoCfgSt[idx].fieldCfg->itemText);
    
      if (menuCfgSt->fieldInfoCfgSt[idx].fieldCfg->hasValue)
      {
        createBoxForVal(menuCfgSt, idx, cfgValActive, cfgDigitActive, MENU_VAL_FIELD_XCORD_TO_START, yCord-4);
      } 
        
      /* If the screen was not cleared and the length of the text is 
         smaller than the maximum fill with empty spaces to avoid seeing
         characters from previous text
      */
      length =  (uint8_t)strlen(menuCfgSt->fieldInfoCfgSt[idx].fieldCfg->itemText);   
      if ((length < menuCfgSt->maxTextLen) && (!(menuCfgSt->clearFlag)))
      {         
         displayFillBlankSpaces(menuCfgSt->maxTextLen-length, menuCfgSt->colorBackGround );  
      }
      
    } 
}

/*  This function will calculate the final value of the field if this after this
    has been increased or decrecment. This also will limit the digit if the final
    In order to verify that the new field value is inside the valid range it assumes
    that the value is updated starting with the most significative digit.
*/
bool displayGetFieldValueFromIncDecDigit(fieldInfoCfgSt *cfgSt, tFieldDigitIncDec  incDec)
{
  uint32_t currentVal= cfgSt->currentValue;
  uint8_t digSel = cfgSt->charSel;  /* .. 2 1 0*/
  uint32_t newVal;
  uint8_t idx;
  uint8_t digitVal;
  bool updated = false;
    
    
  if ((incDec == FIELD_DIGIT_INC) || (incDec == FIELD_DIGIT_DEC))
  {
     newVal = 1;
     for (idx=0; idx < digSel; idx++ )
     {
       newVal *= 10;
     }
    
     printf("@disp: currentVal: %d digSel: %d newVal: %d\r\n", currentVal, newVal, digSel);
     digitVal = (uint8_t)((currentVal/newVal)%10);
     printf("@disp: digitVal: %d\r\n", digitVal);
     if ( incDec == FIELD_DIGIT_INC)
     {
        /* Increment only if the digit is not the max */  
        if (digitVal != 9)
        {
            newVal += currentVal;
            if ( newVal <= cfgSt->fieldCfg->maxValue )
            {
               cfgSt->currentValue = newVal;
            }
            else
            {  /* If the new value is higher than the maximum
                  then limit this to the maximum value */
                cfgSt->currentValue = cfgSt->fieldCfg->maxValue;               
            }
            updated = true;
        }
        printf("@disp: finalVal(INC): %d\r\n", cfgSt->currentValue);
     }
     else
     {
        /* Decrement only if the digit is not the minimum */
        if (digitVal != 0)
        {
            
            if ( cfgSt->currentValue >= newVal )
            {
               newVal = cfgSt->currentValue - newVal;
            }
            else
            {
                newVal = 0;
            }
            
            if (newVal >= cfgSt->fieldCfg->minValue)
            {
               cfgSt->currentValue = newVal;
            }
            else
            {  /* If the new value is lower than the minimum
                  then limit this to the mainimum value */
                cfgSt->currentValue = cfgSt->fieldCfg->minValue;               
            }
            updated = true;
        } 
         printf("@disp: finalVal(DEC): %d\r\n", cfgSt->currentValue);
     }
  }    
  return updated;  
}




/* -----------  Starting screen ---------------*/
void prjBannerStart()
{
     fillRect(0, 0, LCD_WIDTH, LCD_HEIGHT, COLOR_BLACK);        
     drawRect(0, 0, LCD_WIDTH, LCD_HEIGHT, COLOR_WHITE);    
    
     displaySetTextSize(3); 
     displaySetTextColor(COLOR_YELLOW, COLOR_BLACK);
     displaySetTextCursor(80, 60);
     displayPrint("JASR PRO");
    
     displaySetTextCursor(85, 100);
     displaySetTextSize(2); 
     displayPrint("ELECTRONICS");
    
     displaySetTextCursor(140, 160);     
     displaySetTextSize(1);     
     displayPrint("<LOGO");
    
     displaySetTextCursor(120, 170);
     displayPrint("PLACEHOLDER>");
    
     displaySetTextCursor(135, 200);
     displayPrint("Mexico");
     
    
}



/* [] END OF FILE */

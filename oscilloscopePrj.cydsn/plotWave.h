/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef LCD_PLOT_WAVE_H
            
#define LCD_PLOT_WAVE_H

#include "psocUtilities.h"
#include "adcSampleState.h"
    
#define PLOT_QUEUE_SIZE 1024    

#ifdef PLOT_DATA_SIZE_16
  typedef  uint16_t tPlotData;    
#else
  typedef  uint8_t tPlotData;
#endif

/* Define an array of  PLOT_QUEUE_SIZE elements */
typedef tPlotData  plotWaveBuffer[PLOT_QUEUE_SIZE];

#define PLOT_CHANNEL_0_ONLY   0x1
#define PLOT_CHANNEL_1_ONLY   0x2
#define PLOT_CHANNEL_1_2      0x3


typedef uint8_t tChnlMask;

typedef struct
{
  uint16_t xPlotAreaSt;
  uint16_t yPlotAreaSt;  
  uint16_t plotWidth;
  uint16_t plotHeight;
  uint16_t backPlaneColor;
  uint8_t xGridSize; 
  uint8_t yGridSize;
  uint8_t numChannelsDefault;
  uint16_t gridColor;
  bool linePlot;  
  uint16_t *pWaveColor;
  tAdcSampleStateCfg  defaultAdcSampleStateCfg;
      
} tPlotCfgIn;

typedef struct
{
  uint16_t xPlotAreaSt;
  uint16_t yPlotAreaSt;  
  uint16_t plotWidth;
  uint16_t plotHeight;
  uint16_t backPlaneColor;
  uint8_t xGridSize;  
  uint8_t yGridSize; 
  uint8_t numGridLinesWidth;
  uint8_t numGridLinesHeight;  
  uint16_t firstGridLineWidth;  /* First grid line in X axis */
  uint16_t firstGridLineHeigth; /* First grid line in Y axis */ 
  uint16_t gridColor;
  uint8_t  numChannels;
  uint16_t  numMaxSamples;
    
  bool linePlot;       /* Control if the points to plot are joined with a line or not*/   
  uint16_t *pWaveColor;      
  bool initSuccess;  
} tPlotCfg;
    

typedef struct
{
  uint16_t numData;
  uint16_t rdPtr;
  uint16_t wrPtr;
  uint16_t prevDataSetPtr;
  plotWaveBuffer *pDataBuffer; /* Pointer to an array of PLOT_QUEUE_SIZE elements*/
}tPlotQueue;



uint8_t plotAreaInit( uint16_t xPlotStart, uint16_t yPlotStart, 
                      uint16_t xPlotEnd, uint16_t yPlotEnd, uint16_t backPlaneColor,
                      uint8_t  xGridSize, uint8_t yGridSize, uint16_t gridColor,
                      uint8_t numChannels, uint16_t numMaxSamples, uint16_t numMaxSampleGroups,
                      uint8_t *sampleState, uint8_t **tPlotData, uint16_t channelColor[], 
                      bool linePlot, bool getMemory);

uint8_t drawBackPlane(void);
uint8_t drawGrid(void);
uint8_t plotWaveAddDataToQueue( tPlotData *pData, tChnlMask chnlMask);

/* Return the pointer to the buffer that can be used to pass data */
tPlotData *plotGetDataInPtr(void);
void plotWaveClearDataBuffer(void);
uint8_t plotWaveplotData(uint8_t chnlMask);

typedef bool (*ptrSetSampleSetInvalid)(uint8_t channelNum, uint16_t numSampleSet);
typedef void (*ptrIsSampleSetReadyToPlot)(uint8_t channelNum, uint16_t numSampleSet);

#endif    


/* [] END OF FILE */

/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "psocUtilities.h"
#include "CyLib.h"


void pSOCdelayUS(uint16_t timeUS)
{
    CyDelayUs(timeUS);
    
}

void pSOCdelyMS(uint16_t timeMS)
{
    uint16_t iter;
    for (iter = 0; iter <1000; iter++)
    {
       pSOCdelayUS(timeMS);
    }
}    
/* [] END OF FILE */

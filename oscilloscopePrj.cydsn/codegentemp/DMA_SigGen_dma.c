/***************************************************************************
* File Name: DMA_SigGen_dma.c  
* Version 1.70
*
*  Description:
*   Provides an API for the DMAC component. The API includes functions
*   for the DMA controller, DMA channels and Transfer Descriptors.
*
*
*   Note:
*     This module requires the developer to finish or fill in the auto
*     generated funcions and setup the dma channel and TD's.
*
********************************************************************************
* Copyright 2008-2010, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/
#include <CYLIB.H>
#include <CYDMAC.H>
#include <DMA_SigGen_dma.H>



/****************************************************************************
* 
* The following defines are available in Cyfitter.h
* 
* 
* 
* DMA_SigGen__DRQ_CTL_REG
* 
* 
* DMA_SigGen__DRQ_NUMBER
* 
* Number of TD's used by this channel.
* DMA_SigGen__NUMBEROF_TDS
* 
* Priority of this channel.
* DMA_SigGen__PRIORITY
* 
* True if DMA_SigGen_TERMIN_SEL is used.
* DMA_SigGen__TERMIN_EN
* 
* TERMIN interrupt line to signal terminate.
* DMA_SigGen__TERMIN_SEL
* 
* 
* True if DMA_SigGen_TERMOUT0_SEL is used.
* DMA_SigGen__TERMOUT0_EN
* 
* 
* TERMOUT0 interrupt line to signal completion.
* DMA_SigGen__TERMOUT0_SEL
* 
* 
* True if DMA_SigGen_TERMOUT1_SEL is used.
* DMA_SigGen__TERMOUT1_EN
* 
* 
* TERMOUT1 interrupt line to signal completion.
* DMA_SigGen__TERMOUT1_SEL
* 
****************************************************************************/


/* Zero based index of DMA_SigGen dma channel */
uint8 DMA_SigGen_DmaHandle = DMA_INVALID_CHANNEL;

/*********************************************************************
* Function Name: uint8 DMA_SigGen_DmaInitalize
**********************************************************************
* Summary:
*   Allocates and initialises a channel of the DMAC to be used by the
*   caller.
*
* Parameters:
*   BurstCount.
*       
*       
*   ReqestPerBurst.
*       
*       
*   UpperSrcAddress.
*       
*       
*   UpperDestAddress.
*       
*
* Return:
*   The channel that can be used by the caller for DMA activity.
*   DMA_INVALID_CHANNEL (0xFF) if there are no channels left. 
*
*
*******************************************************************/
uint8 DMA_SigGen_DmaInitialize(uint8 BurstCount, uint8 ReqestPerBurst, uint16 UpperSrcAddress, uint16 UpperDestAddress) 
{

    /* Allocate a DMA channel. */
    DMA_SigGen_DmaHandle = (uint8)DMA_SigGen__DRQ_NUMBER;

    /* Configure the channel. */
    (void)CyDmaChSetConfiguration(DMA_SigGen_DmaHandle,
                                  BurstCount,
                                  ReqestPerBurst,
                                  (uint8)DMA_SigGen__TERMOUT0_SEL,
                                  (uint8)DMA_SigGen__TERMOUT1_SEL,
                                  (uint8)DMA_SigGen__TERMIN_SEL);

    /* Set the extended address for the transfers */
    (void)CyDmaChSetExtendedAddress(DMA_SigGen_DmaHandle, UpperSrcAddress, UpperDestAddress);

    /* Set the priority for this channel */
    (void)CyDmaChPriority(DMA_SigGen_DmaHandle, (uint8)DMA_SigGen__PRIORITY);
    
    return DMA_SigGen_DmaHandle;
}

/*********************************************************************
* Function Name: void DMA_SigGen_DmaRelease
**********************************************************************
* Summary:
*   Frees the channel associated with DMA_SigGen.
*
*
* Parameters:
*   void.
*
*
*
* Return:
*   void.
*
*******************************************************************/
void DMA_SigGen_DmaRelease(void) 
{
    /* Disable the channel */
    (void)CyDmaChDisable(DMA_SigGen_DmaHandle);
}


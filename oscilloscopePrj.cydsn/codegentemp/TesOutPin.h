/*******************************************************************************
* File Name: TesOutPin.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_TesOutPin_H) /* Pins TesOutPin_H */
#define CY_PINS_TesOutPin_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "TesOutPin_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 TesOutPin__PORT == 15 && ((TesOutPin__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    TesOutPin_Write(uint8 value);
void    TesOutPin_SetDriveMode(uint8 mode);
uint8   TesOutPin_ReadDataReg(void);
uint8   TesOutPin_Read(void);
void    TesOutPin_SetInterruptMode(uint16 position, uint16 mode);
uint8   TesOutPin_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the TesOutPin_SetDriveMode() function.
     *  @{
     */
        #define TesOutPin_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define TesOutPin_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define TesOutPin_DM_RES_UP          PIN_DM_RES_UP
        #define TesOutPin_DM_RES_DWN         PIN_DM_RES_DWN
        #define TesOutPin_DM_OD_LO           PIN_DM_OD_LO
        #define TesOutPin_DM_OD_HI           PIN_DM_OD_HI
        #define TesOutPin_DM_STRONG          PIN_DM_STRONG
        #define TesOutPin_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define TesOutPin_MASK               TesOutPin__MASK
#define TesOutPin_SHIFT              TesOutPin__SHIFT
#define TesOutPin_WIDTH              1u

/* Interrupt constants */
#if defined(TesOutPin__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in TesOutPin_SetInterruptMode() function.
     *  @{
     */
        #define TesOutPin_INTR_NONE      (uint16)(0x0000u)
        #define TesOutPin_INTR_RISING    (uint16)(0x0001u)
        #define TesOutPin_INTR_FALLING   (uint16)(0x0002u)
        #define TesOutPin_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define TesOutPin_INTR_MASK      (0x01u) 
#endif /* (TesOutPin__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define TesOutPin_PS                     (* (reg8 *) TesOutPin__PS)
/* Data Register */
#define TesOutPin_DR                     (* (reg8 *) TesOutPin__DR)
/* Port Number */
#define TesOutPin_PRT_NUM                (* (reg8 *) TesOutPin__PRT) 
/* Connect to Analog Globals */                                                  
#define TesOutPin_AG                     (* (reg8 *) TesOutPin__AG)                       
/* Analog MUX bux enable */
#define TesOutPin_AMUX                   (* (reg8 *) TesOutPin__AMUX) 
/* Bidirectional Enable */                                                        
#define TesOutPin_BIE                    (* (reg8 *) TesOutPin__BIE)
/* Bit-mask for Aliased Register Access */
#define TesOutPin_BIT_MASK               (* (reg8 *) TesOutPin__BIT_MASK)
/* Bypass Enable */
#define TesOutPin_BYP                    (* (reg8 *) TesOutPin__BYP)
/* Port wide control signals */                                                   
#define TesOutPin_CTL                    (* (reg8 *) TesOutPin__CTL)
/* Drive Modes */
#define TesOutPin_DM0                    (* (reg8 *) TesOutPin__DM0) 
#define TesOutPin_DM1                    (* (reg8 *) TesOutPin__DM1)
#define TesOutPin_DM2                    (* (reg8 *) TesOutPin__DM2) 
/* Input Buffer Disable Override */
#define TesOutPin_INP_DIS                (* (reg8 *) TesOutPin__INP_DIS)
/* LCD Common or Segment Drive */
#define TesOutPin_LCD_COM_SEG            (* (reg8 *) TesOutPin__LCD_COM_SEG)
/* Enable Segment LCD */
#define TesOutPin_LCD_EN                 (* (reg8 *) TesOutPin__LCD_EN)
/* Slew Rate Control */
#define TesOutPin_SLW                    (* (reg8 *) TesOutPin__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define TesOutPin_PRTDSI__CAPS_SEL       (* (reg8 *) TesOutPin__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define TesOutPin_PRTDSI__DBL_SYNC_IN    (* (reg8 *) TesOutPin__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define TesOutPin_PRTDSI__OE_SEL0        (* (reg8 *) TesOutPin__PRTDSI__OE_SEL0) 
#define TesOutPin_PRTDSI__OE_SEL1        (* (reg8 *) TesOutPin__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define TesOutPin_PRTDSI__OUT_SEL0       (* (reg8 *) TesOutPin__PRTDSI__OUT_SEL0) 
#define TesOutPin_PRTDSI__OUT_SEL1       (* (reg8 *) TesOutPin__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define TesOutPin_PRTDSI__SYNC_OUT       (* (reg8 *) TesOutPin__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(TesOutPin__SIO_CFG)
    #define TesOutPin_SIO_HYST_EN        (* (reg8 *) TesOutPin__SIO_HYST_EN)
    #define TesOutPin_SIO_REG_HIFREQ     (* (reg8 *) TesOutPin__SIO_REG_HIFREQ)
    #define TesOutPin_SIO_CFG            (* (reg8 *) TesOutPin__SIO_CFG)
    #define TesOutPin_SIO_DIFF           (* (reg8 *) TesOutPin__SIO_DIFF)
#endif /* (TesOutPin__SIO_CFG) */

/* Interrupt Registers */
#if defined(TesOutPin__INTSTAT)
    #define TesOutPin_INTSTAT            (* (reg8 *) TesOutPin__INTSTAT)
    #define TesOutPin_SNAP               (* (reg8 *) TesOutPin__SNAP)
    
	#define TesOutPin_0_INTTYPE_REG 		(* (reg8 *) TesOutPin__0__INTTYPE)
#endif /* (TesOutPin__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_TesOutPin_H */


/* [] END OF FILE */

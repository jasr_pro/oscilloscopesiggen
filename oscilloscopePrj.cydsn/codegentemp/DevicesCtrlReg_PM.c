/*******************************************************************************
* File Name: DevicesCtrlReg_PM.c
* Version 1.80
*
* Description:
*  This file contains the setup, control, and status commands to support 
*  the component operation in the low power mode. 
*
* Note:
*
********************************************************************************
* Copyright 2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "DevicesCtrlReg.h"

/* Check for removal by optimization */
#if !defined(DevicesCtrlReg_Sync_ctrl_reg__REMOVED)

static DevicesCtrlReg_BACKUP_STRUCT  DevicesCtrlReg_backup = {0u};

    
/*******************************************************************************
* Function Name: DevicesCtrlReg_SaveConfig
********************************************************************************
*
* Summary:
*  Saves the control register value.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void DevicesCtrlReg_SaveConfig(void) 
{
    DevicesCtrlReg_backup.controlState = DevicesCtrlReg_Control;
}


/*******************************************************************************
* Function Name: DevicesCtrlReg_RestoreConfig
********************************************************************************
*
* Summary:
*  Restores the control register value.
*
* Parameters:
*  None
*
* Return:
*  None
*
*
*******************************************************************************/
void DevicesCtrlReg_RestoreConfig(void) 
{
     DevicesCtrlReg_Control = DevicesCtrlReg_backup.controlState;
}


/*******************************************************************************
* Function Name: DevicesCtrlReg_Sleep
********************************************************************************
*
* Summary:
*  Prepares the component for entering the low power mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void DevicesCtrlReg_Sleep(void) 
{
    DevicesCtrlReg_SaveConfig();
}


/*******************************************************************************
* Function Name: DevicesCtrlReg_Wakeup
********************************************************************************
*
* Summary:
*  Restores the component after waking up from the low power mode.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void DevicesCtrlReg_Wakeup(void)  
{
    DevicesCtrlReg_RestoreConfig();
}

#endif /* End check for removal by optimization */


/* [] END OF FILE */

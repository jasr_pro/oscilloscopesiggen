/*******************************************************************************
* File Name: LCD_DATA.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_LCD_DATA_ALIASES_H) /* Pins LCD_DATA_ALIASES_H */
#define CY_PINS_LCD_DATA_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"


/***************************************
*              Constants        
***************************************/
#define LCD_DATA_0			(LCD_DATA__0__PC)
#define LCD_DATA_0_INTR	((uint16)((uint16)0x0001u << LCD_DATA__0__SHIFT))

#define LCD_DATA_1			(LCD_DATA__1__PC)
#define LCD_DATA_1_INTR	((uint16)((uint16)0x0001u << LCD_DATA__1__SHIFT))

#define LCD_DATA_2			(LCD_DATA__2__PC)
#define LCD_DATA_2_INTR	((uint16)((uint16)0x0001u << LCD_DATA__2__SHIFT))

#define LCD_DATA_3			(LCD_DATA__3__PC)
#define LCD_DATA_3_INTR	((uint16)((uint16)0x0001u << LCD_DATA__3__SHIFT))

#define LCD_DATA_4			(LCD_DATA__4__PC)
#define LCD_DATA_4_INTR	((uint16)((uint16)0x0001u << LCD_DATA__4__SHIFT))

#define LCD_DATA_5			(LCD_DATA__5__PC)
#define LCD_DATA_5_INTR	((uint16)((uint16)0x0001u << LCD_DATA__5__SHIFT))

#define LCD_DATA_6			(LCD_DATA__6__PC)
#define LCD_DATA_6_INTR	((uint16)((uint16)0x0001u << LCD_DATA__6__SHIFT))

#define LCD_DATA_7			(LCD_DATA__7__PC)
#define LCD_DATA_7_INTR	((uint16)((uint16)0x0001u << LCD_DATA__7__SHIFT))

#define LCD_DATA_INTR_ALL	 ((uint16)(LCD_DATA_0_INTR| LCD_DATA_1_INTR| LCD_DATA_2_INTR| LCD_DATA_3_INTR| LCD_DATA_4_INTR| LCD_DATA_5_INTR| LCD_DATA_6_INTR| LCD_DATA_7_INTR))

#endif /* End Pins LCD_DATA_ALIASES_H */


/* [] END OF FILE */

/*******************************************************************************
* File Name: LCD_DATA.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_LCD_DATA_H) /* Pins LCD_DATA_H */
#define CY_PINS_LCD_DATA_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "LCD_DATA_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 LCD_DATA__PORT == 15 && ((LCD_DATA__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    LCD_DATA_Write(uint8 value);
void    LCD_DATA_SetDriveMode(uint8 mode);
uint8   LCD_DATA_ReadDataReg(void);
uint8   LCD_DATA_Read(void);
void    LCD_DATA_SetInterruptMode(uint16 position, uint16 mode);
uint8   LCD_DATA_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the LCD_DATA_SetDriveMode() function.
     *  @{
     */
        #define LCD_DATA_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define LCD_DATA_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define LCD_DATA_DM_RES_UP          PIN_DM_RES_UP
        #define LCD_DATA_DM_RES_DWN         PIN_DM_RES_DWN
        #define LCD_DATA_DM_OD_LO           PIN_DM_OD_LO
        #define LCD_DATA_DM_OD_HI           PIN_DM_OD_HI
        #define LCD_DATA_DM_STRONG          PIN_DM_STRONG
        #define LCD_DATA_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define LCD_DATA_MASK               LCD_DATA__MASK
#define LCD_DATA_SHIFT              LCD_DATA__SHIFT
#define LCD_DATA_WIDTH              8u

/* Interrupt constants */
#if defined(LCD_DATA__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in LCD_DATA_SetInterruptMode() function.
     *  @{
     */
        #define LCD_DATA_INTR_NONE      (uint16)(0x0000u)
        #define LCD_DATA_INTR_RISING    (uint16)(0x0001u)
        #define LCD_DATA_INTR_FALLING   (uint16)(0x0002u)
        #define LCD_DATA_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define LCD_DATA_INTR_MASK      (0x01u) 
#endif /* (LCD_DATA__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define LCD_DATA_PS                     (* (reg8 *) LCD_DATA__PS)
/* Data Register */
#define LCD_DATA_DR                     (* (reg8 *) LCD_DATA__DR)
/* Port Number */
#define LCD_DATA_PRT_NUM                (* (reg8 *) LCD_DATA__PRT) 
/* Connect to Analog Globals */                                                  
#define LCD_DATA_AG                     (* (reg8 *) LCD_DATA__AG)                       
/* Analog MUX bux enable */
#define LCD_DATA_AMUX                   (* (reg8 *) LCD_DATA__AMUX) 
/* Bidirectional Enable */                                                        
#define LCD_DATA_BIE                    (* (reg8 *) LCD_DATA__BIE)
/* Bit-mask for Aliased Register Access */
#define LCD_DATA_BIT_MASK               (* (reg8 *) LCD_DATA__BIT_MASK)
/* Bypass Enable */
#define LCD_DATA_BYP                    (* (reg8 *) LCD_DATA__BYP)
/* Port wide control signals */                                                   
#define LCD_DATA_CTL                    (* (reg8 *) LCD_DATA__CTL)
/* Drive Modes */
#define LCD_DATA_DM0                    (* (reg8 *) LCD_DATA__DM0) 
#define LCD_DATA_DM1                    (* (reg8 *) LCD_DATA__DM1)
#define LCD_DATA_DM2                    (* (reg8 *) LCD_DATA__DM2) 
/* Input Buffer Disable Override */
#define LCD_DATA_INP_DIS                (* (reg8 *) LCD_DATA__INP_DIS)
/* LCD Common or Segment Drive */
#define LCD_DATA_LCD_COM_SEG            (* (reg8 *) LCD_DATA__LCD_COM_SEG)
/* Enable Segment LCD */
#define LCD_DATA_LCD_EN                 (* (reg8 *) LCD_DATA__LCD_EN)
/* Slew Rate Control */
#define LCD_DATA_SLW                    (* (reg8 *) LCD_DATA__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define LCD_DATA_PRTDSI__CAPS_SEL       (* (reg8 *) LCD_DATA__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define LCD_DATA_PRTDSI__DBL_SYNC_IN    (* (reg8 *) LCD_DATA__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define LCD_DATA_PRTDSI__OE_SEL0        (* (reg8 *) LCD_DATA__PRTDSI__OE_SEL0) 
#define LCD_DATA_PRTDSI__OE_SEL1        (* (reg8 *) LCD_DATA__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define LCD_DATA_PRTDSI__OUT_SEL0       (* (reg8 *) LCD_DATA__PRTDSI__OUT_SEL0) 
#define LCD_DATA_PRTDSI__OUT_SEL1       (* (reg8 *) LCD_DATA__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define LCD_DATA_PRTDSI__SYNC_OUT       (* (reg8 *) LCD_DATA__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(LCD_DATA__SIO_CFG)
    #define LCD_DATA_SIO_HYST_EN        (* (reg8 *) LCD_DATA__SIO_HYST_EN)
    #define LCD_DATA_SIO_REG_HIFREQ     (* (reg8 *) LCD_DATA__SIO_REG_HIFREQ)
    #define LCD_DATA_SIO_CFG            (* (reg8 *) LCD_DATA__SIO_CFG)
    #define LCD_DATA_SIO_DIFF           (* (reg8 *) LCD_DATA__SIO_DIFF)
#endif /* (LCD_DATA__SIO_CFG) */

/* Interrupt Registers */
#if defined(LCD_DATA__INTSTAT)
    #define LCD_DATA_INTSTAT            (* (reg8 *) LCD_DATA__INTSTAT)
    #define LCD_DATA_SNAP               (* (reg8 *) LCD_DATA__SNAP)
    
	#define LCD_DATA_0_INTTYPE_REG 		(* (reg8 *) LCD_DATA__0__INTTYPE)
	#define LCD_DATA_1_INTTYPE_REG 		(* (reg8 *) LCD_DATA__1__INTTYPE)
	#define LCD_DATA_2_INTTYPE_REG 		(* (reg8 *) LCD_DATA__2__INTTYPE)
	#define LCD_DATA_3_INTTYPE_REG 		(* (reg8 *) LCD_DATA__3__INTTYPE)
	#define LCD_DATA_4_INTTYPE_REG 		(* (reg8 *) LCD_DATA__4__INTTYPE)
	#define LCD_DATA_5_INTTYPE_REG 		(* (reg8 *) LCD_DATA__5__INTTYPE)
	#define LCD_DATA_6_INTTYPE_REG 		(* (reg8 *) LCD_DATA__6__INTTYPE)
	#define LCD_DATA_7_INTTYPE_REG 		(* (reg8 *) LCD_DATA__7__INTTYPE)
#endif /* (LCD_DATA__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_LCD_DATA_H */


/* [] END OF FILE */

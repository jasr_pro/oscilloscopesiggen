/*******************************************************************************
* File Name: DacPin.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_DacPin_H) /* Pins DacPin_H */
#define CY_PINS_DacPin_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "DacPin_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 DacPin__PORT == 15 && ((DacPin__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    DacPin_Write(uint8 value);
void    DacPin_SetDriveMode(uint8 mode);
uint8   DacPin_ReadDataReg(void);
uint8   DacPin_Read(void);
void    DacPin_SetInterruptMode(uint16 position, uint16 mode);
uint8   DacPin_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the DacPin_SetDriveMode() function.
     *  @{
     */
        #define DacPin_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define DacPin_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define DacPin_DM_RES_UP          PIN_DM_RES_UP
        #define DacPin_DM_RES_DWN         PIN_DM_RES_DWN
        #define DacPin_DM_OD_LO           PIN_DM_OD_LO
        #define DacPin_DM_OD_HI           PIN_DM_OD_HI
        #define DacPin_DM_STRONG          PIN_DM_STRONG
        #define DacPin_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define DacPin_MASK               DacPin__MASK
#define DacPin_SHIFT              DacPin__SHIFT
#define DacPin_WIDTH              1u

/* Interrupt constants */
#if defined(DacPin__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in DacPin_SetInterruptMode() function.
     *  @{
     */
        #define DacPin_INTR_NONE      (uint16)(0x0000u)
        #define DacPin_INTR_RISING    (uint16)(0x0001u)
        #define DacPin_INTR_FALLING   (uint16)(0x0002u)
        #define DacPin_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define DacPin_INTR_MASK      (0x01u) 
#endif /* (DacPin__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define DacPin_PS                     (* (reg8 *) DacPin__PS)
/* Data Register */
#define DacPin_DR                     (* (reg8 *) DacPin__DR)
/* Port Number */
#define DacPin_PRT_NUM                (* (reg8 *) DacPin__PRT) 
/* Connect to Analog Globals */                                                  
#define DacPin_AG                     (* (reg8 *) DacPin__AG)                       
/* Analog MUX bux enable */
#define DacPin_AMUX                   (* (reg8 *) DacPin__AMUX) 
/* Bidirectional Enable */                                                        
#define DacPin_BIE                    (* (reg8 *) DacPin__BIE)
/* Bit-mask for Aliased Register Access */
#define DacPin_BIT_MASK               (* (reg8 *) DacPin__BIT_MASK)
/* Bypass Enable */
#define DacPin_BYP                    (* (reg8 *) DacPin__BYP)
/* Port wide control signals */                                                   
#define DacPin_CTL                    (* (reg8 *) DacPin__CTL)
/* Drive Modes */
#define DacPin_DM0                    (* (reg8 *) DacPin__DM0) 
#define DacPin_DM1                    (* (reg8 *) DacPin__DM1)
#define DacPin_DM2                    (* (reg8 *) DacPin__DM2) 
/* Input Buffer Disable Override */
#define DacPin_INP_DIS                (* (reg8 *) DacPin__INP_DIS)
/* LCD Common or Segment Drive */
#define DacPin_LCD_COM_SEG            (* (reg8 *) DacPin__LCD_COM_SEG)
/* Enable Segment LCD */
#define DacPin_LCD_EN                 (* (reg8 *) DacPin__LCD_EN)
/* Slew Rate Control */
#define DacPin_SLW                    (* (reg8 *) DacPin__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define DacPin_PRTDSI__CAPS_SEL       (* (reg8 *) DacPin__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define DacPin_PRTDSI__DBL_SYNC_IN    (* (reg8 *) DacPin__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define DacPin_PRTDSI__OE_SEL0        (* (reg8 *) DacPin__PRTDSI__OE_SEL0) 
#define DacPin_PRTDSI__OE_SEL1        (* (reg8 *) DacPin__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define DacPin_PRTDSI__OUT_SEL0       (* (reg8 *) DacPin__PRTDSI__OUT_SEL0) 
#define DacPin_PRTDSI__OUT_SEL1       (* (reg8 *) DacPin__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define DacPin_PRTDSI__SYNC_OUT       (* (reg8 *) DacPin__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(DacPin__SIO_CFG)
    #define DacPin_SIO_HYST_EN        (* (reg8 *) DacPin__SIO_HYST_EN)
    #define DacPin_SIO_REG_HIFREQ     (* (reg8 *) DacPin__SIO_REG_HIFREQ)
    #define DacPin_SIO_CFG            (* (reg8 *) DacPin__SIO_CFG)
    #define DacPin_SIO_DIFF           (* (reg8 *) DacPin__SIO_DIFF)
#endif /* (DacPin__SIO_CFG) */

/* Interrupt Registers */
#if defined(DacPin__INTSTAT)
    #define DacPin_INTSTAT            (* (reg8 *) DacPin__INTSTAT)
    #define DacPin_SNAP               (* (reg8 *) DacPin__SNAP)
    
	#define DacPin_0_INTTYPE_REG 		(* (reg8 *) DacPin__0__INTTYPE)
#endif /* (DacPin__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_DacPin_H */


/* [] END OF FILE */

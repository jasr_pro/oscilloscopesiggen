/*******************************************************************************
* File Name: SqrDigOut.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_SqrDigOut_H) /* Pins SqrDigOut_H */
#define CY_PINS_SqrDigOut_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"
#include "SqrDigOut_aliases.h"

/* APIs are not generated for P15[7:6] */
#if !(CY_PSOC5A &&\
	 SqrDigOut__PORT == 15 && ((SqrDigOut__MASK & 0xC0) != 0))


/***************************************
*        Function Prototypes             
***************************************/    

/**
* \addtogroup group_general
* @{
*/
void    SqrDigOut_Write(uint8 value);
void    SqrDigOut_SetDriveMode(uint8 mode);
uint8   SqrDigOut_ReadDataReg(void);
uint8   SqrDigOut_Read(void);
void    SqrDigOut_SetInterruptMode(uint16 position, uint16 mode);
uint8   SqrDigOut_ClearInterrupt(void);
/** @} general */

/***************************************
*           API Constants        
***************************************/
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup driveMode Drive mode constants
     * \brief Constants to be passed as "mode" parameter in the SqrDigOut_SetDriveMode() function.
     *  @{
     */
        #define SqrDigOut_DM_ALG_HIZ         PIN_DM_ALG_HIZ
        #define SqrDigOut_DM_DIG_HIZ         PIN_DM_DIG_HIZ
        #define SqrDigOut_DM_RES_UP          PIN_DM_RES_UP
        #define SqrDigOut_DM_RES_DWN         PIN_DM_RES_DWN
        #define SqrDigOut_DM_OD_LO           PIN_DM_OD_LO
        #define SqrDigOut_DM_OD_HI           PIN_DM_OD_HI
        #define SqrDigOut_DM_STRONG          PIN_DM_STRONG
        #define SqrDigOut_DM_RES_UPDWN       PIN_DM_RES_UPDWN
    /** @} driveMode */
/** @} group_constants */
    
/* Digital Port Constants */
#define SqrDigOut_MASK               SqrDigOut__MASK
#define SqrDigOut_SHIFT              SqrDigOut__SHIFT
#define SqrDigOut_WIDTH              1u

/* Interrupt constants */
#if defined(SqrDigOut__INTSTAT)
/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in SqrDigOut_SetInterruptMode() function.
     *  @{
     */
        #define SqrDigOut_INTR_NONE      (uint16)(0x0000u)
        #define SqrDigOut_INTR_RISING    (uint16)(0x0001u)
        #define SqrDigOut_INTR_FALLING   (uint16)(0x0002u)
        #define SqrDigOut_INTR_BOTH      (uint16)(0x0003u) 
    /** @} intrMode */
/** @} group_constants */

    #define SqrDigOut_INTR_MASK      (0x01u) 
#endif /* (SqrDigOut__INTSTAT) */


/***************************************
*             Registers        
***************************************/

/* Main Port Registers */
/* Pin State */
#define SqrDigOut_PS                     (* (reg8 *) SqrDigOut__PS)
/* Data Register */
#define SqrDigOut_DR                     (* (reg8 *) SqrDigOut__DR)
/* Port Number */
#define SqrDigOut_PRT_NUM                (* (reg8 *) SqrDigOut__PRT) 
/* Connect to Analog Globals */                                                  
#define SqrDigOut_AG                     (* (reg8 *) SqrDigOut__AG)                       
/* Analog MUX bux enable */
#define SqrDigOut_AMUX                   (* (reg8 *) SqrDigOut__AMUX) 
/* Bidirectional Enable */                                                        
#define SqrDigOut_BIE                    (* (reg8 *) SqrDigOut__BIE)
/* Bit-mask for Aliased Register Access */
#define SqrDigOut_BIT_MASK               (* (reg8 *) SqrDigOut__BIT_MASK)
/* Bypass Enable */
#define SqrDigOut_BYP                    (* (reg8 *) SqrDigOut__BYP)
/* Port wide control signals */                                                   
#define SqrDigOut_CTL                    (* (reg8 *) SqrDigOut__CTL)
/* Drive Modes */
#define SqrDigOut_DM0                    (* (reg8 *) SqrDigOut__DM0) 
#define SqrDigOut_DM1                    (* (reg8 *) SqrDigOut__DM1)
#define SqrDigOut_DM2                    (* (reg8 *) SqrDigOut__DM2) 
/* Input Buffer Disable Override */
#define SqrDigOut_INP_DIS                (* (reg8 *) SqrDigOut__INP_DIS)
/* LCD Common or Segment Drive */
#define SqrDigOut_LCD_COM_SEG            (* (reg8 *) SqrDigOut__LCD_COM_SEG)
/* Enable Segment LCD */
#define SqrDigOut_LCD_EN                 (* (reg8 *) SqrDigOut__LCD_EN)
/* Slew Rate Control */
#define SqrDigOut_SLW                    (* (reg8 *) SqrDigOut__SLW)

/* DSI Port Registers */
/* Global DSI Select Register */
#define SqrDigOut_PRTDSI__CAPS_SEL       (* (reg8 *) SqrDigOut__PRTDSI__CAPS_SEL) 
/* Double Sync Enable */
#define SqrDigOut_PRTDSI__DBL_SYNC_IN    (* (reg8 *) SqrDigOut__PRTDSI__DBL_SYNC_IN) 
/* Output Enable Select Drive Strength */
#define SqrDigOut_PRTDSI__OE_SEL0        (* (reg8 *) SqrDigOut__PRTDSI__OE_SEL0) 
#define SqrDigOut_PRTDSI__OE_SEL1        (* (reg8 *) SqrDigOut__PRTDSI__OE_SEL1) 
/* Port Pin Output Select Registers */
#define SqrDigOut_PRTDSI__OUT_SEL0       (* (reg8 *) SqrDigOut__PRTDSI__OUT_SEL0) 
#define SqrDigOut_PRTDSI__OUT_SEL1       (* (reg8 *) SqrDigOut__PRTDSI__OUT_SEL1) 
/* Sync Output Enable Registers */
#define SqrDigOut_PRTDSI__SYNC_OUT       (* (reg8 *) SqrDigOut__PRTDSI__SYNC_OUT) 

/* SIO registers */
#if defined(SqrDigOut__SIO_CFG)
    #define SqrDigOut_SIO_HYST_EN        (* (reg8 *) SqrDigOut__SIO_HYST_EN)
    #define SqrDigOut_SIO_REG_HIFREQ     (* (reg8 *) SqrDigOut__SIO_REG_HIFREQ)
    #define SqrDigOut_SIO_CFG            (* (reg8 *) SqrDigOut__SIO_CFG)
    #define SqrDigOut_SIO_DIFF           (* (reg8 *) SqrDigOut__SIO_DIFF)
#endif /* (SqrDigOut__SIO_CFG) */

/* Interrupt Registers */
#if defined(SqrDigOut__INTSTAT)
    #define SqrDigOut_INTSTAT            (* (reg8 *) SqrDigOut__INTSTAT)
    #define SqrDigOut_SNAP               (* (reg8 *) SqrDigOut__SNAP)
    
	#define SqrDigOut_0_INTTYPE_REG 		(* (reg8 *) SqrDigOut__0__INTTYPE)
#endif /* (SqrDigOut__INTSTAT) */

#endif /* CY_PSOC5A... */

#endif /*  CY_PINS_SqrDigOut_H */


/* [] END OF FILE */

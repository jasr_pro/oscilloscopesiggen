/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef SIGNAL_GENERATOR_H

#define SIGNAL_GENERATOR_H

#include "psocUtilities.h"
#include "signalGenWave.h"
    
#define SIGGEN_MAX_BUF_SIZE  MAX_NUM_SAMPLES
    
#define TIMER_EN_CTRL_NUM_SIG  3  /* Bit in DeviceCtrl Register used 
                                      to reset Signal generator timer*/
#define SIG_GEM_DIGITAL_EN_NUM_SIG  2
    
#define TIMER_EN_CTRL_MASK  (uint8_t)(1 << TIMER_EN_CTRL_NUM_SIG);
#define SIG_GEN_DIGITAL_EN_CTRL_MASK  (uint8_t)(1 << SIG_GEM_DIGITAL_EN_NUM_SIG);    
    
#define TIMER_EN_SET_VAL   1    /* Value that releases from reset */  
#define TIMER_EN_CLEAR_VAL 0    /* Value that releases from reset */
    
#define TIMER_EN_SET_VAL_MASK   (uint8_t)(TIMER_EN_SET_VAL << TIMER_EN_CTRL_NUM_SIG)
#define TIMER_EN_CLEAR_VAL_MASK (uint8_t)(TIMER_EN_CLEAR_VAL << TIMER_EN_CTRL_NUM_SIG)    
    
#define TIMER_SIGGEN_CLOCK_FREQ 32000000U
#define TIMER_SIGGEN_CLOCK_FREQ_MHZ (TIMER_SIGGEN_CLOCK_FREQ/1000000U)

#define SIG_GEN_DIGITAL_ENABLE_MASK   (uint8_t) (TIMER_EN_SET_VAL << SIG_GEM_DIGITAL_EN_NUM_SIG)
#define SIG_GEN_DIGITAL_DISABLE_MASK  (uint8_t) (TIMER_EN_CLEAR_VAL << SIG_GEM_DIGITAL_EN_NUM_SIG)    
    
/* For 8-bit DAC with a range of 0v-1v the maximum sample rate is 1 Msps 
  (minimum time between samples is 1us or 1000 ns */
#define  SIGGEN_MINUM_SAMPLE_TIME_SUPPORTED_NS 1000   

#define SIGGEN_TIMER_RESOLUTION 24
#define SIGGEN_MAX_TIMER_TICK   ((((uint32_t)1) << SIGGEN_TIMER_RESOLUTION) - 1)
    
#define SIGGEN_INIT_FREQ_HZ  10000
    
void signalGenInit();
void sigGenTimerEnable();
void sigGenTimerDisable();
void sigGenDigitalEnable();
void sigGenDigitalDisable();
void sigGenDemo();

uint32_t sigGenConvertSampleTime2Ticks(uint32_t sampleTimeNs );
int32_t reconfigureSigGenWave(uint32_t timerPeriodTicks, uint16_t numSamples);
int32_t reconfigureSigGenWaveManual(tSigGenWaveType sigType, uint32_t timerPeriodTicks, uint16_t numSamples);
int32_t reconfigureSigGenWaveAuto(tSigGenWaveType sigType, uint32_t freqHz);

#endif

/* [] END OF FILE */

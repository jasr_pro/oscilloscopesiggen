/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef PSOC_UTILITIES
  
#define PSOC_UTILITIES
  
#include "cytypes.h"
#include <CyLib.h>

#define true   1U
#define false  0U
#define NULL   ((void *)0)
    
#define ERROR_RESULT 0xFF
#define QUEUE_FULL_RESULT 0xFE
#define QUEUE_NO_ENOUGH_DATA_RESULT 0xFD    
#define OK_RESULT 0x0    
    
typedef  uint16 uint16_t;
typedef  uint8 uint8_t;
typedef  uint64 uint64_t;
    
typedef  unsigned char bool;
   
#define atomicStart CyGlobalIntDisable
#define atomicEnd   CyGlobalIntEnable 
    
 void pSOCdelayUS(uint16_t timeUS);
 void pSOCdelyMS(uint16_t timeMS);   
#endif



/* [] END OF FILE */

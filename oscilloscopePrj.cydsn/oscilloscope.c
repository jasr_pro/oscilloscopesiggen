/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "oscilloscope.h"
#include "TimerADC.h"
#include "DevicesCtrlReg.h"
#include "psocUtilities.h"
#include "ADC_DMA_CH1_dma.h"
#include "ADC_DMA_CH2_dma.h"
#include "ADC_SAR_1.h"
#include "ADC_SAR_2.h"
#include "adc1_dma_complete.h"
#include "adc2_dma_complete.h"

#include "plotWave.h"
#include "lcdMcuFriend9342.h"
#include "projectApp.h"


#include "stdio.h"

/* Defines for ADC_DMA */
#if (ADC_RESOLUTION == 8)     
  #define ADC_DMA_BYTES_PER_BURST 1
#else
  #define ADC_DMA_BYTES_PER_BURST 2   
#endif

#define ADC_DMA_REQUEST_PER_BURST 1
#define ADC_DMA_SRC_BASE (CYDEV_PERIPH_BASE)
#define ADC_DMA_DST_BASE (CYDEV_SRAM_BASE)

#define ADC_DMA_NUM_BYTES_CYCLE  (ADC_NUM_SAMPLES_BUFF_SIZE*ADC_DMA_BYTES_PER_BURST)

tOscStatus oscStatus;

CY_ISR(adc1DmaCompleteHandler);
CY_ISR(adc2DmaCompleteHandler);

void adcTimerDisable();
void adcTimerEnable();

uint32_t oscConvertSampleTime2Ticks(uint32_t sampleTimeNs );

uint16_t colorChannels[OSC_ADC_NUM_CHANNELS] = {COLOR_RED, COLOR_BLUE };

extern tPrjStatus prjStatus;

static tAdcDataType  adcValues[OSC_ADC_NUM_CHANNELS][ADC_NUM_SAMPLES_BUFF_SIZE];  


/*
   1) Initializa ADC timer
   2) Disable ADC timer
   3) Convert default ADC sample time (OSC_DEFAULT_SAMPLE_TIME_NS) to ADC timer ticks 
   4) Write period time to ADC timer
   5) Configure DMA for ADC 
   6) Initialize ADC
   
    At this time the Oscilloscope hardware is fully configured to sample
    at the default rate and only need that the ADC Timer gets enabled.

*/

void oscilloscopeInit()
{
    uint8_t chnlIdx;
    bool adcSampleSt = false;
    bool pWaveInitBool = false;
    uint8_t pWaveInit;    
    
    oscStatus.adcTimerState = false;
       
    /* Configure timer with a default frequency */
    oscStatus.cfgIn.numChannelsEnabled = OSC_NUM_CHANNELS_EN_DEFAULT;
    oscStatus.cfgIn.numMaxChannels = OSC_ADC_NUM_CHANNELS;    
    oscStatus.cfgIn.sampleTimeNs = OSC_DEFAULT_SAMPLE_TIME_NS;
    oscStatus.cfgIn.timeToPrepareForNextSetNs = OSC_SETUP_TIME_SAMPLE_NEXT_SET_NS;
    oscStatus.cfgIn.convertTimePerSampleNs = OSC_CONVERT_SAMPLE_TO_PLOT_TIME_NS;
    oscStatus.cfgIn.plotTimePerSampleNs = OSC_PLOT_TIME_PER_SAMPLE_NS;
    oscStatus.cfgIn.windowRefreshTimeNs = OSC_PLOT_REFRESH_TIME_NS;
    oscStatus.samplePeriodTicks = oscConvertSampleTime2Ticks( OSC_DEFAULT_SAMPLE_TIME_NS );   /* Fix me. Define initial sample period*/    
    oscStatus.reconfigFlag = false;
    oscStatus.pauseSamplingFlag = false;
    oscStatus.activateOscFlag = false;
    
    for (chnlIdx = 0; chnlIdx < oscStatus.cfgIn.numMaxChannels; chnlIdx++)
    {
       oscStatus.allSetsAquired[chnlIdx] = false; 
       oscStatus.setAquiredIdx[chnlIdx] = 0;
       oscStatus.setConvertIdx[chnlIdx] = 0;
       oscStatus.sampleBuffAddr[chnlIdx] = &adcValues[chnlIdx][0];       
    }
    
    /* Initialize sample state component, including the clearing of the buffer
       that keeps the state of the sampling set.
    */
    adcSampleSt = adcSampleStateInit(&oscStatus.cfgIn, &oscStatus.setInfo );
    
    prjStatus.oscilInitDone = adcSampleSt;
    
    printf("@oscilloscopeInit() - Number of Sets: %d Samples in set: %d \n", 
            oscStatus.setInfo.numSampleSets, oscStatus.setInfo.numSamplesPerSet );
    
    TimerADC_Start();  /* Enable timer, but Hardware Enable signal is not set*/
    adcTimerDisable(); /* Make sure the ADC timer is disabled keeping it disabled */
    
    printf("@oscilloscopeInit() Timer ticks: %d\n", oscStatus.samplePeriodTicks);
    TimerADC_WritePeriod(oscStatus.samplePeriodTicks);
    
    /* DMA Configuration for DMA_2 */
    for (chnlIdx = 0; chnlIdx < oscStatus.cfgIn.numMaxChannels; chnlIdx++)
    {
       if (chnlIdx == ADC_CHNL_1_IDX)
       {
          printf("Configuring ADC CH1- numSamples: %d target addr: 0x%lx, 0x%lx\r\n", 
                 ADC_DMA_NUM_BYTES_CYCLE, (uint32)adcValues[chnlIdx], (uint32)&adcValues[chnlIdx][0]);
          oscStatus.ADC_DMA_Chan[ADC_CHNL_1_IDX] = ADC_DMA_CH1_DmaInitialize(ADC_DMA_BYTES_PER_BURST, ADC_DMA_REQUEST_PER_BURST, 
                                  HI16(ADC_DMA_SRC_BASE), HI16(ADC_DMA_DST_BASE));
          oscStatus.ADC_DMA_TD[ADC_CHNL_1_IDX] = CyDmaTdAllocate();
          CyDmaTdSetConfiguration(oscStatus.ADC_DMA_TD[ADC_CHNL_1_IDX], oscStatus.setInfo.numSamplesPerSet*ADC_DMA_BYTES_PER_BURST, CY_DMA_DISABLE_TD,  ADC_DMA_CH1__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR);
          CyDmaTdSetAddress(oscStatus.ADC_DMA_TD[ADC_CHNL_1_IDX], LO16((uint32)ADC_SAR_1_SAR_WRK0_PTR), LO16((uint16)oscStatus.sampleBuffAddr[ADC_CHNL_1_IDX]));
          CyDmaChSetInitialTd(oscStatus.ADC_DMA_Chan[ADC_CHNL_1_IDX], oscStatus.ADC_DMA_TD[ADC_CHNL_1_IDX]);
          CyDmaChEnable(oscStatus.ADC_DMA_Chan[ADC_CHNL_1_IDX], 1);

          /* Enable ADC DMA Interrupt */
          adc1_dma_complete_StartEx(adc1DmaCompleteHandler);

          /* Enable ADC 1 */
          ADC_SAR_1_Start(); 
       }
    
       if (chnlIdx == ADC_CHNL_2_IDX)
       {
          printf("Configuring ADC CH2- numSamples: %d target addr: 0x%lx, 0x%lx\r\n", 
                 ADC_DMA_NUM_BYTES_CYCLE, (uint32)adcValues[chnlIdx], (uint32)&adcValues[chnlIdx][0]);
          oscStatus.ADC_DMA_Chan[ADC_CHNL_2_IDX] = ADC_DMA_CH2_DmaInitialize(ADC_DMA_BYTES_PER_BURST, ADC_DMA_REQUEST_PER_BURST, 
                                  HI16(ADC_DMA_SRC_BASE), HI16(ADC_DMA_DST_BASE));
          oscStatus.ADC_DMA_TD[ADC_CHNL_2_IDX] = CyDmaTdAllocate();
          CyDmaTdSetConfiguration(oscStatus.ADC_DMA_TD[ADC_CHNL_2_IDX], oscStatus.setInfo.numSamplesPerSet*ADC_DMA_BYTES_PER_BURST, CY_DMA_DISABLE_TD,  ADC_DMA_CH2__TD_TERMOUT_EN | CY_DMA_TD_INC_DST_ADR);
          CyDmaTdSetAddress(oscStatus.ADC_DMA_TD[ADC_CHNL_2_IDX], LO16((uint32)ADC_SAR_2_SAR_WRK0_PTR), LO16((uint16)oscStatus.sampleBuffAddr[ADC_CHNL_2_IDX]));
          CyDmaChSetInitialTd(oscStatus.ADC_DMA_Chan[ADC_CHNL_2_IDX], oscStatus.ADC_DMA_TD[ADC_CHNL_2_IDX]);
          CyDmaChEnable(oscStatus.ADC_DMA_Chan[ADC_CHNL_2_IDX], 1);

          /* Enable ADC DMA Interrupt */
          adc2_dma_complete_StartEx(adc2DmaCompleteHandler);

          /* Enable ADC 1 */
          ADC_SAR_2_Start(); 
       }
    
       
       /* Disable the channels that are not enabled */
       for (chnlIdx = oscStatus.cfgIn.numChannelsEnabled; chnlIdx < oscStatus.cfgIn.numMaxChannels; chnlIdx++)
       {
          if ( chnlIdx == ADC_CHNL_1_IDX)
          {
             ADC_SAR_1_Stop();
             CyDmaChDisable(oscStatus.ADC_DMA_Chan[ADC_CHNL_1_IDX]);
          }
        
          if ( chnlIdx == ADC_CHNL_2_IDX)
          {
             ADC_SAR_2_Stop();
             CyDmaChDisable(oscStatus.ADC_DMA_Chan[ADC_CHNL_2_IDX]);
          }          
       }                  
    }
       /* Initialize plot area for oscilloscope */ 
   /*pWaveInit = plotAreaInit( 21, 21, 300, 220, COLOR_BLACK, 50, 50, COLOR_YELLOW,                       
                              2,  colorChannels, true, true  );*/
    
    pWaveInitBool = (pWaveInit == 0) ? true : false; 
    prjStatus.oscilInitDone &= pWaveInitBool; 
}

/* Enable the Timer used for ADC */    
void adcTimerEnable()
{
    uint8_t devCtrlRegVal;
    
    if ( oscStatus.adcTimerState == false)
    {
      devCtrlRegVal = DevicesCtrlReg_Read();
      /*Clear corresponding pin */
      devCtrlRegVal &= ~ADC_TIMER_EN_CTRL_MASK;
      devCtrlRegVal |= ADC_TIMER_EN_SET_VAL_MASK;
      DevicesCtrlReg_Write(devCtrlRegVal);
      oscStatus.adcTimerState = true;
    }
}

/* Disable ADC Timer */
void adcTimerDisable()
{
    uint8_t devCtrlRegVal;
    
    if ( oscStatus.adcTimerState == true)
    {
      devCtrlRegVal = DevicesCtrlReg_Read();
      /*Clear corresponding pin */
      devCtrlRegVal &= ~ADC_TIMER_EN_CTRL_MASK;
      devCtrlRegVal |= ADC_TIMER_EN_CLEAR_VAL_MASK;   
      DevicesCtrlReg_Write(devCtrlRegVal); 
      oscStatus.adcTimerState = false;
    }
}

/* Restart a new sample capture stage for a new set (with the same configuration only updating the address buffer) */
inline void adcDmaIterReconfig(uint8_t channelNum,  void *sampleBufferAddr)
{    
       
  /* Disable channel */
  CyDmaChDisable(oscStatus.ADC_DMA_Chan[channelNum]);    
    
  /* Set the buffer address */
  if (channelNum == ADC_CHNL_1_IDX)
  {
    CyDmaTdSetAddress(oscStatus.ADC_DMA_TD[channelNum], LO16((uint32)ADC_SAR_1_SAR_WRK0_PTR), LO16((uint16_t)sampleBufferAddr));
  }
  if (channelNum == ADC_CHNL_2_IDX)
  {
     CyDmaTdSetAddress(oscStatus.ADC_DMA_TD[channelNum], LO16((uint32)ADC_SAR_2_SAR_WRK0_PTR), LO16((uint16_t)sampleBufferAddr));  
  }
    
  /* Re-enable the channel */
  CyDmaChEnable(oscStatus.ADC_DMA_Chan[channelNum], 1);
    
}

/* ADC DMA Channel 0 ISR */
CY_ISR(adc1DmaCompleteHandler)
{
  uint16_t newIdx = oscStatus.setAquiredIdx[ADC_CHNL_1_IDX] + 1;
    
  adc1_dma_complete_ClearPending();

  /* Only restart opperation if abort flag is not set */
  if (oscStatus.reconfigFlag == false)
  {
    /* Notify to the sample state that this set is ready to be converted */
    setSampleSetState(ADC_CHNL_1_IDX, oscStatus.setAquiredIdx[ADC_CHNL_1_IDX], ADC_SAMPLE_AQUIRED_ST);

    if ( newIdx < oscStatus.setInfo.numSampleSets )
    {     
      /* Calculate the buffer address for the new set of samples */
      oscStatus.sampleBuffAddr[ADC_CHNL_1_IDX] += oscStatus.setInfo.numSamplesPerSet;
      oscStatus.setAquiredIdx[ADC_CHNL_1_IDX] = newIdx;    
      /* Setup new sample iteration for the next set */
      CyDmaChDisable(oscStatus.ADC_DMA_Chan[ADC_CHNL_1_IDX]);
      CyDmaTdSetAddress(oscStatus.ADC_DMA_TD[ADC_CHNL_1_IDX], LO16((uint32)ADC_SAR_1_SAR_WRK0_PTR), LO16((uint16_t)oscStatus.sampleBuffAddr[ADC_CHNL_1_IDX]));
      CyDmaChEnable(oscStatus.ADC_DMA_Chan[ADC_CHNL_1_IDX], 1);
    }
    else
    {
      /* All the sets in the iteration has been aquired. */
      oscStatus.allSetsAquired[ADC_CHNL_1_IDX] = true;
      /* Reset of indexes and values is done at continuous task when 
         all the data has been converted and ploted.
      */
    }
  }
}


/* ADC DMA Channel 2 ISR */
CY_ISR(adc2DmaCompleteHandler)
{
  uint16_t newIdx = oscStatus.setAquiredIdx[ADC_CHNL_2_IDX] + 1;  
  
  adc2_dma_complete_ClearPending();

    /* Only restart opperation if abort flag is not set */
  if (oscStatus.reconfigFlag == false)
  {
    /* Notify to the sample state that this set is ready to be converted */
    setSampleSetState(ADC_CHNL_2_IDX, oscStatus.setAquiredIdx[ADC_CHNL_2_IDX], ADC_SAMPLE_AQUIRED_ST);

    if ( newIdx < oscStatus.setInfo.numSampleSets )
    {     
      /* Calculate the buffer address for the new set of samples */
      oscStatus.sampleBuffAddr[ADC_CHNL_2_IDX] += oscStatus.setInfo.numSamplesPerSet;
      oscStatus.setAquiredIdx[ADC_CHNL_2_IDX] = newIdx;    
      /* Setup new sample iteration for the next set */
      CyDmaChDisable(oscStatus.ADC_DMA_Chan[ADC_CHNL_2_IDX]);
      CyDmaTdSetAddress(oscStatus.ADC_DMA_TD[ADC_CHNL_2_IDX], LO16((uint32)ADC_SAR_2_SAR_WRK0_PTR), LO16((uint16_t)oscStatus.sampleBuffAddr[ADC_CHNL_2_IDX]));
      CyDmaChEnable(oscStatus.ADC_DMA_Chan[ADC_CHNL_2_IDX], 1);
    }
    else
    {
      /* All the sets in the iteration has been aquired. */
      oscStatus.allSetsAquired[ADC_CHNL_2_IDX] = true;
      /* Reset of indexes and values is done at continuous task when 
         all the data has been converted and ploted.
      */
    }
  }
  
}

/* This function will be called to resart a new cycle of the sampling - 
   converting - ploting process using the same configuration than in the
   previous cycle. This should reset all the counters and sampling state.
   
*/
void restartOscProcess()
{
    uint8_t chnlIdx;
    
    /* Disable timer */ 
    adcTimerDisable();
    
    for ( chnlIdx = 0; chnlIdx < oscStatus.cfgIn.numChannelsEnabled; chnlIdx++)
    {
      oscStatus.allSetsAquired[chnlIdx] = false;
      oscStatus.setAquiredIdx[chnlIdx] = 0; 
      oscStatus.setConvertIdx[chnlIdx] = 0;
      adcSampleClearAllSetStateBuffer(chnlIdx);
      oscStatus.sampleBuffAddr[chnlIdx] = &adcValues[chnlIdx][0];      
    
      if (chnlIdx == ADC_CHNL_1_IDX)
      {
        adc1_dma_complete_ClearPending(); 
        CyDmaChDisable(oscStatus.ADC_DMA_Chan[ADC_CHNL_1_IDX]);
        CyDmaTdSetAddress(oscStatus.ADC_DMA_TD[ADC_CHNL_1_IDX], LO16((uint32)ADC_SAR_1_SAR_WRK0_PTR), LO16((uint16_t)oscStatus.sampleBuffAddr[ADC_CHNL_1_IDX]));
        CyDmaChEnable(oscStatus.ADC_DMA_Chan[ADC_CHNL_1_IDX], 1);
      }
     
      if (chnlIdx == ADC_CHNL_2_IDX)
      {
        adc2_dma_complete_ClearPending();
        CyDmaChDisable(oscStatus.ADC_DMA_Chan[ADC_CHNL_2_IDX]);
        CyDmaTdSetAddress(oscStatus.ADC_DMA_TD[ADC_CHNL_2_IDX], LO16((uint32)ADC_SAR_2_SAR_WRK0_PTR), LO16((uint16_t)oscStatus.sampleBuffAddr[ADC_CHNL_2_IDX]));
        CyDmaChEnable(oscStatus.ADC_DMA_Chan[ADC_CHNL_2_IDX], 1);
      }
    }
    
    /* Enable the ADC timer */
    adcTimerEnable();
}

/* Stop ADC components and restart status. This is expected to be used
   before restartOscAdcSamplingProcess.
*/
void stopOscAdcSamplingProcess()
{
 
   uint8_t chnlIdx;    
   adcTimerDisable(); /* Disable timer */
   
   /* Reset the channel enabled */
   for ( chnlIdx = 0; chnlIdx < oscStatus.cfgIn.numChannelsEnabled; chnlIdx++ )
   {    
      oscStatus.setAquiredIdx[chnlIdx] = 0;
      oscStatus.setConvertIdx[chnlIdx] = 0;
      oscStatus.sampleBuffAddr[chnlIdx] = &adcValues[chnlIdx][0];
      oscStatus.allSetsAquired[chnlIdx] = false;      
      
      if ( chnlIdx == ADC_CHNL_1_IDX)
      {
        adc1_dma_complete_ClearPending();
        /* Disable ADC 1 */
        ADC_SAR_1_Stop();        
        /* Disable DMA */
        CyDmaChDisable(oscStatus.ADC_DMA_Chan[ADC_CHNL_1_IDX]);
      }
    
      if ( chnlIdx == ADC_CHNL_2_IDX)
      {
        adc2_dma_complete_ClearPending();
        /* Disable ADC 1 */
        ADC_SAR_2_Stop();        
        /* Disable DMA */
        CyDmaChDisable(oscStatus.ADC_DMA_Chan[ADC_CHNL_2_IDX]);
      }
   }    
}

/* Reactivate Oscilloscope sampling process with the current configuration. 
   This doesn`t clears variables, so this uses the current configuration.
   This is expected to be used after stopOscAdcSamplingProcess
*/
void restartOscAdcSamplingProcess()
{
    
   uint8_t chnlIdx;
    
     /* Enabling the channels that need to be enabled */
   for ( chnlIdx = 0; chnlIdx < oscStatus.cfgIn.numChannelsEnabled; chnlIdx++)
   {                    
     
       if ( chnlIdx == ADC_CHNL_1_IDX)
       {  
          CyDmaTdSetAddress(oscStatus.ADC_DMA_TD[ADC_CHNL_1_IDX], LO16((uint32)ADC_SAR_1_SAR_WRK0_PTR), LO16((uint16_t)oscStatus.sampleBuffAddr[ADC_CHNL_1_IDX]));          
          CyDmaChEnable(oscStatus.ADC_DMA_Chan[ADC_CHNL_1_IDX], 1);        
                            
           /* Re-Enable DMA */
           CyDmaChEnable(oscStatus.ADC_DMA_Chan[ADC_CHNL_1_IDX], 1); 
                        
           /* Enable ADC 1 */
           ADC_SAR_1_Start();        
        }
    
        if ( chnlIdx == ADC_CHNL_2_IDX)
        {       
          CyDmaTdSetAddress(oscStatus.ADC_DMA_TD[ADC_CHNL_2_IDX], LO16((uint32)ADC_SAR_2_SAR_WRK0_PTR), LO16((uint16_t)oscStatus.sampleBuffAddr[ADC_CHNL_2_IDX]));          
          CyDmaChEnable(oscStatus.ADC_DMA_Chan[ADC_CHNL_2_IDX], 1);        
                            
           /* Re-Enable DMA */
           CyDmaChEnable(oscStatus.ADC_DMA_Chan[ADC_CHNL_2_IDX], 1); 
                        
           /* Enable ADC 2 */
           ADC_SAR_2_Start();        
          
        }            
    }

   /* Enable the ADC timer */
   adcTimerEnable(); 
}

/* At this time is assumed that only time and channel enabling would be reconfigured, 
   but if called, also will restart the capture from the initial of the buffer.
   This last case would be useful when want to start a fresh capture process. */
void reconfigureOscilloscopeChannel( uint32_t sampleTimeNs, uint8_t numChannelsEnabled)
{
   uint8_t chnlIdx; 
   tAdcSampleSetInfo adcSampleSetInfo;
   bool status;
    
   stopOscAdcSamplingProcess(); 
   
   /* Reconfigure the timer with new data and obtain new set parameters */
   status = adcSampleStateRecalculateSetInfo( numChannelsEnabled, sampleTimeNs, &oscStatus.setInfo );
      
   /* Configure the timer using the sample period provided */
   oscStatus.cfgIn.numChannelsEnabled = numChannelsEnabled;       
   oscStatus.cfgIn.sampleTimeNs = sampleTimeNs;    
    
   oscStatus.samplePeriodTicks = oscConvertSampleTime2Ticks( sampleTimeNs );    
   TimerADC_WritePeriod(oscStatus.samplePeriodTicks);
     
   restartOscAdcSamplingProcess();

}

/* Get the number of ticks of the ADC timer in order to get the 
   desired sample time  */
uint32_t oscConvertSampleTime2Ticks(uint32_t sampleTimeNs )
{
    uint32_t ticks;
    uint64_t temp;
    
    temp = (uint64_t)sampleTimeNs*TIMER_ADC_CLOCK_FREQ_MHZ;
    ticks = (uint32_t)(temp/1000);
    ticks = ticks - 1;
    
    /* Limiting the maximum number of ticks to the size of the timer */
    if ( ticks > OSC_MAX_TIMER_TICK)
    {
       ticks = OSC_MAX_TIMER_TICK;
    }
    
    return ticks;  
}

void oscSignalSample(uint32_t samplePeriodTicks, bool reconfigTimer)
{
  reconfigureOscilloscopeChannel(samplePeriodTicks, reconfigTimer);
}

/* This function is expected to be executed periodically, 
   so it can take action when a set of data has been completed 
 */
extern tPlotQueue plotQueue;
void oscContinousProcess()
{
    uint8_t chnlIdx;
    static uint32_t iteration = 0;
    
    if ( oscStatus.activateOscFlag )
    {
       
        /* At set level check if any set is aquired so the values in the set can be converted
           to be ploted.
        */
        
        /* Let´s wait until all the samples are ready */
        if ( isAllSetReadyAllChannels(ADC_SAMPLE_AQUIRED_ST) )
        {
           for (chnlIdx = 0; chnlIdx < oscStatus.cfgIn.numChannelsEnabled; chnlIdx++)
           {
             if (iteration < 5)
             {
             
               printf("@oscContinousProcess() - iteration: %d Chnl: %d [0]: %d  [1]: %d \r\n", 
                      iteration, chnlIdx, adcValues[chnlIdx][0]), adcValues[chnlIdx][1023];   
             }
           }
        
           /* Restart a new iteration of sampling sets */
           restartOscProcess();   
        
        }
        
        
    }

        printf("@oscContinousProcess() - Data of ADC CH1 available : 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x 0x%x\r\n",
              adcValues[ADC_CHNL_1_IDX][0], 
              adcValues[ADC_CHNL_1_IDX][1], 
              adcValues[ADC_CHNL_1_IDX][2],
              adcValues[ADC_CHNL_1_IDX][255],
              adcValues[ADC_CHNL_1_IDX][256],
              adcValues[ADC_CHNL_1_IDX][1022],
              adcValues[ADC_CHNL_1_IDX][1023]
              );
                       
       uint16_t idx;
       /* Move data to plot buffer */ 
       tPlotData *pInputBuff;
       pInputBuff = plotGetDataInPtr();
       printf("@oscContinousProcess() - DataIn addr: 0x%x  \r\n", pInputBuff);
       
       for (idx=0; idx<ADC_NUM_SAMPLES_BUFF_SIZE; idx++)
       {
        pInputBuff[ADC_CHNL_1_IDX] = adcValues[ADC_CHNL_1_IDX][idx];        
        (void)plotWaveAddDataToQueue( pInputBuff, 0x1);
       }
       
        printf("@oscContinousProcess() -  numData: %d rdPtr: %d  wrPtr: %d\r\n",
                plotQueue.numData, plotQueue.rdPtr, plotQueue.wrPtr);
   
       plotWaveplotData(0x1);
       
    
      
    
}

/* Activate the Oscilloscope process */
void activateOscProcess()
{
  if ((prjStatus.oscilInitDone ) && (oscStatus.activateOscFlag == false))
  {
    restartOscAdcSamplingProcess();
    oscStatus.activateOscFlag = true;
  }
}

/* Deactivate the Oscilloscope process */
void deactivateOscProcess()
{  
  if (((prjStatus.oscilInitDone ) && oscStatus.activateOscFlag )) 
  {
     stopOscAdcSamplingProcess();
     oscStatus.activateOscFlag = false;
  }
}

/* [] END OF FILE */

/*******************************************************************************
* File Name: adc1_dma_complete.h
* Version 1.71
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_adc1_dma_complete_H)
#define CY_ISR_adc1_dma_complete_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void adc1_dma_complete_Start(void);
void adc1_dma_complete_StartEx(cyisraddress address);
void adc1_dma_complete_Stop(void);

CY_ISR_PROTO(adc1_dma_complete_Interrupt);

void adc1_dma_complete_SetVector(cyisraddress address);
cyisraddress adc1_dma_complete_GetVector(void);

void adc1_dma_complete_SetPriority(uint8 priority);
uint8 adc1_dma_complete_GetPriority(void);

void adc1_dma_complete_Enable(void);
uint8 adc1_dma_complete_GetState(void);
void adc1_dma_complete_Disable(void);

void adc1_dma_complete_SetPending(void);
void adc1_dma_complete_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the adc1_dma_complete ISR. */
#define adc1_dma_complete_INTC_VECTOR            ((reg32 *) adc1_dma_complete__INTC_VECT)

/* Address of the adc1_dma_complete ISR priority. */
#define adc1_dma_complete_INTC_PRIOR             ((reg8 *) adc1_dma_complete__INTC_PRIOR_REG)

/* Priority of the adc1_dma_complete interrupt. */
#define adc1_dma_complete_INTC_PRIOR_NUMBER      adc1_dma_complete__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable adc1_dma_complete interrupt. */
#define adc1_dma_complete_INTC_SET_EN            ((reg32 *) adc1_dma_complete__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the adc1_dma_complete interrupt. */
#define adc1_dma_complete_INTC_CLR_EN            ((reg32 *) adc1_dma_complete__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the adc1_dma_complete interrupt state to pending. */
#define adc1_dma_complete_INTC_SET_PD            ((reg32 *) adc1_dma_complete__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the adc1_dma_complete interrupt. */
#define adc1_dma_complete_INTC_CLR_PD            ((reg32 *) adc1_dma_complete__INTC_CLR_PD_REG)


#endif /* CY_ISR_adc1_dma_complete_H */


/* [] END OF FILE */

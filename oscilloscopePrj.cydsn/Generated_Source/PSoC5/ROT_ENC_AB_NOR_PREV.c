/*******************************************************************************
* File Name: ROT_ENC_AB_NOR_PREV.c  
* Version 1.90
*
* Description:
*  This file contains API to enable firmware to read the value of a Status 
*  Register.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#include "ROT_ENC_AB_NOR_PREV.h"

#if !defined(ROT_ENC_AB_NOR_PREV_sts_sts_reg__REMOVED) /* Check for removal by optimization */


/*******************************************************************************
* Function Name: ROT_ENC_AB_NOR_PREV_Read
********************************************************************************
*
* Summary:
*  Reads the current value assigned to the Status Register.
*
* Parameters:
*  None.
*
* Return:
*  The current value in the Status Register.
*
*******************************************************************************/
uint8 ROT_ENC_AB_NOR_PREV_Read(void) 
{ 
    return ROT_ENC_AB_NOR_PREV_Status;
}


/*******************************************************************************
* Function Name: ROT_ENC_AB_NOR_PREV_InterruptEnable
********************************************************************************
*
* Summary:
*  Enables the Status Register interrupt.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void ROT_ENC_AB_NOR_PREV_InterruptEnable(void) 
{
    uint8 interruptState;
    interruptState = CyEnterCriticalSection();
    ROT_ENC_AB_NOR_PREV_Status_Aux_Ctrl |= ROT_ENC_AB_NOR_PREV_STATUS_INTR_ENBL;
    CyExitCriticalSection(interruptState);
}


/*******************************************************************************
* Function Name: ROT_ENC_AB_NOR_PREV_InterruptDisable
********************************************************************************
*
* Summary:
*  Disables the Status Register interrupt.
*
* Parameters:
*  None.
*
* Return:
*  None.
*
*******************************************************************************/
void ROT_ENC_AB_NOR_PREV_InterruptDisable(void) 
{
    uint8 interruptState;
    interruptState = CyEnterCriticalSection();
    ROT_ENC_AB_NOR_PREV_Status_Aux_Ctrl &= (uint8)(~ROT_ENC_AB_NOR_PREV_STATUS_INTR_ENBL);
    CyExitCriticalSection(interruptState);
}


/*******************************************************************************
* Function Name: ROT_ENC_AB_NOR_PREV_WriteMask
********************************************************************************
*
* Summary:
*  Writes the current mask value assigned to the Status Register.
*
* Parameters:
*  mask:  Value to write into the mask register.
*
* Return:
*  None.
*
*******************************************************************************/
void ROT_ENC_AB_NOR_PREV_WriteMask(uint8 mask) 
{
    #if(ROT_ENC_AB_NOR_PREV_INPUTS < 8u)
    	mask &= ((uint8)(1u << ROT_ENC_AB_NOR_PREV_INPUTS) - 1u);
	#endif /* End ROT_ENC_AB_NOR_PREV_INPUTS < 8u */
    ROT_ENC_AB_NOR_PREV_Status_Mask = mask;
}


/*******************************************************************************
* Function Name: ROT_ENC_AB_NOR_PREV_ReadMask
********************************************************************************
*
* Summary:
*  Reads the current interrupt mask assigned to the Status Register.
*
* Parameters:
*  None.
*
* Return:
*  The value of the interrupt mask of the Status Register.
*
*******************************************************************************/
uint8 ROT_ENC_AB_NOR_PREV_ReadMask(void) 
{
    return ROT_ENC_AB_NOR_PREV_Status_Mask;
}

#endif /* End check for removal by optimization */


/* [] END OF FILE */

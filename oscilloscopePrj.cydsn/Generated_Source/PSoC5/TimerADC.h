/*******************************************************************************
* File Name: TimerADC.h
* Version 2.80
*
*  Description:
*     Contains the function prototypes and constants available to the timer
*     user module.
*
*   Note:
*     None
*
********************************************************************************
* Copyright 2008-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
********************************************************************************/

#if !defined(CY_TIMER_TimerADC_H)
#define CY_TIMER_TimerADC_H

#include "cytypes.h"
#include "cyfitter.h"
#include "CyLib.h" /* For CyEnterCriticalSection() and CyExitCriticalSection() functions */

extern uint8 TimerADC_initVar;

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component Timer_v2_80 requires cy_boot v3.0 or later
#endif /* (CY_ PSOC5LP) */


/**************************************
*           Parameter Defaults
**************************************/

#define TimerADC_Resolution                 24u
#define TimerADC_UsingFixedFunction         0u
#define TimerADC_UsingHWCaptureCounter      0u
#define TimerADC_SoftwareCaptureMode        0u
#define TimerADC_SoftwareTriggerMode        0u
#define TimerADC_UsingHWEnable              1u
#define TimerADC_EnableTriggerMode          0u
#define TimerADC_InterruptOnCaptureCount    0u
#define TimerADC_RunModeUsed                0u
#define TimerADC_ControlRegRemoved          1u

#if defined(TimerADC_TimerUDB_sCTRLReg_SyncCtl_ctrlreg__CONTROL_REG)
    #define TimerADC_UDB_CONTROL_REG_REMOVED            (0u)
#elif  (TimerADC_UsingFixedFunction)
    #define TimerADC_UDB_CONTROL_REG_REMOVED            (0u)
#else 
    #define TimerADC_UDB_CONTROL_REG_REMOVED            (1u)
#endif /* End TimerADC_TimerUDB_sCTRLReg_SyncCtl_ctrlreg__CONTROL_REG */


/***************************************
*       Type defines
***************************************/


/**************************************************************************
 * Sleep Wakeup Backup structure for Timer Component
 *************************************************************************/
typedef struct
{
    uint8 TimerEnableState;
    #if(!TimerADC_UsingFixedFunction)

        uint32 TimerUdb;
        uint8 InterruptMaskValue;
        #if (TimerADC_UsingHWCaptureCounter)
            uint8 TimerCaptureCounter;
        #endif /* variable declarations for backing up non retention registers in CY_UDB_V1 */

        #if (!TimerADC_UDB_CONTROL_REG_REMOVED)
            uint8 TimerControlRegister;
        #endif /* variable declaration for backing up enable state of the Timer */
    #endif /* define backup variables only for UDB implementation. Fixed function registers are all retention */

}TimerADC_backupStruct;


/***************************************
*       Function Prototypes
***************************************/

void    TimerADC_Start(void) ;
void    TimerADC_Stop(void) ;

void    TimerADC_SetInterruptMode(uint8 interruptMode) ;
uint8   TimerADC_ReadStatusRegister(void) ;
/* Deprecated function. Do not use this in future. Retained for backward compatibility */
#define TimerADC_GetInterruptSource() TimerADC_ReadStatusRegister()

#if(!TimerADC_UDB_CONTROL_REG_REMOVED)
    uint8   TimerADC_ReadControlRegister(void) ;
    void    TimerADC_WriteControlRegister(uint8 control) ;
#endif /* (!TimerADC_UDB_CONTROL_REG_REMOVED) */

uint32  TimerADC_ReadPeriod(void) ;
void    TimerADC_WritePeriod(uint32 period) ;
uint32  TimerADC_ReadCounter(void) ;
void    TimerADC_WriteCounter(uint32 counter) ;
uint32  TimerADC_ReadCapture(void) ;
void    TimerADC_SoftwareCapture(void) ;

#if(!TimerADC_UsingFixedFunction) /* UDB Prototypes */
    #if (TimerADC_SoftwareCaptureMode)
        void    TimerADC_SetCaptureMode(uint8 captureMode) ;
    #endif /* (!TimerADC_UsingFixedFunction) */

    #if (TimerADC_SoftwareTriggerMode)
        void    TimerADC_SetTriggerMode(uint8 triggerMode) ;
    #endif /* (TimerADC_SoftwareTriggerMode) */

    #if (TimerADC_EnableTriggerMode)
        void    TimerADC_EnableTrigger(void) ;
        void    TimerADC_DisableTrigger(void) ;
    #endif /* (TimerADC_EnableTriggerMode) */


    #if(TimerADC_InterruptOnCaptureCount)
        void    TimerADC_SetInterruptCount(uint8 interruptCount) ;
    #endif /* (TimerADC_InterruptOnCaptureCount) */

    #if (TimerADC_UsingHWCaptureCounter)
        void    TimerADC_SetCaptureCount(uint8 captureCount) ;
        uint8   TimerADC_ReadCaptureCount(void) ;
    #endif /* (TimerADC_UsingHWCaptureCounter) */

    void TimerADC_ClearFIFO(void) ;
#endif /* UDB Prototypes */

/* Sleep Retention APIs */
void TimerADC_Init(void)          ;
void TimerADC_Enable(void)        ;
void TimerADC_SaveConfig(void)    ;
void TimerADC_RestoreConfig(void) ;
void TimerADC_Sleep(void)         ;
void TimerADC_Wakeup(void)        ;


/***************************************
*   Enumerated Types and Parameters
***************************************/

/* Enumerated Type B_Timer__CaptureModes, Used in Capture Mode */
#define TimerADC__B_TIMER__CM_NONE 0
#define TimerADC__B_TIMER__CM_RISINGEDGE 1
#define TimerADC__B_TIMER__CM_FALLINGEDGE 2
#define TimerADC__B_TIMER__CM_EITHEREDGE 3
#define TimerADC__B_TIMER__CM_SOFTWARE 4



/* Enumerated Type B_Timer__TriggerModes, Used in Trigger Mode */
#define TimerADC__B_TIMER__TM_NONE 0x00u
#define TimerADC__B_TIMER__TM_RISINGEDGE 0x04u
#define TimerADC__B_TIMER__TM_FALLINGEDGE 0x08u
#define TimerADC__B_TIMER__TM_EITHEREDGE 0x0Cu
#define TimerADC__B_TIMER__TM_SOFTWARE 0x10u


/***************************************
*    Initialial Parameter Constants
***************************************/

#define TimerADC_INIT_PERIOD             1599999u
#define TimerADC_INIT_CAPTURE_MODE       ((uint8)((uint8)0u << TimerADC_CTRL_CAP_MODE_SHIFT))
#define TimerADC_INIT_TRIGGER_MODE       ((uint8)((uint8)0u << TimerADC_CTRL_TRIG_MODE_SHIFT))
#if (TimerADC_UsingFixedFunction)
    #define TimerADC_INIT_INTERRUPT_MODE (((uint8)((uint8)0u << TimerADC_STATUS_TC_INT_MASK_SHIFT)) | \
                                                  ((uint8)((uint8)0 << TimerADC_STATUS_CAPTURE_INT_MASK_SHIFT)))
#else
    #define TimerADC_INIT_INTERRUPT_MODE (((uint8)((uint8)0u << TimerADC_STATUS_TC_INT_MASK_SHIFT)) | \
                                                 ((uint8)((uint8)0 << TimerADC_STATUS_CAPTURE_INT_MASK_SHIFT)) | \
                                                 ((uint8)((uint8)0 << TimerADC_STATUS_FIFOFULL_INT_MASK_SHIFT)))
#endif /* (TimerADC_UsingFixedFunction) */
#define TimerADC_INIT_CAPTURE_COUNT      (2u)
#define TimerADC_INIT_INT_CAPTURE_COUNT  ((uint8)((uint8)(1u - 1u) << TimerADC_CTRL_INTCNT_SHIFT))


/***************************************
*           Registers
***************************************/

#if (TimerADC_UsingFixedFunction) /* Implementation Specific Registers and Register Constants */


    /***************************************
    *    Fixed Function Registers
    ***************************************/

    #define TimerADC_STATUS         (*(reg8 *) TimerADC_TimerHW__SR0 )
    /* In Fixed Function Block Status and Mask are the same register */
    #define TimerADC_STATUS_MASK    (*(reg8 *) TimerADC_TimerHW__SR0 )
    #define TimerADC_CONTROL        (*(reg8 *) TimerADC_TimerHW__CFG0)
    #define TimerADC_CONTROL2       (*(reg8 *) TimerADC_TimerHW__CFG1)
    #define TimerADC_CONTROL2_PTR   ( (reg8 *) TimerADC_TimerHW__CFG1)
    #define TimerADC_RT1            (*(reg8 *) TimerADC_TimerHW__RT1)
    #define TimerADC_RT1_PTR        ( (reg8 *) TimerADC_TimerHW__RT1)

    #if (CY_PSOC3 || CY_PSOC5LP)
        #define TimerADC_CONTROL3       (*(reg8 *) TimerADC_TimerHW__CFG2)
        #define TimerADC_CONTROL3_PTR   ( (reg8 *) TimerADC_TimerHW__CFG2)
    #endif /* (CY_PSOC3 || CY_PSOC5LP) */
    #define TimerADC_GLOBAL_ENABLE  (*(reg8 *) TimerADC_TimerHW__PM_ACT_CFG)
    #define TimerADC_GLOBAL_STBY_ENABLE  (*(reg8 *) TimerADC_TimerHW__PM_STBY_CFG)

    #define TimerADC_CAPTURE_LSB         (* (reg16 *) TimerADC_TimerHW__CAP0 )
    #define TimerADC_CAPTURE_LSB_PTR       ((reg16 *) TimerADC_TimerHW__CAP0 )
    #define TimerADC_PERIOD_LSB          (* (reg16 *) TimerADC_TimerHW__PER0 )
    #define TimerADC_PERIOD_LSB_PTR        ((reg16 *) TimerADC_TimerHW__PER0 )
    #define TimerADC_COUNTER_LSB         (* (reg16 *) TimerADC_TimerHW__CNT_CMP0 )
    #define TimerADC_COUNTER_LSB_PTR       ((reg16 *) TimerADC_TimerHW__CNT_CMP0 )


    /***************************************
    *    Register Constants
    ***************************************/

    /* Fixed Function Block Chosen */
    #define TimerADC_BLOCK_EN_MASK                     TimerADC_TimerHW__PM_ACT_MSK
    #define TimerADC_BLOCK_STBY_EN_MASK                TimerADC_TimerHW__PM_STBY_MSK

    /* Control Register Bit Locations */
    /* Interrupt Count - Not valid for Fixed Function Block */
    #define TimerADC_CTRL_INTCNT_SHIFT                  0x00u
    /* Trigger Polarity - Not valid for Fixed Function Block */
    #define TimerADC_CTRL_TRIG_MODE_SHIFT               0x00u
    /* Trigger Enable - Not valid for Fixed Function Block */
    #define TimerADC_CTRL_TRIG_EN_SHIFT                 0x00u
    /* Capture Polarity - Not valid for Fixed Function Block */
    #define TimerADC_CTRL_CAP_MODE_SHIFT                0x00u
    /* Timer Enable - As defined in Register Map, part of TMRX_CFG0 register */
    #define TimerADC_CTRL_ENABLE_SHIFT                  0x00u

    /* Control Register Bit Masks */
    #define TimerADC_CTRL_ENABLE                        ((uint8)((uint8)0x01u << TimerADC_CTRL_ENABLE_SHIFT))

    /* Control2 Register Bit Masks */
    /* As defined in Register Map, Part of the TMRX_CFG1 register */
    #define TimerADC_CTRL2_IRQ_SEL_SHIFT                 0x00u
    #define TimerADC_CTRL2_IRQ_SEL                      ((uint8)((uint8)0x01u << TimerADC_CTRL2_IRQ_SEL_SHIFT))

    #if (CY_PSOC5A)
        /* Use CFG1 Mode bits to set run mode */
        /* As defined by Verilog Implementation */
        #define TimerADC_CTRL_MODE_SHIFT                 0x01u
        #define TimerADC_CTRL_MODE_MASK                 ((uint8)((uint8)0x07u << TimerADC_CTRL_MODE_SHIFT))
    #endif /* (CY_PSOC5A) */
    #if (CY_PSOC3 || CY_PSOC5LP)
        /* Control3 Register Bit Locations */
        #define TimerADC_CTRL_RCOD_SHIFT        0x02u
        #define TimerADC_CTRL_ENBL_SHIFT        0x00u
        #define TimerADC_CTRL_MODE_SHIFT        0x00u

        /* Control3 Register Bit Masks */
        #define TimerADC_CTRL_RCOD_MASK  ((uint8)((uint8)0x03u << TimerADC_CTRL_RCOD_SHIFT)) /* ROD and COD bit masks */
        #define TimerADC_CTRL_ENBL_MASK  ((uint8)((uint8)0x80u << TimerADC_CTRL_ENBL_SHIFT)) /* HW_EN bit mask */
        #define TimerADC_CTRL_MODE_MASK  ((uint8)((uint8)0x03u << TimerADC_CTRL_MODE_SHIFT)) /* Run mode bit mask */

        #define TimerADC_CTRL_RCOD       ((uint8)((uint8)0x03u << TimerADC_CTRL_RCOD_SHIFT))
        #define TimerADC_CTRL_ENBL       ((uint8)((uint8)0x80u << TimerADC_CTRL_ENBL_SHIFT))
    #endif /* (CY_PSOC3 || CY_PSOC5LP) */

    /*RT1 Synch Constants: Applicable for PSoC3 and PSoC5LP */
    #define TimerADC_RT1_SHIFT                       0x04u
    /* Sync TC and CMP bit masks */
    #define TimerADC_RT1_MASK                        ((uint8)((uint8)0x03u << TimerADC_RT1_SHIFT))
    #define TimerADC_SYNC                            ((uint8)((uint8)0x03u << TimerADC_RT1_SHIFT))
    #define TimerADC_SYNCDSI_SHIFT                   0x00u
    /* Sync all DSI inputs with Mask  */
    #define TimerADC_SYNCDSI_MASK                    ((uint8)((uint8)0x0Fu << TimerADC_SYNCDSI_SHIFT))
    /* Sync all DSI inputs */
    #define TimerADC_SYNCDSI_EN                      ((uint8)((uint8)0x0Fu << TimerADC_SYNCDSI_SHIFT))

    #define TimerADC_CTRL_MODE_PULSEWIDTH            ((uint8)((uint8)0x01u << TimerADC_CTRL_MODE_SHIFT))
    #define TimerADC_CTRL_MODE_PERIOD                ((uint8)((uint8)0x02u << TimerADC_CTRL_MODE_SHIFT))
    #define TimerADC_CTRL_MODE_CONTINUOUS            ((uint8)((uint8)0x00u << TimerADC_CTRL_MODE_SHIFT))

    /* Status Register Bit Locations */
    /* As defined in Register Map, part of TMRX_SR0 register */
    #define TimerADC_STATUS_TC_SHIFT                 0x07u
    /* As defined in Register Map, part of TMRX_SR0 register, Shared with Compare Status */
    #define TimerADC_STATUS_CAPTURE_SHIFT            0x06u
    /* As defined in Register Map, part of TMRX_SR0 register */
    #define TimerADC_STATUS_TC_INT_MASK_SHIFT        (TimerADC_STATUS_TC_SHIFT - 0x04u)
    /* As defined in Register Map, part of TMRX_SR0 register, Shared with Compare Status */
    #define TimerADC_STATUS_CAPTURE_INT_MASK_SHIFT   (TimerADC_STATUS_CAPTURE_SHIFT - 0x04u)

    /* Status Register Bit Masks */
    #define TimerADC_STATUS_TC                       ((uint8)((uint8)0x01u << TimerADC_STATUS_TC_SHIFT))
    #define TimerADC_STATUS_CAPTURE                  ((uint8)((uint8)0x01u << TimerADC_STATUS_CAPTURE_SHIFT))
    /* Interrupt Enable Bit-Mask for interrupt on TC */
    #define TimerADC_STATUS_TC_INT_MASK              ((uint8)((uint8)0x01u << TimerADC_STATUS_TC_INT_MASK_SHIFT))
    /* Interrupt Enable Bit-Mask for interrupt on Capture */
    #define TimerADC_STATUS_CAPTURE_INT_MASK         ((uint8)((uint8)0x01u << TimerADC_STATUS_CAPTURE_INT_MASK_SHIFT))

#else   /* UDB Registers and Register Constants */


    /***************************************
    *           UDB Registers
    ***************************************/

    #define TimerADC_STATUS              (* (reg8 *) TimerADC_TimerUDB_rstSts_stsreg__STATUS_REG )
    #define TimerADC_STATUS_MASK         (* (reg8 *) TimerADC_TimerUDB_rstSts_stsreg__MASK_REG)
    #define TimerADC_STATUS_AUX_CTRL     (* (reg8 *) TimerADC_TimerUDB_rstSts_stsreg__STATUS_AUX_CTL_REG)
    #define TimerADC_CONTROL             (* (reg8 *) TimerADC_TimerUDB_sCTRLReg_SyncCtl_ctrlreg__CONTROL_REG )
    
    #if(TimerADC_Resolution <= 8u) /* 8-bit Timer */
        #define TimerADC_CAPTURE_LSB         (* (reg8 *) TimerADC_TimerUDB_sT24_timerdp_u0__F0_REG )
        #define TimerADC_CAPTURE_LSB_PTR       ((reg8 *) TimerADC_TimerUDB_sT24_timerdp_u0__F0_REG )
        #define TimerADC_PERIOD_LSB          (* (reg8 *) TimerADC_TimerUDB_sT24_timerdp_u0__D0_REG )
        #define TimerADC_PERIOD_LSB_PTR        ((reg8 *) TimerADC_TimerUDB_sT24_timerdp_u0__D0_REG )
        #define TimerADC_COUNTER_LSB         (* (reg8 *) TimerADC_TimerUDB_sT24_timerdp_u0__A0_REG )
        #define TimerADC_COUNTER_LSB_PTR       ((reg8 *) TimerADC_TimerUDB_sT24_timerdp_u0__A0_REG )
    #elif(TimerADC_Resolution <= 16u) /* 8-bit Timer */
        #if(CY_PSOC3) /* 8-bit addres space */
            #define TimerADC_CAPTURE_LSB         (* (reg16 *) TimerADC_TimerUDB_sT24_timerdp_u0__F0_REG )
            #define TimerADC_CAPTURE_LSB_PTR       ((reg16 *) TimerADC_TimerUDB_sT24_timerdp_u0__F0_REG )
            #define TimerADC_PERIOD_LSB          (* (reg16 *) TimerADC_TimerUDB_sT24_timerdp_u0__D0_REG )
            #define TimerADC_PERIOD_LSB_PTR        ((reg16 *) TimerADC_TimerUDB_sT24_timerdp_u0__D0_REG )
            #define TimerADC_COUNTER_LSB         (* (reg16 *) TimerADC_TimerUDB_sT24_timerdp_u0__A0_REG )
            #define TimerADC_COUNTER_LSB_PTR       ((reg16 *) TimerADC_TimerUDB_sT24_timerdp_u0__A0_REG )
        #else /* 16-bit address space */
            #define TimerADC_CAPTURE_LSB         (* (reg16 *) TimerADC_TimerUDB_sT24_timerdp_u0__16BIT_F0_REG )
            #define TimerADC_CAPTURE_LSB_PTR       ((reg16 *) TimerADC_TimerUDB_sT24_timerdp_u0__16BIT_F0_REG )
            #define TimerADC_PERIOD_LSB          (* (reg16 *) TimerADC_TimerUDB_sT24_timerdp_u0__16BIT_D0_REG )
            #define TimerADC_PERIOD_LSB_PTR        ((reg16 *) TimerADC_TimerUDB_sT24_timerdp_u0__16BIT_D0_REG )
            #define TimerADC_COUNTER_LSB         (* (reg16 *) TimerADC_TimerUDB_sT24_timerdp_u0__16BIT_A0_REG )
            #define TimerADC_COUNTER_LSB_PTR       ((reg16 *) TimerADC_TimerUDB_sT24_timerdp_u0__16BIT_A0_REG )
        #endif /* CY_PSOC3 */
    #elif(TimerADC_Resolution <= 24u)/* 24-bit Timer */
        #define TimerADC_CAPTURE_LSB         (* (reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__F0_REG )
        #define TimerADC_CAPTURE_LSB_PTR       ((reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__F0_REG )
        #define TimerADC_PERIOD_LSB          (* (reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__D0_REG )
        #define TimerADC_PERIOD_LSB_PTR        ((reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__D0_REG )
        #define TimerADC_COUNTER_LSB         (* (reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__A0_REG )
        #define TimerADC_COUNTER_LSB_PTR       ((reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__A0_REG )
    #else /* 32-bit Timer */
        #if(CY_PSOC3 || CY_PSOC5) /* 8-bit address space */
            #define TimerADC_CAPTURE_LSB         (* (reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__F0_REG )
            #define TimerADC_CAPTURE_LSB_PTR       ((reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__F0_REG )
            #define TimerADC_PERIOD_LSB          (* (reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__D0_REG )
            #define TimerADC_PERIOD_LSB_PTR        ((reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__D0_REG )
            #define TimerADC_COUNTER_LSB         (* (reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__A0_REG )
            #define TimerADC_COUNTER_LSB_PTR       ((reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__A0_REG )
        #else /* 32-bit address space */
            #define TimerADC_CAPTURE_LSB         (* (reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__32BIT_F0_REG )
            #define TimerADC_CAPTURE_LSB_PTR       ((reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__32BIT_F0_REG )
            #define TimerADC_PERIOD_LSB          (* (reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__32BIT_D0_REG )
            #define TimerADC_PERIOD_LSB_PTR        ((reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__32BIT_D0_REG )
            #define TimerADC_COUNTER_LSB         (* (reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__32BIT_A0_REG )
            #define TimerADC_COUNTER_LSB_PTR       ((reg32 *) TimerADC_TimerUDB_sT24_timerdp_u0__32BIT_A0_REG )
        #endif /* CY_PSOC3 || CY_PSOC5 */ 
    #endif

    #define TimerADC_COUNTER_LSB_PTR_8BIT       ((reg8 *) TimerADC_TimerUDB_sT24_timerdp_u0__A0_REG )
    
    #if (TimerADC_UsingHWCaptureCounter)
        #define TimerADC_CAP_COUNT              (*(reg8 *) TimerADC_TimerUDB_sCapCount_counter__PERIOD_REG )
        #define TimerADC_CAP_COUNT_PTR          ( (reg8 *) TimerADC_TimerUDB_sCapCount_counter__PERIOD_REG )
        #define TimerADC_CAPTURE_COUNT_CTRL     (*(reg8 *) TimerADC_TimerUDB_sCapCount_counter__CONTROL_AUX_CTL_REG )
        #define TimerADC_CAPTURE_COUNT_CTRL_PTR ( (reg8 *) TimerADC_TimerUDB_sCapCount_counter__CONTROL_AUX_CTL_REG )
    #endif /* (TimerADC_UsingHWCaptureCounter) */


    /***************************************
    *       Register Constants
    ***************************************/

    /* Control Register Bit Locations */
    #define TimerADC_CTRL_INTCNT_SHIFT              0x00u       /* As defined by Verilog Implementation */
    #define TimerADC_CTRL_TRIG_MODE_SHIFT           0x02u       /* As defined by Verilog Implementation */
    #define TimerADC_CTRL_TRIG_EN_SHIFT             0x04u       /* As defined by Verilog Implementation */
    #define TimerADC_CTRL_CAP_MODE_SHIFT            0x05u       /* As defined by Verilog Implementation */
    #define TimerADC_CTRL_ENABLE_SHIFT              0x07u       /* As defined by Verilog Implementation */

    /* Control Register Bit Masks */
    #define TimerADC_CTRL_INTCNT_MASK               ((uint8)((uint8)0x03u << TimerADC_CTRL_INTCNT_SHIFT))
    #define TimerADC_CTRL_TRIG_MODE_MASK            ((uint8)((uint8)0x03u << TimerADC_CTRL_TRIG_MODE_SHIFT))
    #define TimerADC_CTRL_TRIG_EN                   ((uint8)((uint8)0x01u << TimerADC_CTRL_TRIG_EN_SHIFT))
    #define TimerADC_CTRL_CAP_MODE_MASK             ((uint8)((uint8)0x03u << TimerADC_CTRL_CAP_MODE_SHIFT))
    #define TimerADC_CTRL_ENABLE                    ((uint8)((uint8)0x01u << TimerADC_CTRL_ENABLE_SHIFT))

    /* Bit Counter (7-bit) Control Register Bit Definitions */
    /* As defined by the Register map for the AUX Control Register */
    #define TimerADC_CNTR_ENABLE                    0x20u

    /* Status Register Bit Locations */
    #define TimerADC_STATUS_TC_SHIFT                0x00u  /* As defined by Verilog Implementation */
    #define TimerADC_STATUS_CAPTURE_SHIFT           0x01u  /* As defined by Verilog Implementation */
    #define TimerADC_STATUS_TC_INT_MASK_SHIFT       TimerADC_STATUS_TC_SHIFT
    #define TimerADC_STATUS_CAPTURE_INT_MASK_SHIFT  TimerADC_STATUS_CAPTURE_SHIFT
    #define TimerADC_STATUS_FIFOFULL_SHIFT          0x02u  /* As defined by Verilog Implementation */
    #define TimerADC_STATUS_FIFONEMP_SHIFT          0x03u  /* As defined by Verilog Implementation */
    #define TimerADC_STATUS_FIFOFULL_INT_MASK_SHIFT TimerADC_STATUS_FIFOFULL_SHIFT

    /* Status Register Bit Masks */
    /* Sticky TC Event Bit-Mask */
    #define TimerADC_STATUS_TC                      ((uint8)((uint8)0x01u << TimerADC_STATUS_TC_SHIFT))
    /* Sticky Capture Event Bit-Mask */
    #define TimerADC_STATUS_CAPTURE                 ((uint8)((uint8)0x01u << TimerADC_STATUS_CAPTURE_SHIFT))
    /* Interrupt Enable Bit-Mask */
    #define TimerADC_STATUS_TC_INT_MASK             ((uint8)((uint8)0x01u << TimerADC_STATUS_TC_SHIFT))
    /* Interrupt Enable Bit-Mask */
    #define TimerADC_STATUS_CAPTURE_INT_MASK        ((uint8)((uint8)0x01u << TimerADC_STATUS_CAPTURE_SHIFT))
    /* NOT-Sticky FIFO Full Bit-Mask */
    #define TimerADC_STATUS_FIFOFULL                ((uint8)((uint8)0x01u << TimerADC_STATUS_FIFOFULL_SHIFT))
    /* NOT-Sticky FIFO Not Empty Bit-Mask */
    #define TimerADC_STATUS_FIFONEMP                ((uint8)((uint8)0x01u << TimerADC_STATUS_FIFONEMP_SHIFT))
    /* Interrupt Enable Bit-Mask */
    #define TimerADC_STATUS_FIFOFULL_INT_MASK       ((uint8)((uint8)0x01u << TimerADC_STATUS_FIFOFULL_SHIFT))

    #define TimerADC_STATUS_ACTL_INT_EN             0x10u   /* As defined for the ACTL Register */

    /* Datapath Auxillary Control Register definitions */
    #define TimerADC_AUX_CTRL_FIFO0_CLR             0x01u   /* As defined by Register map */
    #define TimerADC_AUX_CTRL_FIFO1_CLR             0x02u   /* As defined by Register map */
    #define TimerADC_AUX_CTRL_FIFO0_LVL             0x04u   /* As defined by Register map */
    #define TimerADC_AUX_CTRL_FIFO1_LVL             0x08u   /* As defined by Register map */
    #define TimerADC_STATUS_ACTL_INT_EN_MASK        0x10u   /* As defined for the ACTL Register */

#endif /* Implementation Specific Registers and Register Constants */

#endif  /* CY_TIMER_TimerADC_H */


/* [] END OF FILE */

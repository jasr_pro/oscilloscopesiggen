/*******************************************************************************
* File Name: project.h
* 
* PSoC Creator  4.4
*
* Description:
* It contains references to all generated header files and should not be modified.
* This file is automatically generated by PSoC Creator.
*
********************************************************************************
* Copyright (c) 2007-2020 Cypress Semiconductor.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
********************************************************************************/

#include "cyfitter_cfg.h"
#include "cydevice.h"
#include "cydevice_trm.h"
#include "cyfitter.h"
#include "cydisabledsheets.h"
#include "LCD_RD_aliases.h"
#include "LCD_RD.h"
#include "LCD_DATA_aliases.h"
#include "LCD_DATA.h"
#include "LCD_RS_aliases.h"
#include "LCD_RS.h"
#include "LCD_WR_aliases.h"
#include "LCD_WR.h"
#include "LCD_CS_aliases.h"
#include "LCD_CS.h"
#include "LCD_RST_aliases.h"
#include "LCD_RST.h"
#include "UART_1.h"
#include "PRS_1.h"
#include "Clock_1.h"
#include "TesOutPin_aliases.h"
#include "TesOutPin.h"
#include "TimerISR_1.h"
#include "Timer_1.h"
#include "TestOutPin2_aliases.h"
#include "TestOutPin2.h"
#include "ROT_ENC_SW_aliases.h"
#include "ROT_ENC_SW.h"
#include "ROT_ENC_A_aliases.h"
#include "ROT_ENC_A.h"
#include "ROT_ENC_B_aliases.h"
#include "ROT_ENC_B.h"
#include "ROT_ENC_ISR_A.h"
#include "ENC_CLK.h"
#include "ROT_ENC_ISR_B.h"
#include "ROT_ENC_ISR_SW.h"
#include "ROT_ENC_AB_NOR_PREV.h"
#include "Button1_aliases.h"
#include "Button1.h"
#include "SigGenClk.h"
#include "TimerSigGen.h"
#include "Clock_2.h"
#include "ADC2_complete.h"
#include "DacPin_aliases.h"
#include "DacPin.h"
#include "VDAC8_SigGen.h"
#include "DMA_SigGen_dma.h"
#include "SampleClock_1.h"
#include "TimerADC.h"
#include "Pin_adc1_aliases.h"
#include "Pin_adc1.h"
#include "Pin_adc2_aliases.h"
#include "Pin_adc2.h"
#include "ADC_SAR_1.h"
#include "ADC1_complete.h"
#include "ADC_DMA_CH1_dma.h"
#include "adc1_dma_complete.h"
#include "DevicesCtrlReg.h"
#include "SqrDigOut_aliases.h"
#include "SqrDigOut.h"
#include "Button2_aliases.h"
#include "Button2.h"
#include "ADC_SAR_2.h"
#include "ADC_DMA_CH2_dma.h"
#include "adc2_dma_complete.h"
#include "UART_1_IntClock.h"
#include "ADC_SAR_1_IRQ.h"
#include "ADC_SAR_1_theACLK.h"
#include "ADC_SAR_1_Bypass_aliases.h"
#include "ADC_SAR_1_Bypass.h"
#include "ADC_SAR_2_IRQ.h"
#include "ADC_SAR_2_theACLK.h"
#include "ADC_SAR_2_Bypass_aliases.h"
#include "ADC_SAR_2_Bypass.h"
#include "core_cm3_psoc5.h"
#include "CyDmac.h"
#include "CyFlash.h"
#include "CyLib.h"
#include "cypins.h"
#include "cyPm.h"
#include "CySpc.h"
#include "cytypes.h"
#include "cy_em_eeprom.h"

/*[]*/


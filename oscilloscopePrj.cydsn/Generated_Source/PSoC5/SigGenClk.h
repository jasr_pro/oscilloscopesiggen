/*******************************************************************************
* File Name: SigGenClk.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_SigGenClk_H)
#define CY_CLOCK_SigGenClk_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
* Conditional Compilation Parameters
***************************************/

/* Check to see if required defines such as CY_PSOC5LP are available */
/* They are defined starting with cy_boot v3.0 */
#if !defined (CY_PSOC5LP)
    #error Component cy_clock_v2_20 requires cy_boot v3.0 or later
#endif /* (CY_PSOC5LP) */


/***************************************
*        Function Prototypes
***************************************/

void SigGenClk_Start(void) ;
void SigGenClk_Stop(void) ;

#if(CY_PSOC3 || CY_PSOC5LP)
void SigGenClk_StopBlock(void) ;
#endif /* (CY_PSOC3 || CY_PSOC5LP) */

void SigGenClk_StandbyPower(uint8 state) ;
void SigGenClk_SetDividerRegister(uint16 clkDivider, uint8 restart) 
                                ;
uint16 SigGenClk_GetDividerRegister(void) ;
void SigGenClk_SetModeRegister(uint8 modeBitMask) ;
void SigGenClk_ClearModeRegister(uint8 modeBitMask) ;
uint8 SigGenClk_GetModeRegister(void) ;
void SigGenClk_SetSourceRegister(uint8 clkSource) ;
uint8 SigGenClk_GetSourceRegister(void) ;
#if defined(SigGenClk__CFG3)
void SigGenClk_SetPhaseRegister(uint8 clkPhase) ;
uint8 SigGenClk_GetPhaseRegister(void) ;
#endif /* defined(SigGenClk__CFG3) */

#define SigGenClk_Enable()                       SigGenClk_Start()
#define SigGenClk_Disable()                      SigGenClk_Stop()
#define SigGenClk_SetDivider(clkDivider)         SigGenClk_SetDividerRegister(clkDivider, 1u)
#define SigGenClk_SetDividerValue(clkDivider)    SigGenClk_SetDividerRegister((clkDivider) - 1u, 1u)
#define SigGenClk_SetMode(clkMode)               SigGenClk_SetModeRegister(clkMode)
#define SigGenClk_SetSource(clkSource)           SigGenClk_SetSourceRegister(clkSource)
#if defined(SigGenClk__CFG3)
#define SigGenClk_SetPhase(clkPhase)             SigGenClk_SetPhaseRegister(clkPhase)
#define SigGenClk_SetPhaseValue(clkPhase)        SigGenClk_SetPhaseRegister((clkPhase) + 1u)
#endif /* defined(SigGenClk__CFG3) */


/***************************************
*             Registers
***************************************/

/* Register to enable or disable the clock */
#define SigGenClk_CLKEN              (* (reg8 *) SigGenClk__PM_ACT_CFG)
#define SigGenClk_CLKEN_PTR          ((reg8 *) SigGenClk__PM_ACT_CFG)

/* Register to enable or disable the clock */
#define SigGenClk_CLKSTBY            (* (reg8 *) SigGenClk__PM_STBY_CFG)
#define SigGenClk_CLKSTBY_PTR        ((reg8 *) SigGenClk__PM_STBY_CFG)

/* Clock LSB divider configuration register. */
#define SigGenClk_DIV_LSB            (* (reg8 *) SigGenClk__CFG0)
#define SigGenClk_DIV_LSB_PTR        ((reg8 *) SigGenClk__CFG0)
#define SigGenClk_DIV_PTR            ((reg16 *) SigGenClk__CFG0)

/* Clock MSB divider configuration register. */
#define SigGenClk_DIV_MSB            (* (reg8 *) SigGenClk__CFG1)
#define SigGenClk_DIV_MSB_PTR        ((reg8 *) SigGenClk__CFG1)

/* Mode and source configuration register */
#define SigGenClk_MOD_SRC            (* (reg8 *) SigGenClk__CFG2)
#define SigGenClk_MOD_SRC_PTR        ((reg8 *) SigGenClk__CFG2)

#if defined(SigGenClk__CFG3)
/* Analog clock phase configuration register */
#define SigGenClk_PHASE              (* (reg8 *) SigGenClk__CFG3)
#define SigGenClk_PHASE_PTR          ((reg8 *) SigGenClk__CFG3)
#endif /* defined(SigGenClk__CFG3) */


/**************************************
*       Register Constants
**************************************/

/* Power manager register masks */
#define SigGenClk_CLKEN_MASK         SigGenClk__PM_ACT_MSK
#define SigGenClk_CLKSTBY_MASK       SigGenClk__PM_STBY_MSK

/* CFG2 field masks */
#define SigGenClk_SRC_SEL_MSK        SigGenClk__CFG2_SRC_SEL_MASK
#define SigGenClk_MODE_MASK          (~(SigGenClk_SRC_SEL_MSK))

#if defined(SigGenClk__CFG3)
/* CFG3 phase mask */
#define SigGenClk_PHASE_MASK         SigGenClk__CFG3_PHASE_DLY_MASK
#endif /* defined(SigGenClk__CFG3) */

#endif /* CY_CLOCK_SigGenClk_H */


/* [] END OF FILE */

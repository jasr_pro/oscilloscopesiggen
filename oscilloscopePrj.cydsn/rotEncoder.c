/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *  Description: This is a driver for a switch encoder.
 *  This detects if the rotor encode was turned to the 
 *  right or left and also if the switch button was 
 *  pressed. In the 3 cases there is a status flag and 
 *  a counter. The counter is increased in every event
 *  (triggered by interrupt and limited to 255). The
 *  In every event the flag starus for the respective
 *  event is set. The status flag can be read with
 *  getEncoderStatus(). clearEcondedStatus() is used
 *  to try to clear the status flags. Every time this
 *  is called, the counter corresponding to the event
 *  indicated by the parameter will be decremented and
 *  if this gets to 0, then the flag for that event will
 *  be cleared.  
 * ========================================
*/

#include "ROT_ENC_ISR_A.h"
#include "ROT_ENC_ISR_B.h"
#include "ROT_ENC_ISR_SW.h"
#include "ROT_ENC_AB_NOR_PREV.h"
#include "psocUtilities.h"
#include "rotEncoder.h"

#define MAX_RIGHT_CNT 0xFF
#define MAX_LEFT_CNT 0xFF
#define MAX_SW_PRESS_CNT 0xFF

uint8_t rotEncFlagSt = 0;

static uint8_t rightCnt = 0;
static uint8_t leftCnt = 0;
static uint8_t swPressCnt = 0;

CY_ISR(rotEncoderIsrAHandler)
{
    if (ROT_ENC_ISR_A_GetState() == 1U)
    {        
       if (ROT_ENC_AB_NOR_PREV_Read() == 1U)
       {
           rotEncFlagSt |= ROT_ENC_TURN_RIGHT_FLAG_MASK;
           if ( rightCnt != MAX_RIGHT_CNT)
           {
               rightCnt++;
           }
       }
       ROT_ENC_ISR_A_ClearPending();
    }
}

CY_ISR(rotEncoderIsrBHandler)
{ 
    if (ROT_ENC_ISR_B_GetState() == 1U)
    {         
       if (ROT_ENC_AB_NOR_PREV_Read() == 1U)
       {
          rotEncFlagSt |= ROT_ENC_TURN_LEFT_FLAG_MASK;
          if ( leftCnt != MAX_LEFT_CNT)
           {
               leftCnt++;
           }          
       }
       ROT_ENC_ISR_B_ClearPending();
    }
}

CY_ISR(rotEncoderIsrSWHandler)
{
    if (ROT_ENC_ISR_SW_GetState()== 1U)
    {
        rotEncFlagSt |= ROT_ENC_SWITCH_PRESS_FLAG_MASK;
        if (swPressCnt != MAX_SW_PRESS_CNT)
        {
            swPressCnt++;
        }
        ROT_ENC_ISR_SW_ClearPending();
    }
}


void rotEncoderStart(void)
{
    ROT_ENC_ISR_A_StartEx(rotEncoderIsrAHandler);
    ROT_ENC_ISR_B_StartEx(rotEncoderIsrBHandler);
    ROT_ENC_ISR_SW_StartEx(rotEncoderIsrSWHandler);
    rightCnt = 0;
    leftCnt = 0;
    swPressCnt = 0;
}

uint8_t getEncoderStatus(void)
{
   return rotEncFlagSt;
}

/* Get Right turn flag status*/
bool isSetRotEncoderRightFlag(void)
{
   bool flag;
   flag = ((rotEncFlagSt & ROT_ENC_TURN_RIGHT_FLAG_MASK) ==
             ROT_ENC_TURN_RIGHT_FLAG_MASK) ? true : false;
   return flag; 
}

/* Get Left turn flag status */
bool isSetRotEncoderLeftFlag(void)
{
   bool flag;
   flag = ((rotEncFlagSt & ROT_ENC_TURN_LEFT_FLAG_MASK) ==
             ROT_ENC_TURN_LEFT_FLAG_MASK) ? true : false;
   return flag; 
}

/* Get switch status flag */
bool isSetRotEncoderSwitchFlag(void)
{
   bool flag;
   flag = ((rotEncFlagSt & ROT_ENC_SWITCH_PRESS_FLAG_MASK) ==
            ROT_ENC_SWITCH_PRESS_FLAG_MASK ) ? true : false;
   return flag;
}

/* This will try to clear the flag status indicator for the event
   indicated by the parameter. The counter for the event will be
   decremented and in case this gets the value of 0, the flag will
   be cleared.
*/
void clearEncodedStatus(uint8_t stFlag)
{
    uint8_t clearMask;
    stFlag &= ROT_ENC_FLAG_MASK; /* Make sure than only valid masks are set */
    
    if ( stFlag & ROT_ENC_TURN_RIGHT_FLAG_MASK )
    { 
        if (rightCnt != 0)
        { 
            rightCnt--;
        }
        /*Only clear the flag if the counter is 0 */
        if ( rightCnt == 0)
        {
            clearMask = ~((uint8_t)ROT_ENC_TURN_RIGHT_FLAG_MASK);
            rotEncFlagSt &= clearMask; 
        }        
    }
    
    if ( stFlag & ROT_ENC_TURN_LEFT_FLAG_MASK )
    { 
        if (leftCnt != 0)
        { 
            leftCnt--;
        }
        /*Only clear the flag if the counter is 0 */
        if ( leftCnt == 0)
        {
            clearMask = ~((uint8_t)ROT_ENC_TURN_LEFT_FLAG_MASK);
            rotEncFlagSt &= clearMask; 
        }        
    }
    
    if ( stFlag & ROT_ENC_SWITCH_PRESS_FLAG_MASK )
    { 
        if (swPressCnt != 0)
        { 
            swPressCnt--;
        }
        /*Only clear the flag if the counter is 0 */
        if ( swPressCnt == 0)
        {
            clearMask = ~((uint8_t)ROT_ENC_SWITCH_PRESS_FLAG_MASK);
            rotEncFlagSt &= clearMask; 
        }
        
    }
}

/* This function will clear the counter and status flag
   for the event indicated in the input parameter 
*/
void clearEncodedStatusAll(uint8_t stFlag)
{
    uint8_t clearMask;
    
    stFlag &= ROT_ENC_FLAG_MASK; /* Make sure than only valid masks are set */
    if ( stFlag & ROT_ENC_TURN_RIGHT_FLAG_MASK )
    {
       clearMask = ~((uint8_t)ROT_ENC_TURN_RIGHT_FLAG_MASK);
       rotEncFlagSt &= clearMask;
       rightCnt = 0;
    }
    
    if ( stFlag & ROT_ENC_TURN_LEFT_FLAG_MASK )
    {
       clearMask = ~((uint8_t)ROT_ENC_TURN_LEFT_FLAG_MASK);
       rotEncFlagSt &= clearMask;
       leftCnt = 0;
    }
    
    if ( stFlag & ROT_ENC_SWITCH_PRESS_FLAG_MASK )
    {
        clearMask = ~((uint8_t)ROT_ENC_SWITCH_PRESS_FLAG_MASK);
        rotEncFlagSt &= clearMask;
        swPressCnt = 0;
    }
}

/* Get the current counter value of the right rotary encoder */
uint8_t getRotRightCount()
{
    return rightCnt;
}

/* Get the current counter value of the left rotary encoder */
uint8_t getRotLeftCount()
{
    return leftCnt;
}

uint8_t getSWCount()
{
    return swPressCnt;
}

/* [] END OF FILE */

/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef LCD_MCU_FRIEND_9342_WRAP_H
            
#define LCD_MCU_FRIEND_9342_WRAP_H
    
#include "psocUtilities.h"
#include "LCD_CS.h"
#include "LCD_RD.h"
#include "LCD_RS.h"
#include "LCD_RST.h"
#include "LCD_WR.h"
#include "LCD_DATA.h"

#if 0
#define RESET_SET LCD_RST_Write(0x1); 
#define RESET_CLEAR LCD_RST_Write(0x0);
#define CS_SET LCD_CS_Write(0x1);
#define CS_CLEAR LCD_CS_Write(0x0);
#define DATA_CMD_SET LCD_RS_Write(0x1);
#define DATA_CMD_CLEAR LCD_RS_Write(0x0);
    
#define WRITE_SET LCD_WR_Write(0x1);
#define WRITE_CLEAR LCD_WR_Write(0x0);
#define READ_SET LCD_RD_Write(0x1);
#define READ_CLEAR LCD_RD_Write(0x0);
#endif    



#define RESET_SET atomicStart \
                  LCD_RST_Write(0x1); \
                  atomicEnd

#define RESET_CLEAR atomicStart \
                    LCD_RST_Write(0x0); \
                    atomicEnd

#define CS_SET atomicStart \
               LCD_CS_Write(0x1); \
               atomicEnd

#define CS_CLEAR atomicStart \
                 LCD_CS_Write(0x0); \
                 atomicEnd

#define DATA_CMD_SET atomicStart \
                     LCD_RS_Write(0x1); \
                     atomicEnd                     

#define DATA_CMD_CLEAR atomicStart \
                       LCD_RS_Write(0x0); \
                       atomicEnd    

#define WRITE_SET atomicStart \
                  LCD_WR_Write(0x1); \
                  atomicEnd

#define WRITE_CLEAR atomicStart \
                    LCD_WR_Write(0x0); \
                    atomicEnd

#define READ_SET atomicStart \
                 LCD_RD_Write(0x1); \
                 atomicEnd

#define READ_CLEAR atomicStart \
                   LCD_RD_Write(0x0); \
                   atomicEnd


void lcdWriteDataToPort(uint8_t dataVal);
uint8_t lcdReadDataFromPort(void);
void lcdSetDataPortRead(void);
void lcdSetDataPortWrite(void);
    
#endif
/* [] END OF FILE */

/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef ROT_ENC_H
#define ROT_ENC_H
    
#include "psocUtilities.h"
    


#define ROT_ENC_TURN_RIGHT_FLAG_MASK  0x1
#define ROT_ENC_TURN_LEFT_FLAG_MASK  0x2
#define ROT_ENC_SWITCH_PRESS_FLAG_MASK  0x4   

#define ROT_ENC_FLAG_MASK (ROT_ENC_TURN_RIGHT_FLAG_MASK | ROT_ENC_TURN_LEFT_FLAG_MASK | ROT_ENC_SWITCH_PRESS_FLAG_MASK)    
void rotEncoderStart(void);
uint8_t getEncoderStatus(void);
bool isSetRotEncoderRightFlag(void);
bool isSetRotEncoderLeftFlag(void);
bool isSetRotEncoderSwitchFlag(void);
uint8_t getRotRightCount();
uint8_t getRotLeftCount();
uint8_t getSWCount();
void clearEncodedStatus(uint8_t stFlag);
void clearEncodedStatusAll(uint8_t stFlag);

#endif    
/* [] END OF FILE */

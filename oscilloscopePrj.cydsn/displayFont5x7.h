/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef FONT5X7_H
#define FONT5X7_H

#include "psocUtilities.h"

#define SIZE_OF_LETTER_X  5
#define SIZE_OF_LETTER_Y  7

#define  SIZE_OF_LETTER_X_PLUS_1 (SIZE_OF_LETTER_X + 1)
#define  SIZE_OF_LETTER_Y_PLUS_1 (SIZE_OF_LETTER_Y + 1)

#define  X_PIXELS_NUM_FROM_TEXT_SIZE(textSize)  (textSize*SIZE_OF_LETTER_X)
#define  Y_PIXELS_NUM_FROM_TEXT_SIZE(textSize)  (textSize*SIZE_OF_LETTER_Y)
    
#define  X_PIXELS_NUM_ADVANCE_CHAR_FROM_TEXT_SIZE(textSize)  (textSize*SIZE_OF_LETTER_X_PLUS_1)
#define MAX_ITEMS_MENU 10
    
extern const uint8_t font[];
    
#endif
/* [] END OF FILE */

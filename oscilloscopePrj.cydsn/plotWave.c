/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
 */

#include <stdlib.h>
#include "lcdMcuFriend9342.h"
#include "plotWave.h"
#include <stdio.h>


static tPlotCfg plotWaveCfg; 
tPlotQueue plotQueue;
tPlotData *pPlotDataIn;
tPlotData *pPlotDataOut;

/* Check if there is a sequence of same values */
static void checkSameValue(uint8_t channel, uint16_t startIdx, uint16_t lastIdx, bool *isLine, uint16_t *lineWidth);


uint8_t plotAreaInit( uint16_t xPlotStart, uint16_t yPlotStart, 
                      uint16_t xPlotEnd, uint16_t yPlotEnd, uint16_t backPlaneColor,
                      uint8_t  xGridSize, uint8_t yGridSize, uint16_t gridColor,
                      uint8_t numChannels, uint16_t numMaxSamples, uint16_t numMaxSampleGroups,
                      uint8_t *sampleState, uint8_t **tPlotData, uint16_t channelColor[], 
                      bool linePlot, bool getMemory)
{
    
    uint8_t retVal = OK_RESULT;
    uint8_t index;
    
    /* FIXME : Check vs LCD size */
    plotWaveCfg.xPlotAreaSt = xPlotStart;
    plotWaveCfg.yPlotAreaSt = yPlotStart;
    plotWaveCfg.plotWidth = xPlotEnd - xPlotStart + 1;
    plotWaveCfg.plotHeight = yPlotEnd - yPlotStart + 1;
    plotWaveCfg.backPlaneColor = backPlaneColor;
    plotWaveCfg.xGridSize = xGridSize;
    plotWaveCfg.yGridSize = yGridSize;
    plotWaveCfg.numGridLinesWidth = plotWaveCfg.plotWidth / xGridSize;
    plotWaveCfg.numGridLinesHeight = plotWaveCfg.plotHeight / yGridSize;  
    plotWaveCfg.firstGridLineWidth = xPlotStart + 
                                    ((plotWaveCfg.plotWidth - 
                                    (plotWaveCfg.numGridLinesWidth*xGridSize)) >> 1);    /*Div by 2 to center */
    plotWaveCfg.firstGridLineHeigth = yPlotStart + 
                                      ((plotWaveCfg.plotHeight - 
                                      (plotWaveCfg.numGridLinesHeight*yGridSize)) >> 1); /*Div by 2 to center */
    
    plotWaveCfg.gridColor = gridColor;
    plotWaveCfg.numChannels = numChannels;
    plotWaveCfg.numMaxSamples = numMaxSamples;
    
    if (getMemory)
    {
      plotWaveCfg.pWaveColor = (uint16_t *)calloc(numChannels, sizeof(uint16_t));
    }
    if (plotWaveCfg.pWaveColor == NULL)
    {
      retVal = ERROR_RESULT; 
    }
    else
    {
      for (index=0; index < numChannels; index++)
      {
        plotWaveCfg.pWaveColor[index] = channelColor[index]; 
      }
    }
    
   plotWaveCfg.linePlot = linePlot;
    
   /* Configure data Queue */    
   plotQueue.numData = 0;
   plotQueue.rdPtr = 0;
   plotQueue.wrPtr = 0;
   plotQueue.prevDataSetPtr = 0;
   if (getMemory)
   {    
     plotQueue.pDataBuffer = (plotWaveBuffer *) calloc(numChannels, sizeof(plotWaveBuffer));
   }
   if (  plotQueue.pDataBuffer == NULL)
   {
      retVal = ERROR_RESULT;
   }

   if (getMemory)
   {
     pPlotDataIn = (tPlotData *) calloc(numChannels, sizeof(tPlotData));
     pPlotDataOut = (tPlotData *) calloc(numChannels, sizeof(tPlotData));
     printf("@plotAreaInit() - pDatain: 0x%x size: %d \r\n", pPlotDataIn, sizeof(tPlotData));
   }
   if ((pPlotDataIn == NULL) || (pPlotDataOut == NULL))
   {
     retVal = ERROR_RESULT;
   }
  
   plotWaveCfg.initSuccess = (retVal == OK_RESULT) ? true : false;
   

  return retVal;
    
}

uint8_t drawBackPlane(void)
{
   uint8_t retVal = OK_RESULT;
    
   if (plotWaveCfg.initSuccess)
   {
     fillRect(plotWaveCfg.xPlotAreaSt,plotWaveCfg.yPlotAreaSt, 
              plotWaveCfg.plotWidth,
              plotWaveCfg.plotHeight,
              plotWaveCfg.backPlaneColor );
   }
   else
   {
    retVal = ERROR_RESULT;
   }
  return retVal;
}

uint8_t drawGrid(void)
{
   uint8_t retVal = OK_RESULT;
   uint8_t iter; 
   uint16_t coord;
   if (plotWaveCfg.initSuccess)
   {
    
      /* Grid Vertical lines */
      coord = plotWaveCfg.firstGridLineWidth;
      for (iter = 0; iter <= plotWaveCfg.numGridLinesWidth; iter++)
      {
        drawFastVLine( coord, plotWaveCfg.yPlotAreaSt, plotWaveCfg.plotHeight, plotWaveCfg.gridColor);
        coord += plotWaveCfg.xGridSize;      
      }
   
      /* Grid Horizontal lines */
      coord = plotWaveCfg.firstGridLineHeigth;
      for (iter = 0; iter <= plotWaveCfg.numGridLinesHeight; iter++)
      {
        drawFastHLine(plotWaveCfg.xPlotAreaSt, coord, plotWaveCfg.plotWidth, plotWaveCfg.gridColor);
        coord += plotWaveCfg.yGridSize;   
      }
  }
  else
  {
    retVal = ERROR_RESULT;
  }
   return retVal;
}

  /* Add data to queue (all channels) */
uint8_t plotWaveAddDataToQueue( tPlotData *pData, tChnlMask chnlMask)
{
   uint8_t retVal = OK_RESULT;
   uint8_t chnlIndex;
   uint16_t wrIndex;
   bool canWrite = false;
    
   if (plotWaveCfg.initSuccess)
   {
      
       canWrite = false;  
       if (plotQueue.numData < PLOT_QUEUE_SIZE)
       {
      
        /* Prevent Write in area located in the previous set of data.
           This set of data is needed in the plot.
           Case 1:         Case 2:      Case 3:
          [         ]    [ Invalid ]    [ prevRd Rd ] 
          [ prevRd  ]    [ Rd      ]    All range valid  
          [ Invalid ]    [         ]
          [ Rd      ]    [ prevRd  ]
          [         ]    [ Invalid ]
        */
            
         if (plotQueue.rdPtr > plotQueue.prevDataSetPtr)  /* Case 1*/
         {
           canWrite = ((plotQueue.wrPtr >= plotQueue.rdPtr) ||
                       (plotQueue.wrPtr < plotQueue.prevDataSetPtr));   
         }    
         else if (plotQueue.rdPtr < plotQueue.prevDataSetPtr) /* Case 2*/
         {
           canWrite = ((plotQueue.wrPtr >= plotQueue.rdPtr) &&
                       (plotQueue.wrPtr < plotQueue.prevDataSetPtr));
         }
         else
         {
           canWrite = true;
         }
       }
    
       if ( canWrite )
       {
         wrIndex = plotQueue.wrPtr;
         for (chnlIndex = 0; chnlIndex < plotWaveCfg.numChannels; chnlIndex++)
         {
           if ((1 << chnlIndex) & (chnlMask))
           {
             plotQueue.pDataBuffer[chnlIndex][wrIndex] = pData[chnlIndex];
           }
         }
         plotQueue.wrPtr = (plotQueue.wrPtr + 1) % PLOT_QUEUE_SIZE;
         plotQueue.numData++;
       }
       else
       {
          wrIndex = QUEUE_FULL_RESULT;
       }
   }
   else
   {
     retVal = ERROR_RESULT;  
   }
   
   return retVal;
}


uint8_t plotWaveplotData(uint8_t chnlMask)
{
   uint8_t retVal = OK_RESULT;
   uint8_t chnlIdx;
   uint16_t xPixelIter;
   uint16_t xPixel;
   uint16_t readIndex;
   uint16_t lastIndex;
   uint16_t lineUntilWidth;
   uint16_t lineUntilEnd;
   uint16_t prevRdIndex = 0;
   uint16_t prevSetRdIndex;
   uint16_t prevSetPrevRdIndex = 0; 
   bool     isLine = false;
   bool    wasWaveErased = false;
   bool samePixelInPrevSet; 
   bool plotFirstTime;
   bool firstPixel;   
    
    printf("@plotWaveplotData() - Init: %d \r\n", plotWaveCfg.initSuccess);
    if (plotWaveCfg.initSuccess == false) 
    {
      retVal = ERROR_RESULT;  
    }
    else if ( plotQueue.numData < plotWaveCfg.plotWidth)
    {
      retVal = QUEUE_NO_ENOUGH_DATA_RESULT;
    }
    else
    {
        /* Enough data to plot */
        plotFirstTime = (plotQueue.rdPtr == plotQueue.prevDataSetPtr);
        /* Last read index */
        lastIndex = (plotQueue.rdPtr  + plotWaveCfg.plotWidth - 1 ) % PLOT_QUEUE_SIZE;
        
        for ( chnlIdx = 0; chnlIdx < plotWaveCfg.numChannels; chnlIdx++)
        {
            if (( 1 << chnlIdx ) & chnlMask)
            {
                isLine = false;
                lineUntilEnd = 0; 
                readIndex = plotQueue.rdPtr; /* Index of data to be plot*/
                prevSetRdIndex = plotQueue.prevDataSetPtr; /* Index in the previous set of data (plot in the prev iteration)*/ 
                printf("@plotWaveplotData() - Plot started \r\n");
                for (xPixelIter = 0; xPixelIter < plotWaveCfg.plotWidth; xPixelIter++)
                {
                    xPixel = plotWaveCfg.xPlotAreaSt + xPixelIter; 
                    firstPixel = (xPixelIter == 0);                                    
                    if (plotFirstTime == false) /* This is not the 1st time a signal is draw */
                    {                               
                        
                        /* Check if there was an update, if not do nothing 
                          Case 1: First pixel and current pixel different than current pixel in previous set
                          Case 2: It's not first pixel
                              Case 2a: Plot line disable and current pixel different than current pixel in previous set
                              Case 2b: Plot line enable and  current pixel different than current pixel in previous set
                                       and previous pixel different than previous pixel different than in previous set  
                       */                        
                        samePixelInPrevSet = (plotQueue.pDataBuffer[chnlIdx][readIndex] == plotQueue.pDataBuffer[chnlIdx][prevSetRdIndex]);
                        if ((( firstPixel == true) || (plotWaveCfg.linePlot == false)) && (samePixelInPrevSet == false))
                        {
                           /* There is no line to previous point, just clear previous pixel and draw current pixel*/
                           wasWaveErased = true;
                            /* If this is different, clear the previous pixel */
                            drawPixel(xPixel, plotQueue.pDataBuffer[chnlIdx][prevSetRdIndex], plotWaveCfg.backPlaneColor );
                            drawPixel(xPixel, plotQueue.pDataBuffer[chnlIdx][readIndex], plotWaveCfg.pWaveColor[chnlIdx]);
                        }
                        else if ((firstPixel == false ) && (plotWaveCfg.linePlot == true) && 
                                 ((samePixelInPrevSet == false) || 
                                  (plotQueue.pDataBuffer[chnlIdx][prevRdIndex] != plotQueue.pDataBuffer[chnlIdx][prevSetPrevRdIndex] )
                                 )
                                )                                                                                                                            
                        //if (plotQueue.pDataBuffer[chnlIdx][readIndex] != plotQueue.pDataBuffer[chnlIdx][prevSetRdIndex]) 
                            
                        {    
                            wasWaveErased = true;
                            
                            /* Clear Previous line */                            
                            drawLine( xPixel - 1, plotQueue.pDataBuffer[chnlIdx][prevSetPrevRdIndex], 
                                         xPixel, plotQueue.pDataBuffer[chnlIdx][prevSetRdIndex],
                                         plotWaveCfg.backPlaneColor);
                            /* Draw new line */
                            drawLine(xPixel-1, plotQueue.pDataBuffer[chnlIdx][prevRdIndex], xPixel, plotQueue.pDataBuffer[chnlIdx][readIndex] , plotWaveCfg.pWaveColor[chnlIdx]);                                                                                    
                        }                    
                    }
                    else
                    {             
                        /* First time a signal is draw, just draw it*/
                        if ( !isLine )
                        {
                            /* Check if there is a line in the folowing values */                          
                            checkSameValue(chnlIdx, readIndex, lastIndex, &isLine, &lineUntilWidth);
                            if ( isLine )
                            {                                
                                drawFastHLine(xPixel, plotQueue.pDataBuffer[chnlIdx][readIndex], lineUntilWidth, plotWaveCfg.pWaveColor[chnlIdx]);
                                lineUntilEnd = xPixel + lineUntilWidth - 1; /* Final coordenate when it's no longer line */
                            }
                         
                            /* If configured to drwaing a line to the previous point and this is not the first point */
                            if ((plotWaveCfg.linePlot) && (xPixelIter != 0))
                            {
                                drawLine( xPixel - 1, plotQueue.pDataBuffer[chnlIdx][prevRdIndex], 
                                          xPixel, plotQueue.pDataBuffer[chnlIdx][readIndex],
                                          plotWaveCfg.pWaveColor[chnlIdx]);
                            }
                            else if (isLine == false)
                            {
                               drawPixel(xPixel, plotQueue.pDataBuffer[chnlIdx][readIndex], plotWaveCfg.pWaveColor[chnlIdx]); 
                            }
                        }
                        else
                        {
                            /* If line was set, check if this need to be disabled */  
                            if ( xPixel == lineUntilEnd)
                            {
                                isLine = false;
                            }
                        }
                    } 
                    prevRdIndex = readIndex;
                    prevSetPrevRdIndex = prevSetRdIndex;
                    /* Index of the next pixel to be plot*/                                                            
                    readIndex++;
                    if (readIndex == PLOT_QUEUE_SIZE)
                    {
                        readIndex = 0;
                    }
                    /* Index in the previous set of data (plot in the prev iteration)*/
                    prevSetRdIndex++;
                    if (prevSetRdIndex == PLOT_QUEUE_SIZE)
                    {
                        prevSetRdIndex = 0;
                    }
                } /* for xPixel*/
            } /* channel mask idx */      
        }  /* For Channel */
        /* When finishing to plot both channels, then update read and last pointers */    
        plotQueue.prevDataSetPtr = plotQueue.rdPtr;
        plotQueue.rdPtr = (plotQueue.rdPtr + plotWaveCfg.plotWidth) % PLOT_QUEUE_SIZE;
        plotQueue.numData = plotQueue.numData - plotWaveCfg.plotWidth;
        
        
    } /* enough points to plot */
    
    if (wasWaveErased)
    {
      drawGrid();  
    }
    return retVal;
}

static void checkSameValue(uint8_t channel, uint16_t startIdx, uint16_t lastIdx, bool *isLine, uint16_t *lineWidth)
{
    uint16_t idx;
    tPlotData valToCompare;
    
    *lineWidth = 1;
    valToCompare = plotQueue.pDataBuffer[channel][startIdx];
    idx = startIdx;
    while (idx != lastIdx)
    {
      idx++;
      if (idx == PLOT_QUEUE_SIZE)
      {
        idx = 0;
      }  
      if ( valToCompare == plotQueue.pDataBuffer[channel][idx])
      {
        *lineWidth = *lineWidth + 1U;
      }
      else
      {        
        break;
      }
    }
    *isLine = (*lineWidth != 1U);
    
 } 
    
void plotWaveClearDataBuffer(void)
{
   uint16_t iter;
   uint8_t chnlIter;
   plotQueue.numData = 0;
   plotQueue.rdPtr = 0;
   plotQueue.wrPtr = 0;
   plotQueue.prevDataSetPtr = 0;
    
   for (chnlIter = 0; chnlIter < plotWaveCfg.numChannels; chnlIter++)
   {
       for (iter = 0; iter < PLOT_QUEUE_SIZE; iter++)
       {               
           plotQueue.pDataBuffer[chnlIter][iter] = 0U;
       }    
   } 
}


/* Return the pointer to the buffer that can be used to pass data */
tPlotData *plotGetDataInPtr(void)
{
    tPlotData *ptr = NULL;
    
    if (pPlotDataIn != NULL)
    {
      ptr = pPlotDataIn;   
    }
    return ptr;
}

/* [] END OF FILE */

/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef SIGNAL_GENERATOR_WAVE_H

#define SIGNAL_GENERATOR_WAVE_H

#include "psocUtilities.h"
    
#define MAX_NUM_SAMPLES  256
#define MIN_NUM_SAMPLES  8
    
#define WAVE_SIGGEN_MAX_SAMPLE_PER_SEC 1000000
#define NUM_NS_PER_SECOND 1000000000
#define WAVE_SIGGEN_MIN_SAMPLE_TIME_NS   NUM_NS_PER_SECOND/WAVE_SIGGEN_MAX_SAMPLE_PER_SEC 
    
#define MAX_FREQ_HZ_SUPPORTED_NO_SQUARE (WAVE_SIGGEN_MAX_SAMPLE_PER_SEC/MIN_NUM_SAMPLES)
#define MAX_FREQ_HZ_SUPPORTED_DIG_SQUARE (WAVE_SIGGEN_MAX_SAMPLE_PER_SEC/2)    
    
#define MAX_SIG_VAL 0xFF
#define HALF_SIG_VAL 0x80
            
typedef uint8_t tSigGenData;
    
typedef enum {
   SIG_GEN_WAVE_SAW,
   SIG_GEN_WAVE_INV_SAW,    
   SIG_GEN_WAVE_SINE,
   SIG_GEN_WAVE_TRIANGLE,
   SIG_GEN_WAVE_SQUARE,
   SIG_GEN_WAVE_SQUARE_DIGITAL    
    
}tSigGenWaveType;
    
typedef struct{
  uint16_t numSamples;
  uint32_t sampleTimeNs;  
}tsigWaveInfo;

int8_t getWaveSamplesForSignal( tSigGenWaveType  sigType, uint32_t freqHz, tsigWaveInfo *signalInfo, tSigGenData *sigVec);
int8_t fillSignalBufferFromNumSamples( tSigGenWaveType  sigType, uint16_t numSamples, tSigGenData *sigVec);

#endif

/* [] END OF FILE */

/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <stdio.h>
#include "projectApp.h"
#include "lcdMcuFriend9342.h"
#include "projectDisplay.h"
#include "rotEncoder.h"
#include "signalGen.h"
#include "signalGenWave.h"
#include "oscilloscope.h"

/*#define SHOW_START_BANNER*/



/* Operation selection */
uint8_t rotatorySwitchMonitor(void);
bool rotatory_right_left_item_choose( uint8_t rotSwFlag, tMenuCfgSt *menuCfgSt);
bool rotatory_right_left_field_item_choose(uint8_t rotSwFlag, tMenuFieldSetCfgSt *menuFieldCfgSt);
void projSigGenFieldDigitSelectHandle(uint8_t rotSwFlag, tMenuFieldSetCfgSt *menuFieldSetCfg, 
                                      uint8_t itemIdx, tPrjStages prevPrjState, tPrjStages nextPrjState,
                                      prepareStateFunPtr preparePrevFunPtr);
void projSigGenFieldConfigHandle(uint8_t rotSwFlag, tMenuFieldSetCfgSt *menuFieldSetCfg, 
                                 uint8_t itemIdx, tPrjStages prevPrjState);

/* Init stage handling */
void prjInitOperHanlde(void);
/* Main menu handling */
void prepareToEnterMainMenu(void);
void prjMainMenuOperHandle( uint8_t rotSwFlag);

/* SignalGen menu1 handling (Wave type)*/
void prepareToEnterSigGenMenu1(void);
void prjSigGenMenu1OperHandle( uint8_t rotSwFlag);

/* SignalGen menu2 handling (Mode)*/
void prepareToEnterSigGenMenu2(void);
void prjSigGenMenu2OperHandle( uint8_t rotSwFlag);

/* SignalGen Auto Analog menu handling */
void prepareToEnterAutoAnalogMenu(void);
void projSigGenAutoAnalogHandle(uint8_t rotSwFlag);

/* SignalGen Manual Analog menu handling */
void prepareToEnterManualAnalogMenu(void);
void projSigGenManualAnalogHandle(uint8_t rotSwFlag);

/* SignalGen Auto Digital menu handling */
void prepareToEnterAutoDigitalMenu(void);
void projSigGenAutoDigitalHandle(uint8_t rotSwFlag);

/* SignalGen Auto Analog config freq field */
void prepareToEnterAutoAnalogFreqConfigField(void);
void projSigGenAutoAnalogFreq1ConfigHandle(uint8_t rotSwFlag);
void projSigGenAutoAnalogFreq2ConfigHandle(uint8_t rotSwFlag);

/* SignalGen Manual Analog config Num Samples field */
void prepareToEnterManualAnalogNumSamplesConfigField(void);
void projSigGenManualAnalogNumSamples1ConfigHandle(uint8_t rotSwFlag);
void projSigGenManualAnalogNumSamples2ConfigHandle(uint8_t rotSwFlag);

/*SignalGen Manual Analog config Sample Period field*/
void prepareToEnterManualAnalogSamplePeriodConfigField(void);
void projSigGenManualAnalogSamplePeriod1ConfigHandle(uint8_t rotSwFlag);
void projSigGenManualAnalogSamplePeriod2ConfigHandle(uint8_t rotSwFlag);

/* SignalGen Auto Digital config freq field */
void prepareToEnterAutoDigitalFreqConfigField(void);
void projSigGenAutoDigitalFreq1ConfigHandle(uint8_t rotSwFlag);
void projSigGenAutoDigitalFreq2ConfigHandle(uint8_t rotSwFlag);

/* SignalGen Ready process */
void prjSigGenReadyOperHandle( uint8_t rotSwFlag);
void prepareToEnterSigGenReadyMenu(void);

/* Activate/Disactivate signal generator */
void prjActivateDisactivateSigGen(void);

tPrjStatus prjStatus;

const tMenuTextIdx mainMenuTextIdx[] = {
                                         {OSCILLOSCOPE_MENU_TEXT,OSCILLOSCOPE_MENU_IDX },
                                         {SIGNAL_GEN_MENU_TEXT, SIGNAL_GEN_MENU_IDX},
                                         {OSCILLOSCOPE_SIGNAL_GEN_MENU_TEXT,OSCILLOSCOPE_SIGNAL_GEN_MENU_IDX}
                                     };

/* Type of signal to generate */
const tMenuTextIdx sigGenMenu1TextIdx[] = {
                                           {MENU_BACK_TEXT, MENU_BACK_IDX},
                                           {SIG_GEN_WAVE_SAW_TEXT, SIG_GEN_WAVE_SAW_IDX},
                                           {SIG_GEN_WAVE_INVSAW_TEXT, SIG_GEN_WAVE_INVSAW_IDX},
                                           {SIG_GEN_WAVE_SINE_TEXT, SIG_GEN_WAVE_SINE_IDX},
                                           {SIG_GEN_WAVE_TRIANGLE_TEXT, SIG_GEN_WAVE_TRIANGLE_IDX},
                                           {SIG_GEN_WAVE_SQR_ANALOG_TEXT, SIG_GEN_WAVE_SQR_ANALOG_IDX},
                                           {SIG_GEN_WAVE_CLOCK_TEXT, SIG_GEN_WAVE_CLOCK_IDX}                                              
                                          };
/* Signal generation mode menu */
const tMenuTextIdx sigGenMenu2TextIdx[] = {
                                     {MENU_BACK_TEXT, MENU_BACK_IDX},
                                     {SIG_GEN_OPER_MODE_AUTO_TEXT, SIG_GEN_OPER_MODE_AUTO_IDX},
                                     {SIG_GEN_OPER_MODE_MANUAL_TEXT, SIG_GEN_OPER_MODE_MANUAL_IDX}
                                    };

/* Signal generation Ready(Go) menu. The only option is to return */
const tMenuTextIdx sigGenReadyMenuTextIdx[] = {
                                             {MENU_BACK_TEXT, MENU_BACK_IDX}
                                           };

/* Signal generaton Auto field setup menu(Analog) */
const tFieldInfoCfg autoFreqAnalogInfoCfg = {
                                               SIG_GEN_FIELD_FREQ_TEXT, 
                                               SIG_GEN_FIELD_FREQ_IDX,
                                               true,
                                               SIG_GEN_FIELD_FREQ_ANALOG_MAX_VAL,
                                               SIG_GEN_FIELD_FREQ_ANALOG_MIN_VAL,
                                               SIG_GEN_FIELD_FREQ_ANALOG_NUM_DIGITS
                                             };

/* Signal generaton Auto field setup menu(Digital) */
const tFieldInfoCfg autoFreqDigitalInfoCfg = {
                                              SIG_GEN_FIELD_FREQ_TEXT, 
                                              SIG_GEN_FIELD_FREQ_IDX,
                                              true, 
                                              SIG_GEN_FIELD_FREQ_DIGITAL_MAX_VAL,
                                              SIG_GEN_FIELD_FREQ_DIGITAL_MIN_VAL, 
                                              SIG_GEN_FIELD_FREQ_DIGITAL_NUM_DIGITS
                                             };                                         


/* Signal generaton Manual fields setup menu */
const tFieldInfoCfg manualFieldNumSamplesInfoCfg = {
                                                    SIG_GEN_FIELD_NUM_SAMPLES_TEXT, 
                                                    SIG_GEN_FIELD_NUM_SAMPLES_IDX,
                                                    true,
                                                    SIG_GEN_FIELD_NUM_SAMPLES_MAX_VAL,
                                                    SIG_GEN_FIELD_NUM_SAMPLES_MIN_VAL,
                                                    SIG_GEN_FIELD_NUM_SAMPLES_NUM_DIGITS
                                                   };
                   
const tFieldInfoCfg manualFieldSamplePeriodInfoCfg = {
                                                      SIG_GEN_FIELD_SAMPLE_PERIOD_TEXT,
                                                      SIG_GEN_FIELD_SAMPLE_PERIOD_IDX,
                                                      true,
                                                      SIG_GEN_FIELD_SAMPLE_PERIOD_MAX_VAL,
                                                      SIG_GEN_FIELD_SAMPLE_PERIOD_MIN_VAL,
                                                      SIG_GEN_FIELD_SAMPLE_PERIOD_NUM_DIGITS
                                                      };

const tFieldInfoCfg fieldMenuBackOption = {
                                      MENU_BACK_TEXT,
                                      MENU_BACK_IDX,
                                      false,
                                      0,
                                      0,
                                      0
                                    };

const tFieldInfoCfg fieldMenuAutoGoOption = {
                                      MENU_GO_TEXT,
                                      SIG_GEN_FIELD_AUTO_GO_IDX,
                                      false,
                                      0,
                                      0,
                                      0
                                    };

const tFieldInfoCfg fieldMenuManualGoOption = {
                                      MENU_GO_TEXT,
                                      SIG_GEN_FIELD_MANUAL_GO_IDX,
                                      false,
                                      0,
                                      0,
                                      0
                                    };

/* Each one of the strings in mainMenuText will be obtained using
   mainMenuCfgSt.menuText[x] */
/*----------------------------------- Main Project Menu  -------------------------------*/
tMenuCfgSt mainMenuCfgSt = 
    {
       mainMenuTextIdx,
       sizeof(mainMenuTextIdx)/sizeof(mainMenuTextIdx[0]), /* Number of elements */
       COLOR_WHITE,
       COLOR_YELLOW,
       COLOR_BLACK,
       2,  /* Text Size */
       OSCILLOSCOPE_MENU_IDX,  /* Selecting item 0 as default */
       0,   /* Max length */
       true /* Clear flag */
    };
    
 /*----------------------------------- Signal Gen Menu 1 -------------------------------*/   
 tMenuCfgSt sigGenMenu1CfgSt = 
    {
        sigGenMenu1TextIdx,
        sizeof(sigGenMenu1TextIdx)/sizeof(sigGenMenu1TextIdx[0]), /* Number of elements */
        COLOR_WHITE,
        COLOR_YELLOW,
        COLOR_BLACK,
        2,  /* Text Size */
        MENU_BACK_IDX,  /* Selecting item 0 as default */
        0,  /* Max length */
        true /* Clear flag */
    };
   /*----------------------------------- Signal Gen Menu 2 -------------------------------*/
  tMenuCfgSt sigGenMenu2CfgSt = 
    {
        sigGenMenu2TextIdx,
        sizeof(sigGenMenu2TextIdx)/sizeof(sigGenMenu2TextIdx[0]), /* Number of elements */
        COLOR_WHITE,
        COLOR_YELLOW,
        COLOR_BLACK,
        2,  /* Text Size */
        MENU_BACK_IDX,  /* Selecting item 0 as default */
        0,  /* Max length */
        true /* Clear flag */
    };
  
  /*----------------------------------- Signal Gen Ready (Go) Menu  -------------------------------*/  
   tMenuCfgSt sigGenReadyMenuCfgSt = 
      {
         sigGenReadyMenuTextIdx,
         sizeof(sigGenReadyMenuTextIdx)/sizeof(sigGenReadyMenuTextIdx[0]),
         COLOR_WHITE,
         COLOR_YELLOW,
         COLOR_BLACK,
         2,  /* Text Size */
         MENU_BACK_IDX,  /* Selecting item 0 as default */
         0,  /* Max length */
         true /* Clear flag */
      };
  /*----------------------------------- Signal Gen Field Menu (AUTO-ANALOG) -------------------------------*/  
  fieldInfoCfgSt sigGenAutoAnalogFields[] = {
                                              {
                                                &fieldMenuBackOption,
                                                0,
                                                0
                                              },    
                                              { &autoFreqAnalogInfoCfg,
                                                SIG_GEN_FIELD_FREQ_ANALOG_NUM_DIGITS-1, /*Char Select starting at most significative char*/
                                                SIG_GEN_DEFAULT_ANALOG_FREQ_AUTO    
                                              },
                                              {
                                                &fieldMenuAutoGoOption,
                                                0,
                                                0
                                              }
                                            };


 tMenuFieldSetCfgSt sigGenMenuFieldAutoAnalog = {
                                                   sigGenAutoAnalogFields,
                                                   sizeof(sigGenAutoAnalogFields)/sizeof(sigGenAutoAnalogFields[0]),
                                                   COLOR_WHITE,
                                                   COLOR_YELLOW,
                                                   COLOR_RED,
                                                   COLOR_BLACK,
                                                   2,  /* Text Size */
                                                   MENU_BACK_IDX,
                                                   0,  /* Max length */
                                                   0,  /* Max value length */
                                                   true /* Clear flag */
                                                };

/*----------------------------------- Signal Gen Field Value Menu (AUTO-DIGITAL) -------------------------------*/  
  fieldInfoCfgSt sigGenAutoDigitalFields[] = {
                                              {
                                                &fieldMenuBackOption,
                                                0,
                                                0
                                              },
                                              {  
                                                &autoFreqDigitalInfoCfg,
                                                SIG_GEN_FIELD_FREQ_DIGITAL_NUM_DIGITS-1, /*Char Select starting at most significative char*/
                                                SIG_GEN_DEFAULT_DIGITAL_FREQ_AUTO    
                                              },
                                              {
                                                &fieldMenuAutoGoOption,
                                                0,
                                                0
                                              }
                                            };

 tMenuFieldSetCfgSt sigGenMenuFieldAutoDigital = {
                                                   sigGenAutoDigitalFields,
                                                   sizeof(sigGenAutoDigitalFields)/sizeof(sigGenAutoDigitalFields[0]),
                                                   COLOR_WHITE,
                                                   COLOR_YELLOW,
                                                   COLOR_RED,
                                                   COLOR_BLACK,
                                                   2,  /* Text Size */
                                                   MENU_BACK_IDX,
                                                   0,  /* Max length */
                                                   0,  /* Max value length */
                                                   true /* Clear flag */
                                                };

/*----------------------------------- Signal Gen Field Value Menu (MANUAL-ANALOG) -------------------------------*/
  fieldInfoCfgSt sigGenManualAnalogFields[] = {
                                                {
                                                  &fieldMenuBackOption,
                                                  0,
                                                  0
                                                },
                                                { &manualFieldNumSamplesInfoCfg,
                                                  SIG_GEN_FIELD_NUM_SAMPLES_NUM_DIGITS-1, /*Char Select starting at most significative char*/
                                                  SIG_GEN_DEFAULT_ANALOG_NUMSAMPLES_MANUAL /*Initial value 16*/
                                                },
                                                { 
                                                  &manualFieldSamplePeriodInfoCfg,
                                                  SIG_GEN_FIELD_SAMPLE_PERIOD_NUM_DIGITS-1, /*Char Select starting at most significative char*/
                                                  SIG_GEN_DEFAULT_ANALOG_SAMPLEPERIOD_MANUAL  
                                                },
                                                {
                                                  &fieldMenuManualGoOption,
                                                  0,
                                                  0
                                                }
                                              };

 tMenuFieldSetCfgSt sigGenMenuFieldManualAnalog = {
                                                   sigGenManualAnalogFields,
                                                   sizeof(sigGenManualAnalogFields)/sizeof(sigGenManualAnalogFields[0]),
                                                   COLOR_WHITE,
                                                   COLOR_YELLOW,
                                                   COLOR_RED,
                                                   COLOR_BLACK,
                                                   2,  /* Text Size */
                                                   MENU_BACK_IDX,
                                                   0,  /* Max length */
                                                   0, 
                                                   true /* Clear flag */
                                                };

/*--------------------------------- Global variables ---------------------------------*/
tPrjStages prjStage = PRJ_INIT_ST;
tPrjEnableDisableOper prjEnableDisableOper = PRJ_ENABLE_DISABLE_NOTHING;
/*----------------------------------------------------------------------------------- */
/* Get the maximum length in list of strings for regular menu */    
uint8_t getMaxLengthTextIdx(const tMenuTextIdx *menuTextIdx, uint8_t numItems)
{
  uint8_t idx;
  uint8_t maxLen = 0;
  uint8_t length;  
    
  for (idx=0; idx <numItems; idx++)
  {
    length = (uint8_t)strlen(menuTextIdx[idx].itemText);
    if ( length > maxLen)
    {
      maxLen = length;
    }
  }
  printf("Max length: %d \r\n", maxLen);
  return maxLen;
}

/*----------------------------------------------------------------------------------- */
/* Get the  maximum length in list of strings for configurable field menu */
uint8_t getMaxLengthFieldInfo(fieldInfoCfgSt *menuField, uint8_t numItems )
{
  uint8_t idx;
  uint8_t maxLen = 0;
  uint8_t length;  
    
  for (idx=0; idx <numItems; idx++)
  {
    length = (uint8_t)strlen(menuField[idx].fieldCfg->itemText);
    if ( length > maxLen)
    {
      maxLen = length;
    }
  }
  printf("Max length: %d \r\n", maxLen);
  return maxLen;
}
 
/*----------------------------------------------------------------------------------- */
/* Get the  maximum length in list of strings for configurable field menu */
uint8_t getMaxLengthFieldValueInfo(fieldInfoCfgSt *menuField, uint8_t numItems )
{
   uint8_t idx;
   uint8_t maxLen = 0;
   uint8_t length;
    
      
   for (idx=0; idx <numItems; idx++)
   { 
     if (menuField[idx].fieldCfg->hasValue)
     {
        length = menuField[idx].fieldCfg->numDigits;
        
        if ( length > maxLen)
        {
          maxLen = length;
        }
      }
   }    
   
   printf("Max length FieldVal: %d \r\n", maxLen);
   return maxLen;   
}
/*----------------------------------------------------------------------------------- */
uint8_t prjAppInit()
{   
    /* Request to show main menu */
    uint8_t retVal;
    
    /* Obtain max length in the text of each menu */
    mainMenuCfgSt.maxTextLen = getMaxLengthTextIdx(mainMenuTextIdx, mainMenuCfgSt.numItems);
    sigGenMenu1CfgSt.maxTextLen = getMaxLengthTextIdx(sigGenMenu1TextIdx, sigGenMenu1CfgSt.numItems);
    sigGenMenu2CfgSt.maxTextLen = getMaxLengthTextIdx(sigGenMenu2TextIdx, sigGenMenu2CfgSt.numItems);
    
    sigGenMenuFieldAutoAnalog.maxTextLen = getMaxLengthFieldInfo(sigGenAutoAnalogFields, sigGenMenuFieldAutoAnalog.numItems);
    sigGenMenuFieldAutoAnalog.maxValueLen  = getMaxLengthFieldValueInfo(sigGenAutoAnalogFields, sigGenMenuFieldAutoAnalog.numItems);
    
    sigGenMenuFieldAutoDigital.maxTextLen = getMaxLengthFieldInfo(sigGenAutoDigitalFields, sigGenMenuFieldAutoDigital.numItems);
    sigGenMenuFieldAutoDigital.maxValueLen = getMaxLengthFieldValueInfo(sigGenAutoDigitalFields,sigGenMenuFieldAutoDigital.numItems);
    
    sigGenMenuFieldManualAnalog.maxTextLen = getMaxLengthFieldInfo(sigGenManualAnalogFields, sigGenMenuFieldManualAnalog.numItems);
    sigGenMenuFieldManualAnalog.maxValueLen = getMaxLengthFieldValueInfo(sigGenManualAnalogFields, sigGenMenuFieldManualAnalog.numItems);
    
    printf("SigGen NumItems: %d  maxTextLen: %d\r\n", sigGenMenuFieldManualAnalog.numItems, sigGenMenuFieldManualAnalog.maxTextLen);
    printf("SigGen NumItems: %d  valueMaxValLen: %d\r\n", sigGenMenuFieldManualAnalog.numItems, sigGenMenuFieldManualAnalog.maxValueLen);
    
    printf("Oscilloscpe Project\r\n");
   
    /* Initialize the rotary encoder */
    rotEncoderStart();
    
    /* Initialization of Signal Generator */
    signalGenInit();
    
    /* Initialization of the oscilloscope */
    oscilloscopeInit();
    
    /* Initialize LCD display */
    retVal = prjDisplayInit();
    
    #ifdef SHOW_START_BANNER
    if (retVal == OK_RESULT)
   {     
      prjBannerStart();
      pSOCdelyMS(3000);
   }
   #endif   
    
   return retVal;
}

/* This call is executed from the main. An infinite loop is executed here */
void prjAppEntryPoint()
{
   uint8_t rotSwFlag;
    while(1)
    {
        /* Check if there is any rotatory switch event */
        rotSwFlag = rotatorySwitchMonitor();
        
        switch (prjStage)
        {
          case PRJ_INIT_ST:
          {
            prjInitOperHanlde();
            break;
          }
          case PRJ_MAIN_MENU_ST:
          {
            prjMainMenuOperHandle(rotSwFlag);
            break;
          }
          case PRJ_SIG_GEN_MENU1_ST:
          {
            prjSigGenMenu1OperHandle(rotSwFlag);
            break; 
          }
          case PRJ_SIG_GEN_MENU2_ST:
          {
            prjSigGenMenu2OperHandle(rotSwFlag);
            break;
          }
          case PRJ_SIG_GEN_AUTO_ANALOG_MENU_ST:
          {
            projSigGenAutoAnalogHandle(rotSwFlag);            
            break;
          }
          case PRJ_SIG_GEN_MANUAL_ANALOG_MENU_ST:
          {
            projSigGenManualAnalogHandle(rotSwFlag);
            break;
          }
          case PRJ_SIG_GEN_AUTO_DIGITAL_MENU_ST:
          {
            projSigGenAutoDigitalHandle(rotSwFlag);
            break;
          }
          case PRJ_SIG_GEN_AUTO_ANALOG_FREQ_SET_ST1:
          {
            projSigGenAutoAnalogFreq1ConfigHandle(rotSwFlag);
            break;
          }
          case PRJ_SIG_GEN_AUTO_ANALOG_FREQ_SET_ST2:
          {
            projSigGenAutoAnalogFreq2ConfigHandle(rotSwFlag);
            break;
          }
          case PRJ_SIG_GEN_MANUAL_ANALOG_MENU_NUMSAMPLES_SET_ST1:
          {
            projSigGenManualAnalogNumSamples1ConfigHandle(rotSwFlag);
            break;
          }
          case PRJ_SIG_GEN_MANUAL_ANALOG_MENU_NUMSAMPLES_SET_ST2:
          {
            projSigGenManualAnalogNumSamples2ConfigHandle(rotSwFlag);
            break;
          }
          case PRJ_SIG_GEN_MANUAL_ANALOG_MENU_SAMPLEPERIOD_SET_ST1:
          {
            projSigGenManualAnalogSamplePeriod1ConfigHandle(rotSwFlag);
            break;
          }
          case PRJ_SIG_GEN_MANUAL_ANALOG_MENU_SAMPLEPERIOD_SET_ST2:
          {
            projSigGenManualAnalogSamplePeriod2ConfigHandle(rotSwFlag);
            break;
          }
          case PRJ_SIG_GEN_AUTO_DIGITAL_FREQ_SET_ST1:
          {
            projSigGenAutoDigitalFreq1ConfigHandle(rotSwFlag);
            break;
          }
          case PRJ_SIG_GEN_AUTO_DIGITAL_FREQ_SET_ST2:
          {
            projSigGenAutoDigitalFreq2ConfigHandle(rotSwFlag);
            break;
          }
          case PRJ_SIG_GEN_AUTO_ANALOG_READY_ST:
          case PRJ_SIG_GEN_AUTO_DIGITAL_READY_SET_ST:
          case PRJ_SIG_GEN_MANUAL_ANALOG_READY_ST:
          {            
            prjSigGenReadyOperHandle(rotSwFlag);
            if (prjEnableDisableOper != PRJ_ENABLE_DISABLE_NOTHING)
            {
               prjActivateDisactivateSigGen();
               printf("Enabling ADC timer \r\n");
               adcTimerEnable(); /*FIXME this is temporal to just enable ADC capture */
            }
            break; 
          }
           
        } /* switch (prjStage) */
        
        /* Process OscFlag */
        oscContinousProcess();
    } /*  while(1) */
    
} /* prjAppEntryPoint */

/* [] END OF FILE */

/* This is expected to be executed in an infinite loop */
uint8_t rotatorySwitchMonitor()
{
 
    uint8_t retFlag = 0;    
    
    if (isSetRotEncoderRightFlag())
    {
       retFlag |= PRJ_ROT_ENCODER_RIGHT_FLAG;   
       clearEncodedStatus(ROT_ENC_TURN_RIGHT_FLAG_MASK);
    }
    
    if (isSetRotEncoderLeftFlag())
    {    
       retFlag |= PRJ_ROT_ENCODER_LEFT_FLAG; 
       clearEncodedStatus(ROT_ENC_TURN_LEFT_FLAG_MASK);
    }
    
    if (isSetRotEncoderSwitchFlag())
    {  
       /* if button is pressed, cancel right/left flags */
       retFlag = PRJ_ROT_ENCODER_BUTTON_FLAG;
       clearEncodedStatus(ROT_ENC_SWITCH_PRESS_FLAG_MASK);    
    }
       
    /* if left and right flags are set, then cancel them */
    if ((retFlag & PRJ_ROT_ENCODER_RIGHT_LEFT_FLAG) == PRJ_ROT_ENCODER_RIGHT_LEFT_FLAG)
    {
        retFlag &= (uint8_t)(~PRJ_ROT_ENCODER_RIGHT_LEFT_FLAG);
    }
    
    return retFlag;
}

/* This function will handle the selection of a new option in 
   a menu based on the status reported by the rotatory switch.
   This will return true if any of the selected item was updated.
*/
bool rotatory_right_left_item_choose( uint8_t rotSwFlag, tMenuCfgSt *menuCfgSt)
{
   bool updateMenu = false; 
   if (rotSwFlag & PRJ_ROT_ENCODER_RIGHT_FLAG )
   {
       if (menuCfgSt->itemSelected < (menuCfgSt->numItems-1))
       {
            menuCfgSt->itemSelected++;
            updateMenu = true;
       }
   }
        
   if (rotSwFlag & PRJ_ROT_ENCODER_LEFT_FLAG )
   {
       if (menuCfgSt->itemSelected != 0)
       {
           menuCfgSt->itemSelected--;
           updateMenu = true;
        }
    }
   
   return updateMenu;
}

/* This function will handle the selection of a new option in 
   a field menu based on the status reported by the rotatory switch.
   This will return true if any of the selected item was updated.
*/
bool rotatory_right_left_field_item_choose(uint8_t rotSwFlag, tMenuFieldSetCfgSt *menuFieldCfgSt)
{
     bool updateMenu = false; 
   if (rotSwFlag & PRJ_ROT_ENCODER_RIGHT_FLAG )
   {
       if (menuFieldCfgSt->fieldSelected < (menuFieldCfgSt->numItems-1))
       {
            menuFieldCfgSt->fieldSelected++;
            updateMenu = true;
       }
   }
        
   if (rotSwFlag & PRJ_ROT_ENCODER_LEFT_FLAG )
   {
       if (menuFieldCfgSt->fieldSelected != 0)
       {
           menuFieldCfgSt->fieldSelected--;
           updateMenu = true;
        }
    }
   
   return updateMenu;
}
/* Function that allows to select the digit of a field parameter.
   If moving left or right of the digits of the field, it will
   return to the previous menu.
*/
void projSigGenFieldDigitSelectHandle(uint8_t rotSwFlag, tMenuFieldSetCfgSt *menuFieldSetCfg, 
                                       uint8_t itemIdx, tPrjStages prevPrjState, tPrjStages nextPrjState,
                                       prepareStateFunPtr preparePrevFunPtr)    
{
   bool updateMenu = false;
   uint8_t charSel = menuFieldSetCfg->fieldInfoCfgSt[itemIdx].charSel;
   uint8_t numDigits = menuFieldSetCfg->fieldInfoCfgSt[itemIdx].fieldCfg->numDigits;
   
   /* Handle Rotatory switch button operations */
   if ( rotSwFlag & PRJ_ROT_ENCODER_LEFT_FLAG )
   {    
     if (charSel == (numDigits-1))
     {
       menuFieldSetCfg->fieldInfoCfgSt[itemIdx].charSel = numDigits-1;       
       preparePrevFunPtr();
       prjStage = prevPrjState;
     }     
     else
     {
       menuFieldSetCfg->fieldInfoCfgSt[itemIdx].charSel = charSel+1;
       updateMenu = true;
     }
   }
   else if ( rotSwFlag & PRJ_ROT_ENCODER_RIGHT_FLAG )
   {    
     if (charSel == 0)
     {
       menuFieldSetCfg->fieldInfoCfgSt[itemIdx].charSel = numDigits-1;        
       preparePrevFunPtr();
       prjStage = prevPrjState;
     }     
     else
     {
       menuFieldSetCfg->fieldInfoCfgSt[itemIdx].charSel = charSel-1;
       updateMenu = true;
     }
   }
   else if (rotSwFlag & PRJ_ROT_ENCODER_BUTTON_FLAG)
   {
     displayFieldSetupMenu(menuFieldSetCfg, true, true);
     prjStage = nextPrjState;
   }

            
   if ( updateMenu )   
   {
     displayFieldSetupMenu(menuFieldSetCfg, true, false);
     menuFieldSetCfg->clearFlag = false;
   }
}


/* This will handle the configuration of a field at digit by digit basis */
void projSigGenFieldConfigHandle(uint8_t rotSwFlag, tMenuFieldSetCfgSt *menuFieldSetCfg, 
                                 uint8_t itemIdx, tPrjStages prevPrjState)
{
    tFieldDigitIncDec incDec = FIELD_DIGIT_NONE;
    bool updated = false;
    bool upDownDone = false;
    
    /* left -> Decrease */
    if ( rotSwFlag & PRJ_ROT_ENCODER_LEFT_FLAG )
    {
      incDec = FIELD_DIGIT_DEC;
    }
    else if ( rotSwFlag & PRJ_ROT_ENCODER_RIGHT_FLAG )
    {    
      incDec = FIELD_DIGIT_INC;
    }
    else if ( rotSwFlag & PRJ_ROT_ENCODER_BUTTON_FLAG )
    {
      /*Return to digit select state */
      prjStage = prevPrjState;
      upDownDone = true;
    }
    
    updated = displayGetFieldValueFromIncDecDigit(&menuFieldSetCfg->fieldInfoCfgSt[itemIdx],  incDec);
    
    if ( updated )
    {
      displayFieldSetupMenu(menuFieldSetCfg, true, true);
    }
    if ( upDownDone )
    {
      displayFieldSetupMenu(menuFieldSetCfg, true, false);
    }
}

/*--------------------------- STARTING POINT  ----------------------------*/
/* Process any operation needed in the PRJ_INIT_ST stage*/
void prjInitOperHanlde(void)
{
    /* Just clear rot encoder states and move to next state */
    prepareToEnterMainMenu();
    prjStage = PRJ_MAIN_MENU_ST;
}

/*--------------------------- MAIN MENU  ----------------------------*/
void prepareToEnterMainMenu(void)
{
   /* Just clear rot encoder states and move to next state */
    clearEncodedStatus(ROT_ENC_FLAG_MASK);
    mainMenuCfgSt.clearFlag = true;
}
/* This will handle the operations derived from the main menu*/
void prjMainMenuOperHandle( uint8_t rotSwFlag)
{
    bool updateMenu = false;
    uint8_t selItem = mainMenuCfgSt.itemSelected;
    /* Handle Rotatory switch button operations */
    if ( rotSwFlag & PRJ_ROT_ENCODER_BUTTON_FLAG)
    {
        switch (selItem)
        {
           /* If selected Signal Generator option */ 
           case  SIGNAL_GEN_MENU_IDX:
           case  OSCILLOSCOPE_SIGNAL_GEN_MENU_IDX:
           {
               prepareToEnterSigGenMenu1();
               prjStage = PRJ_SIG_GEN_MENU1_ST;  /* For now, in any case go to SIG GEN Menu */
               break;
           }
        }
    }
    else
    {
        updateMenu = rotatory_right_left_item_choose( rotSwFlag, &mainMenuCfgSt);        
    }
    /* Show current menu */
    if (updateMenu || mainMenuCfgSt.clearFlag) 
    {
       displayReactangleMenu(&mainMenuCfgSt); 
       mainMenuCfgSt.clearFlag = false;
    }
}

/*--------------------------- SIGNAL GENERATOR MENU 1 ----------------------*/
void prepareToEnterSigGenMenu1(void)
{
    clearEncodedStatus(ROT_ENC_FLAG_MASK);
    sigGenMenu1CfgSt.clearFlag = true;
}

void prjSigGenMenu1OperHandle( uint8_t rotSwFlag)
{
   bool updateMenu = false;
   uint8_t selItem = sigGenMenu1CfgSt.itemSelected;
  
   /* Handle Rotatory switch button operations */ 
   if (rotSwFlag & PRJ_ROT_ENCODER_BUTTON_FLAG)
   {
     switch (selItem)
     {
       /* Check if the option selected is the one used to return to the previous menu*/
       case MENU_BACK_IDX:
       {
          prepareToEnterMainMenu();
          prjStage = PRJ_MAIN_MENU_ST;
          break;
       }
       case SIG_GEN_WAVE_CLOCK_IDX:
       {
          /* Go to PRJ_SIG_GEN_SET_FREQ_ST */
          prepareToEnterAutoDigitalMenu();
          prjStage = PRJ_SIG_GEN_AUTO_DIGITAL_MENU_ST;
          break;
       }
       default:  /* For any other waveform (analog) go to show  PRJ_SIG_GEN_MENU2_ST*/
       {
          prepareToEnterSigGenMenu2();
          prjStage = PRJ_SIG_GEN_MENU2_ST;
       }
      }
   }
   else
   {
      updateMenu = rotatory_right_left_item_choose( rotSwFlag, &sigGenMenu1CfgSt);
   }
    
   if ( updateMenu || sigGenMenu1CfgSt.clearFlag)
   {
       displayReactangleMenu(&sigGenMenu1CfgSt); 
       sigGenMenu1CfgSt.clearFlag = false;              
    }
}
/*--------------------------- SIGNAL GENERATOR MENU 2 ----------------------*/
void prepareToEnterSigGenMenu2(void)
{
    clearEncodedStatus(ROT_ENC_FLAG_MASK);
    sigGenMenu2CfgSt.clearFlag = true;
}

void prjSigGenMenu2OperHandle( uint8_t rotSwFlag)
{
   bool updateMenu = false;
   uint8_t selItem = sigGenMenu2CfgSt.itemSelected;
  
   /* Handle Rotatory switch button operations */ 
   if (rotSwFlag & PRJ_ROT_ENCODER_BUTTON_FLAG)
   {
     switch (selItem)
     {
       /* Check if the option selected is the one used to return to the previous menu*/
       case MENU_BACK_IDX:
       {
          prepareToEnterSigGenMenu1();
          prjStage = PRJ_SIG_GEN_MENU1_ST;
          break;
       }
       case SIG_GEN_OPER_MODE_AUTO_IDX:
       {
         prepareToEnterAutoAnalogMenu();
         prjStage = PRJ_SIG_GEN_AUTO_ANALOG_MENU_ST;
         break;
       }
       case SIG_GEN_OPER_MODE_MANUAL_IDX:
       {
         prepareToEnterManualAnalogMenu(); 
         prjStage = PRJ_SIG_GEN_MANUAL_ANALOG_MENU_ST;
         break;
       }
      }
   }
   else
   {
      updateMenu = rotatory_right_left_item_choose( rotSwFlag, &sigGenMenu2CfgSt);
   }
    
   if ( updateMenu || sigGenMenu2CfgSt.clearFlag)
   {
       displayReactangleMenu(&sigGenMenu2CfgSt); 
       sigGenMenu2CfgSt.clearFlag = false;              
    }
}

/* ------------------ SIGNAL GENERATOR READY (GO) MENU --------------------- */

void prepareToEnterSigGenReadyMenu(void)
{
   clearEncodedStatus(ROT_ENC_FLAG_MASK);
   sigGenReadyMenuCfgSt.clearFlag = true;
   /* Add Signal Generator configuration based on the parameters set */
   prjEnableDisableOper = PRJ_ENABLE_SIG_GEN;
}

void prjSigGenReadyOperHandle( uint8_t rotSwFlag)
{
   bool backToCfg = false;
   /* Handle Rotatory switch button operations. Only one option, 
      don't check for left or right options  */ 
   if (rotSwFlag & PRJ_ROT_ENCODER_BUTTON_FLAG)
   {
     /* If button is pressed check to which menu needs to return to */
     if (prjStage == PRJ_SIG_GEN_AUTO_ANALOG_READY_ST)
     {
        prepareToEnterAutoAnalogMenu();
        prjStage = PRJ_SIG_GEN_AUTO_ANALOG_MENU_ST;
     }
     else if ( prjStage == PRJ_SIG_GEN_MANUAL_ANALOG_READY_ST )
     {
        prepareToEnterManualAnalogMenu();
        prjStage = PRJ_SIG_GEN_MANUAL_ANALOG_MENU_ST;
     }
     if (prjStage == PRJ_SIG_GEN_AUTO_DIGITAL_READY_SET_ST)
     {
        prepareToEnterAutoDigitalMenu();
        prjStage = PRJ_SIG_GEN_AUTO_DIGITAL_MENU_ST;
     }
     prjEnableDisableOper = PRJ_DISABLE_SIG_GEN;
     backToCfg = true; /* Set flag to indicate that need 
                          to return to configuration. */
   }
      
   if (( sigGenReadyMenuCfgSt.clearFlag ) && (!backToCfg))
   {
       displayReactangleMenu(&sigGenReadyMenuCfgSt); 
       sigGenReadyMenuCfgSt.clearFlag = false;              
   }
   
}

/*--------------------------- SIGNAL GENERATOR AUTO-ANALOG MENU  ----------------------*/
/* Prepare to enter Auto-Analog Menu*/
void prepareToEnterAutoAnalogMenu(void)
{
    clearEncodedStatus(ROT_ENC_FLAG_MASK);
    sigGenMenuFieldAutoAnalog.clearFlag = true;
}

void projSigGenAutoAnalogHandle(uint8_t rotSwFlag)
{
   bool updateMenu = false;
   uint8_t selItem = sigGenMenuFieldAutoAnalog.fieldSelected;
   
   /* Handle Rotatory switch button operations */ 
   if (rotSwFlag & PRJ_ROT_ENCODER_BUTTON_FLAG)
   {
     switch (selItem)
     {
       /* Check if the option selected is the one used to return to the previous menu*/
       case MENU_BACK_IDX:
       {
          prepareToEnterSigGenMenu2();
          prjStage = PRJ_SIG_GEN_MENU2_ST;
          break;
       }
       case SIG_GEN_FIELD_FREQ_IDX:
       {
          /* Start configure the selected field*/
          prepareToEnterAutoAnalogFreqConfigField();
          prjStage = PRJ_SIG_GEN_AUTO_ANALOG_FREQ_SET_ST1;          
          break;
       }
       case SIG_GEN_FIELD_AUTO_GO_IDX:
       {
          /* Ready to start signal gen */
          prepareToEnterSigGenReadyMenu();
          prjStage = PRJ_SIG_GEN_AUTO_ANALOG_READY_ST;
       }
     }
   }
   else
   {
      updateMenu = rotatory_right_left_field_item_choose( rotSwFlag, &sigGenMenuFieldAutoAnalog );
   }
            
   if ( updateMenu || sigGenMenuFieldAutoAnalog.clearFlag)
   {
       displayFieldSetupMenu(&sigGenMenuFieldAutoAnalog, false, false);
       sigGenMenuFieldAutoAnalog.clearFlag = false;              
    }
}


/* ---------------- SIGNAL GENERATOR ANALOG AUTO FREQ CONFIGURATION HANDLING ----------------- */
void prepareToEnterAutoAnalogFreqConfigField(void)
{
    clearEncodedStatus(ROT_ENC_FLAG_MASK);
    sigGenMenuFieldAutoAnalog.fieldInfoCfgSt[SIG_GEN_FIELD_FREQ_IDX].charSel = 
                           SIG_GEN_FIELD_FREQ_ANALOG_NUM_DIGITS-1;
    displayFieldSetupMenu(&sigGenMenuFieldAutoAnalog, true, false);  
}

/* In PRJ_SIG_GEN_AUTO_ANALOG_FREQ_SET_ST1 we need to select the digit to configure */
void projSigGenAutoAnalogFreq1ConfigHandle(uint8_t rotSwFlag)
{

   projSigGenFieldDigitSelectHandle(rotSwFlag, &sigGenMenuFieldAutoAnalog, 
                                    SIG_GEN_FIELD_FREQ_IDX, PRJ_SIG_GEN_AUTO_ANALOG_MENU_ST,
                                    PRJ_SIG_GEN_AUTO_ANALOG_FREQ_SET_ST2,
                                    &prepareToEnterAutoAnalogMenu);  
                                    
}
/* This will configure the frequency at digit by digit basis */
void projSigGenAutoAnalogFreq2ConfigHandle(uint8_t rotSwFlag)
{
 
    projSigGenFieldConfigHandle(rotSwFlag, &sigGenMenuFieldAutoAnalog,
                                SIG_GEN_FIELD_FREQ_IDX, PRJ_SIG_GEN_AUTO_ANALOG_FREQ_SET_ST1);
}



/*--------------------------- SIGNAL GENERATOR MANUAL-ANALOG MENU  ----------------------*/
/* Prepare to enter Manual-Analog Menu*/
void prepareToEnterManualAnalogMenu(void)
{
    clearEncodedStatus(ROT_ENC_FLAG_MASK);
    sigGenMenuFieldManualAnalog.clearFlag = true;
}

void projSigGenManualAnalogHandle(uint8_t rotSwFlag)
{
   bool updateMenu = false;
   uint8_t selItem = sigGenMenuFieldManualAnalog.fieldSelected;
   
   /* Handle Rotatory switch button operations */ 
   if (rotSwFlag & PRJ_ROT_ENCODER_BUTTON_FLAG)
   {
     switch (selItem)
     {
       /* Check if the option selected is the one used to return to the previous menu*/
       case MENU_BACK_IDX:
       {
          prepareToEnterSigGenMenu2();
          prjStage = PRJ_SIG_GEN_MENU2_ST;
          break;
       }
            
       case SIG_GEN_FIELD_NUM_SAMPLES_IDX:
       {
          /* Start configure the selected field*/
          prepareToEnterManualAnalogNumSamplesConfigField();
          prjStage = PRJ_SIG_GEN_MANUAL_ANALOG_MENU_NUMSAMPLES_SET_ST1;                    
          break;
       }
      
       case SIG_GEN_FIELD_SAMPLE_PERIOD_IDX:
       {
          /* Start configure the selected field*/
          prepareToEnterManualAnalogSamplePeriodConfigField();
          prjStage = PRJ_SIG_GEN_MANUAL_ANALOG_MENU_SAMPLEPERIOD_SET_ST1;          
          break;
       }
    
       case SIG_GEN_FIELD_MANUAL_GO_IDX:
       {
          /* Ready to start signal gen */
          prepareToEnterSigGenReadyMenu();
          prjStage = PRJ_SIG_GEN_MANUAL_ANALOG_READY_ST;          
          break;
       }
     }
   }
   else
   {
      updateMenu = rotatory_right_left_field_item_choose( rotSwFlag, &sigGenMenuFieldManualAnalog );
   }
            
   if ( updateMenu || sigGenMenuFieldManualAnalog.clearFlag)
   {
       displayFieldSetupMenu(&sigGenMenuFieldManualAnalog, false, false);
       sigGenMenuFieldManualAnalog.clearFlag = false;
   }
}

/* ------- SIGNAL GENERATOR ANALOG MANUAL CONFIGURATION HANDLING (Num Samples) ------*/
void prepareToEnterManualAnalogNumSamplesConfigField(void)
{
    clearEncodedStatus(ROT_ENC_FLAG_MASK);
    sigGenMenuFieldManualAnalog.fieldInfoCfgSt[SIG_GEN_FIELD_NUM_SAMPLES_IDX].charSel = 
                           SIG_GEN_FIELD_NUM_SAMPLES_NUM_DIGITS-1;
    displayFieldSetupMenu(&sigGenMenuFieldManualAnalog, true, false);  
}

/* In PRJ_SIG_GEN_MANUAL_ANALOG_MENU_NUMSAMPLES_SET_ST1 we need to select the digit to configure */
void projSigGenManualAnalogNumSamples1ConfigHandle(uint8_t rotSwFlag)
{

   projSigGenFieldDigitSelectHandle(rotSwFlag, &sigGenMenuFieldManualAnalog, 
                                    SIG_GEN_FIELD_NUM_SAMPLES_IDX, PRJ_SIG_GEN_MANUAL_ANALOG_MENU_ST,
                                    PRJ_SIG_GEN_MANUAL_ANALOG_MENU_NUMSAMPLES_SET_ST2,
                                    &prepareToEnterManualAnalogMenu);  
                                    
}
/* This will configure the frequency at digit by digit basis */
void projSigGenManualAnalogNumSamples2ConfigHandle(uint8_t rotSwFlag)
{
 
    projSigGenFieldConfigHandle(rotSwFlag, &sigGenMenuFieldManualAnalog,
                                SIG_GEN_FIELD_NUM_SAMPLES_IDX, PRJ_SIG_GEN_MANUAL_ANALOG_MENU_NUMSAMPLES_SET_ST1);
}

/* ------- SIGNAL GENERATOR ANALOG MANUAL CONFIGURATION HANDLING (SamplePeriod) ------*/
void prepareToEnterManualAnalogSamplePeriodConfigField(void)
{
    clearEncodedStatus(ROT_ENC_FLAG_MASK);
    sigGenMenuFieldManualAnalog.fieldInfoCfgSt[SIG_GEN_FIELD_SAMPLE_PERIOD_IDX].charSel = 
                           SIG_GEN_FIELD_SAMPLE_PERIOD_NUM_DIGITS-1;
    displayFieldSetupMenu(&sigGenMenuFieldManualAnalog, true, false);  
}

/* In PRJ_SIG_GEN_MANUAL_ANALOG_MENU_NUMSAMPLES_SET_ST1 we need to select the digit to configure */
void projSigGenManualAnalogSamplePeriod1ConfigHandle(uint8_t rotSwFlag)
{
    
   projSigGenFieldDigitSelectHandle(rotSwFlag, &sigGenMenuFieldManualAnalog, 
                                    SIG_GEN_FIELD_SAMPLE_PERIOD_IDX, PRJ_SIG_GEN_MANUAL_ANALOG_MENU_ST,
                                    PRJ_SIG_GEN_MANUAL_ANALOG_MENU_SAMPLEPERIOD_SET_ST2,
                                    &prepareToEnterManualAnalogMenu);  
                                    
}
/* This will configure the frequency at digit by digit basis */
void projSigGenManualAnalogSamplePeriod2ConfigHandle(uint8_t rotSwFlag)
{
 
    projSigGenFieldConfigHandle(rotSwFlag, &sigGenMenuFieldManualAnalog,
                                SIG_GEN_FIELD_SAMPLE_PERIOD_IDX, PRJ_SIG_GEN_MANUAL_ANALOG_MENU_SAMPLEPERIOD_SET_ST1);
}


/*------------------- SIGNAL GENERATOR DIGITAL MENU (only supported auto)  ----------------------*/
/* Prepare to enter Auto-Digital (CLK) Menu*/
void prepareToEnterAutoDigitalMenu(void)
{
    clearEncodedStatus(ROT_ENC_FLAG_MASK);
    sigGenMenuFieldAutoDigital.clearFlag = true;
}

void projSigGenAutoDigitalHandle(uint8_t rotSwFlag)
{
   bool updateMenu = false;
   uint8_t selItem = sigGenMenuFieldAutoDigital.fieldSelected;
   
   /* Handle Rotatory switch button operations */ 
   if (rotSwFlag & PRJ_ROT_ENCODER_BUTTON_FLAG)
   {
     switch (selItem)
     {
       /* Check if the option selected is the one used to return to the previous menu*/
       case MENU_BACK_IDX:
       {
          prepareToEnterSigGenMenu1();
          prjStage = PRJ_SIG_GEN_MENU1_ST;
          break;
       }
       case SIG_GEN_FIELD_FREQ_IDX:
       {
          /* Start configure the selected field*/
          prepareToEnterAutoDigitalFreqConfigField();
          prjStage = PRJ_SIG_GEN_AUTO_DIGITAL_FREQ_SET_ST1;          
          break;
       }
       case SIG_GEN_FIELD_AUTO_GO_IDX:
       {
          /* Ready to start signal gen  */
          prepareToEnterSigGenReadyMenu();
          prjStage = PRJ_SIG_GEN_AUTO_DIGITAL_READY_SET_ST;
       }
     }
   }
   else
   {
      updateMenu = rotatory_right_left_field_item_choose( rotSwFlag, &sigGenMenuFieldAutoDigital );
   }
            
   if ( updateMenu || sigGenMenuFieldAutoDigital.clearFlag)
   {
       displayFieldSetupMenu(&sigGenMenuFieldAutoDigital, false, false);
       sigGenMenuFieldAutoDigital.clearFlag = false;              
    }       
}

/* ------- SIGNAL GENERATOR DIGITAL AUTO FREQUENCY CONFIGURATION HANDLING  ------*/
void prepareToEnterAutoDigitalFreqConfigField(void)
{
    clearEncodedStatus(ROT_ENC_FLAG_MASK);
    sigGenMenuFieldAutoDigital.fieldInfoCfgSt[SIG_GEN_OPER_MODE_AUTO_IDX].charSel = 
                           SIG_GEN_FIELD_FREQ_DIGITAL_NUM_DIGITS-1;
    displayFieldSetupMenu(&sigGenMenuFieldAutoDigital, true, false);  
}

/* In PRJ_SIG_GEN_AUTO_DIGITAL_MENU_FREQ_SET_ST1 we need to select the digit to configure */
void projSigGenAutoDigitalFreq1ConfigHandle(uint8_t rotSwFlag)
{

   projSigGenFieldDigitSelectHandle(rotSwFlag, &sigGenMenuFieldAutoDigital, 
                                    SIG_GEN_FIELD_FREQ_IDX, PRJ_SIG_GEN_AUTO_DIGITAL_MENU_ST,
                                    PRJ_SIG_GEN_AUTO_DIGITAL_FREQ_SET_ST2,
                                    &prepareToEnterAutoDigitalMenu);  
                                    
}
/* This will configure the frequency at digit by digit basis */
void projSigGenAutoDigitalFreq2ConfigHandle(uint8_t rotSwFlag)
{
 
    projSigGenFieldConfigHandle(rotSwFlag, &sigGenMenuFieldAutoDigital,
                                SIG_GEN_FIELD_FREQ_IDX, PRJ_SIG_GEN_AUTO_DIGITAL_FREQ_SET_ST1);
}
/* This function will activate or disactivate the signal generation.
   This will determine which signal is going to be generated and with
   which parameters.   
*/
void prjActivateDisactivateSigGen(void)
{    
    tSigGenWaveType sigType;
    uint32_t freqHz = 0;
    uint16_t numSamples = 0;
    uint32_t sampleTimeNs = 0;
    uint32_t timerPeriodTicks = 0;
    uint8_t idx = 0;
    bool isAuto = false;
    bool paramGot = false;
    
    if (prjEnableDisableOper == PRJ_ENABLE_SIG_GEN )
    {
       /* Enable signal generator, but it's needed to determine
          which waveform is needed 
        */
        /* Get signal type converting from Menu Index signal to real */
        switch (sigGenMenu1CfgSt.itemSelected)
        {
          case SIG_GEN_WAVE_SAW_IDX:    { sigType = SIG_GEN_WAVE_SAW; break; }
          case SIG_GEN_WAVE_INVSAW_IDX: { sigType= SIG_GEN_WAVE_INV_SAW; break; }
          case SIG_GEN_WAVE_SINE_IDX:   { sigType= SIG_GEN_WAVE_SINE; break; }
          case SIG_GEN_WAVE_TRIANGLE_IDX: { sigType= SIG_GEN_WAVE_TRIANGLE; break;}
          case SIG_GEN_WAVE_SQR_ANALOG_IDX: { sigType= SIG_GEN_WAVE_SQUARE;break;}
          case SIG_GEN_WAVE_CLOCK_IDX: { sigType= SIG_GEN_WAVE_SQUARE_DIGITAL; break;}
          default: sigType= SIG_GEN_WAVE_SINE;
        }
        
        
        /* All the signals support Manual and Atuto  except 
        SIG_GEN_WAVE_CLOCK_IDX */
        if ( sigType == SIG_GEN_WAVE_SQUARE_DIGITAL)
        {
          /* Digital clock signal is Auto */
          idx = SIG_GEN_FIELD_FREQ_IDX;
          freqHz = sigGenMenuFieldAutoDigital.fieldInfoCfgSt[idx].currentValue;
          isAuto = true;
          paramGot = true;
        }
        else
        {
          /* Is Auto or Manual */
          if (sigGenMenu2CfgSt.itemSelected == SIG_GEN_OPER_MODE_AUTO_IDX)
          {
            /* In Auto only freq is neede */
            idx = SIG_GEN_FIELD_FREQ_IDX;
            freqHz = sigGenMenuFieldAutoAnalog.fieldInfoCfgSt[idx].currentValue;
            isAuto = true;
            paramGot = true;
          }
          else if (sigGenMenu2CfgSt.itemSelected == SIG_GEN_OPER_MODE_MANUAL_IDX)
          {
            /* Manual parameters */
            idx = SIG_GEN_FIELD_NUM_SAMPLES_IDX;
            numSamples = sigGenMenuFieldManualAnalog.fieldInfoCfgSt[idx].currentValue;
            idx = SIG_GEN_FIELD_SAMPLE_PERIOD_IDX;
            sampleTimeNs = sigGenMenuFieldManualAnalog.fieldInfoCfgSt[idx].currentValue;
            timerPeriodTicks = sigGenConvertSampleTime2Ticks(sampleTimeNs);
            isAuto = false;
            paramGot = true;
          }
        }
        
        if ( paramGot )
        {
          if ( isAuto )
          {
            /* Auto */
            printf("@prjActivateDisactivateSigGen() - Auto: Sig: %d Freq: %d\r\n", sigType, freqHz);
            (void)reconfigureSigGenWaveAuto(sigType, freqHz);
          }
          else
          {
             /* Manual */
             printf("@prjActivateDisactivateSigGen() - Manual: Sig: %d Tticks: %d NumSamples: %d \r\n", 
                               sigType, timerPeriodTicks, numSamples);
             (void)reconfigureSigGenWaveManual(sigType, timerPeriodTicks, numSamples);
          }          
        }
        prjEnableDisableOper = PRJ_ENABLE_DISABLE_NOTHING;
    }
    else if ( prjEnableDisableOper == PRJ_DISABLE_SIG_GEN )    
    {
      sigGenTimerDisable();
      sigGenDigitalDisable();
      prjEnableDisableOper = PRJ_ENABLE_DISABLE_NOTHING;
    }
}  
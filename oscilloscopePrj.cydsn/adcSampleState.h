/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/


/*
   
   The samples taken are divided in sets of certain number of samples in each set. This is
   done to allow start processing the samples without have to wait until all the samples 
   in the buffer are aquired. This is very helpful specially in high sample times in which
   we don´t want to wait too much without see any point draw in the screen. In this case
   as higher sample times the number of samples in a set is lower.


   sa0     sa1     sa2     sa3           sa0     sa1     sa2     sa3     
   <             set 1       > < setup > <             set 1       > < setup > ...
                                 next                                  next
                                 Set                                 Set
                                
   conv0  conv1  conv2   conv3          conv0 conv1   conv2    conv3
                                
   plot0  plot1  plot2   plot3          plot0  plot1  plot2   plot3
                                
   Time to process 
   all the samples  =  (#samples in set)( sample_time + num_channels(covert_time + plot_time ))
   in a set
                                
   We want to refresh the plot in screen at least every WINDOW_TIME_REFRESH(nsec), based on this,
   the maximum sample time for which this is achieved is:
   
   limit_high_sample_time = ( WINDOW_TIME_REFRESH / ADC_MIN_SIZE_SAMPLE_SET) - (num_channels(covert_time + plot_time ))
          
   So, any sample_time above of this time will need to have a number of samples in set equal to  ADC_MIN_SIZE_SAMPLE_SET
   The refresh time divided by the min number of samples in the set doesn´t depend on the numner of channels enabled as 
   in the same period of time the n channels are sampled in parallel.

   Now, if the sample_time is lower than the time needed to set up the configuration for the next Set,
   we can't have more than one Set with the maximum samples in a Set.

   3 scenarios:
   - Sample time lower than "set up time for next set" -> Number of sets = 1
   - Sample time higher than limit_high_sample_time    -> Number of samples in set = 2
   - In the middle of above conditions:
       #samples in set = WINDOW_TIME_REFRESH / (sample_time + num_channels(covert_time + plot_time ))

*/



#ifndef ADC_SAMPLE_STATE_H

#define ADC_SAMPLE_STATE_H   
 
#include "psocUtilities.h"    
    
#define ADC_NUM_SAMPLES_BUFF_SIZE_PER_CHANNEL  1024   /* Number of ADC samples that could be stored per channel */  
#define ADC_MIN_SIZE_SAMPLE_SET       2   /* Minumum number of samples that will be part of a Set */
#define ADC_SAMPLE_SET_STATE_NUM_NUM_BITS 2 /* Number of bits used to track the state of a sample     */
#define ADC_SAMPLE_SET_STATE_VAR_SIZE_BITS 8  /* The type of variable to keep the state of a sample Set */
#define ADC_SAMPLE_MAX_NUM_SETS  (ADC_NUM_SAMPLES_BUFF_SIZE_PER_CHANNEL/ADC_MIN_SIZE_SAMPLE_SET)  /* Max number of sample sets */
    
#define ADC_SAMPLE_SET_STATE_ITEMS_PER_WORD  (ADC_SAMPLE_SET_STATE_VAR_SIZE_BITS/ADC_SAMPLE_SET_STATE_NUM_NUM_BITS)
#define ADC_SAMPLE_SET_NUM_STATE_WORDS  (ADC_SAMPLE_MAX_NUM_SETS/ADC_SAMPLE_SET_STATE_ITEMS_PER_WORD)
#define ADC_SAMPLE_SET_STATE_MASK  ((1 << ADC_SAMPLE_SET_STATE_NUM_NUM_BITS)-1)
    
#define ADC_SAMPLE_INVALID_ST 0  /* The value in the sample Set is invalid */
#define ADC_SAMPLE_AQUIRED_ST 1  /* The value in the sample Set is valid (ADC value)*/
#define ADC_SAMPLE_CONVERT_ST 2  /* The value in the sample Set has been converted to be plot */
 
#if (ADC_SAMPLE_SET_STATE_VAR_SIZE_BITS == 8)
typedef uint8_t tSampleStateVar;    
#endif

#if (ADC_SAMPLE_SET_STATE_VAR_SIZE_BITS == 16)
typedef uint16_t tSampleStateVar;    
#endif

#if (ADC_SAMPLE_SET_STATE_VAR_SIZE_BITS == 32)
typedef uint32_t tSampleStateVar;    
#endif    
   
typedef tSampleStateVar tAdcSampleStateBuffer[ADC_SAMPLE_SET_NUM_STATE_WORDS];


typedef struct
{   
  uint16_t numSampleSets;    /* Number of sample Sets at the moment (from 1) */
  uint16_t numSamplesPerSet; /* Number of samples per Set at the moment */
}tAdcSampleSetInfo;

typedef struct
{
  uint8_t numMaxChannels;      /* Number maximum of channels that will be supported */
  uint8_t numChannelsEnabled;  /* Number of channels enabled by default */
  uint32_t sampleTimeNs;     /* Initial sample time */
  uint32_t windowRefreshTimeNs;  /* Expected time that the screen need to be refresh (best effort) */
  uint32_t convertTimePerSampleNs; /* Time needed to convert a sample (ADC->Real Value)*/
  uint32_t plotTimePerSampleNs;    /* Time needed to display a sample in the screen */
  uint32_t timeToPrepareForNextSetNs;  
} tAdcSampleStateCfgInit;
 
typedef struct
{
  uint8_t numMaxChannels;  
  uint8_t numChannelsEnabled;
  uint32_t sampleTimeNs;  
  tAdcSampleSetInfo adcSampleSetInfo;  /* Sample Sets information */
  tAdcSampleStateBuffer *pSampleStateBuffer;
  uint32_t windowRefreshTimeNs; /* Best effort time to refresh the screen */
  uint32_t sampleProcessTimeNs; /* Time to process a sample (convert + plot) */
  uint32_t timeToPrepareForNextSetNs; /* Time that is needed to prepare the sampling 
                                         of the next set of samples */  
  bool adcSampleStateInitDone;
} tAdcSampleStateSt;


bool adcSampleStateInit(tAdcSampleStateCfgInit *cfgIn, tAdcSampleSetInfo *adcSampleSetInfo );
void adcSampleStateGetSetsInfo(  tAdcSampleSetInfo *adcSampleSetInfo  );
bool adcSampleStateRecalculateSetInfo(uint8_t newNumChannelsEnabled, uint32_t newSampleTimeNs, tAdcSampleSetInfo *adcSampleSetInfo  );
uint8_t getSampleSetState(uint8_t channelNum, uint16_t numSampleSet);
void setSampleSetState(uint8_t channelNum, uint16_t numSampleSet, uint8_t state);
void setSampleSetInvalid(uint8_t channelNum, uint16_t numSampleSet);
bool  isSampleSetReadyToConvert(uint8_t channelNum, uint16_t numSampleSet);
bool  isSampleSetReadyToPlot(uint8_t channelNum, uint16_t numSampleSet);
bool isSampleSetReadyAllChannels( uint16_t numSampleSet, uint8_t readyState);
bool isAllSetReadyAllChannels( uint8_t readyState);
void adcSampleClearAllSetStateBuffer(uint8_t channelNum);

#endif

/* [] END OF FILE */

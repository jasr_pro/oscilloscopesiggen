/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef PROJECT_DISPLAY_H
 
#define PROJECT_DISPLAY_H
#include "psocUtilities.h"


#define MENU_GAP_BETWEEN_OPTIONS 20 /*Num of pixels in Y left between 2 options in the menu*/    
#define MENU_GAP_BETWEEN_OPTIONS_HALF (MENU_GAP_BETWEEN_OPTIONS>>1) /*Num of pixels in Y left between 2 options 10 above and 10 below*/
#define MENU_GAP_BETWEEN_FIELD_AND_VAL  10    
    
#define MENU_XCORD_TO_START 10  /*Pixels*/
#define MENU_VAL_FIELD_XCORD_TO_START  160    
#define MENU_FIELD_XCORD_TO_START 5
#define MAX_NUM_CHARS_FIELD_VAL  13  /*12 chars + end of stirng */
    
#define MENU_REC_HEADER_TEXT "Menu Select:"
#define MENU_REC_HEADER_NO_FIT_TEXT "Menu Select:v ^"
    
#define MENU_FIELD_HEADER_TEXT "Config fields:"
#define MENU_FIELD_HEADER_NO_FIT_TEXT "Config fields:v ^"    
    
typedef struct{
    char *itemText;
    uint8_t idx;
}tMenuTextIdx;
    
typedef struct{
  //const char **menuText;
  const tMenuTextIdx *menuTextIdx;  
  uint8_t numItems; 
  uint16_t colorText;
  uint16_t colorOpSelect;
  uint16_t colorBackGround;  
  uint8_t textSize;    
  uint8_t itemSelected; /*Count from 0 */
  uint8_t maxTextLen;
  bool    clearFlag;      
} tMenuCfgSt;
  
typedef struct{
  char *itemText;
  uint8_t idx;
  bool    hasValue; /* Used to determine if the field has any configurable value */
  uint32_t maxValue;
  uint32_t minValue;  
  uint8_t numDigits;      
}tFieldInfoCfg;

typedef struct{
  const tFieldInfoCfg *fieldCfg;
  uint8_t charSel;  /* Character in the field selected <(numDigits-1).. 2 1 0>*/
  uint32_t currentValue;
}fieldInfoCfgSt;

typedef struct{
  fieldInfoCfgSt *fieldInfoCfgSt;
  uint8_t numItems; 
  uint16_t colorText;
  uint16_t colorOpSelect;
  uint16_t colorDigitSelect;
  uint16_t colorBackGround;  
  uint8_t textSize; 
  uint8_t fieldSelected; /*Count from 0 */
  uint8_t maxTextLen;
  uint8_t maxValueLen;  
  bool    clearFlag; 
}tMenuFieldSetCfgSt;

typedef enum{
  FIELD_DIGIT_NONE,
  FIELD_DIGIT_INC,
  FIELD_DIGIT_DEC    
}tFieldDigitIncDec;

uint8_t prjDisplayInit(void);
void prjBannerStart();
void displayReactangleMenu(tMenuCfgSt *menuCfgSt);
void displayFieldSetupMenu(tMenuFieldSetCfgSt *menuCfgSt, bool cfgValActive, bool cfgDigitActive);
bool displayGetFieldValueFromIncDecDigit(fieldInfoCfgSt *cfgSt, tFieldDigitIncDec  incDec);
#endif


/* [] END OF FILE */

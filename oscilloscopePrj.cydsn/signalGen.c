/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <stdio.h>
#include "signalGen.h"
#include "DMA_SigGen_dma.h"
#include "VDAC8_SigGen.h"
#include "TimerSigGen.h"
#include "DevicesCtrlReg.h"
#include "common.h"
#include "projectApp.h"

    /* Defines for DMA_SigGen */
#define DMA_SigGen_BYTES_PER_BURST 1
#define DMA_SigGen_REQUEST_PER_BURST 1
#define DMA_SigGen_SRC_BASE (CYDEV_SRAM_BASE)
#define DMA_SigGen_DST_BASE (CYDEV_PERIPH_BASE)

/* Variable declarations for DMA_SigGen */
/* Move these variable declarations to the top of the function */
static uint8 DMA_SigGen_Chan;
static uint8 DMA_SigGen_TD[1] = {0};
static uint8_t sigGenBuff[SIGGEN_MAX_BUF_SIZE];
static uint16_t sigGenDmaNumSamples = SIGGEN_MAX_BUF_SIZE;
static uint32_t sigGenTimerPeriodTicks = 1000;
static bool timerState = false;

extern tPrjStatus prjStatus;

/* Signal Generator Init 
   
  1) Initialize SigGen Timer
  2) Disable SigGen Timer
  3) Disable Signal Generator Digital Output
  4) Get signal parameters for the default Signal Gen Configuration
     (num samples, sample time) and generate wave form samples in
     the signal generator buffer. Default is
     SIGGEN_INIT_FREQ_HZ, Sine Waveform
  5) Convert sample time to Sig Gen timer ticks
  6) Write prriod time to the timer
  7) Configure Signal Generator DMA
  8) Initialize VDAC
  

   At this time the Signal Generator is fully configured to generate the
   default waveform and only need that the Sig Gen Timer gets enabled.

*/
void signalGenInit()
{
    /* Initialize timer for signal generator */ 
    tsigWaveInfo signalInfo;
    
    TimerSigGen_Start(); /* Enable timer, but Hardware Enable signal is not set*/
    sigGenTimerDisable(); /* Disable the timer by Hardware */
    sigGenDigitalDisable();  /* Disable digital output signal */
    
    /* Assign default period and wave form */
    (void)getWaveSamplesForSignal( SIG_GEN_WAVE_SINE,
                                   (uint32_t)SIGGEN_INIT_FREQ_HZ, 
                                   &signalInfo, sigGenBuff);
    
    printf("@signalGenInit() - numSamples: %d sampletimeNs: %ld \r\n", signalInfo.numSamples,signalInfo.sampleTimeNs );
    /*printf("@signalGenInit() - sample1: %d sample10: %d sample100: %d \r\n", 
            sigGenBuff[0], sigGenBuff[10], sigGenBuff[100]);  */
    
    sigGenDmaNumSamples = signalInfo.numSamples;
    
    sigGenTimerPeriodTicks = sigGenConvertSampleTime2Ticks(signalInfo.sampleTimeNs );
    printf("@signalGenInit() - ticks: %ld \r\n", sigGenTimerPeriodTicks);
    TimerSigGen_WritePeriod(sigGenTimerPeriodTicks); /* Final sample rate should be < 1 MPS */
         
    
    /* DMA Configuration for Signal Generator */    
    DMA_SigGen_Chan = DMA_SigGen_DmaInitialize(DMA_SigGen_BYTES_PER_BURST, DMA_SigGen_REQUEST_PER_BURST, 
    HI16(DMA_SigGen_SRC_BASE), HI16(DMA_SigGen_DST_BASE));
    DMA_SigGen_TD[0] = CyDmaTdAllocate();
    CyDmaTdSetConfiguration(DMA_SigGen_TD[0], sigGenDmaNumSamples, DMA_END_CHAIN_TD,  DMA_SigGen__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR | CY_DMA_TD_AUTO_EXEC_NEXT);
    CyDmaTdSetAddress(DMA_SigGen_TD[0], LO16((uint32)sigGenBuff), LO16((uint32)VDAC8_SigGen_Data_PTR));
    CyDmaChSetInitialTd(DMA_SigGen_Chan, DMA_SigGen_TD[0]);
    CyDmaChEnable(DMA_SigGen_Chan, 1);
    
    /* Enable VDAC */
     VDAC8_SigGen_Start();
    
    prjStatus.sigGenInitDone = true;
}

/* Enable Signal Generator Timer */
void sigGenTimerEnable()
{
    uint8_t devCtrlRegVal;
    
    if ( timerState == false)
    {
      devCtrlRegVal = DevicesCtrlReg_Read();
      /*Clear corresponding pin */
      devCtrlRegVal &= ~TIMER_EN_CTRL_MASK;
      devCtrlRegVal |= TIMER_EN_SET_VAL_MASK;
      DevicesCtrlReg_Write(devCtrlRegVal);
      timerState = true;
    }
}

/* Release from reset Signal Generator Timer */
void sigGenTimerDisable()
{
    uint8_t devCtrlRegVal;
    
    if ( timerState == true)
    {
      devCtrlRegVal = DevicesCtrlReg_Read();
      /*Clear corresponding pin */
      devCtrlRegVal &= ~TIMER_EN_CTRL_MASK;
      devCtrlRegVal |= TIMER_EN_CLEAR_VAL_MASK;   
      DevicesCtrlReg_Write(devCtrlRegVal); 
      timerState = false;
    }
}

void sigGenDigitalEnable()
{
  uint8_t devCtrlRegVal;
  devCtrlRegVal = DevicesCtrlReg_Read();
  devCtrlRegVal &= ~SIG_GEN_DIGITAL_EN_CTRL_MASK;
  devCtrlRegVal |= SIG_GEN_DIGITAL_ENABLE_MASK;
  DevicesCtrlReg_Write(devCtrlRegVal); 
}
void sigGenDigitalDisable()
{
  uint8_t devCtrlRegVal;  
  devCtrlRegVal = DevicesCtrlReg_Read();
  devCtrlRegVal &= ~SIG_GEN_DIGITAL_EN_CTRL_MASK;
  devCtrlRegVal |= SIG_GEN_DIGITAL_DISABLE_MASK;
  DevicesCtrlReg_Write(devCtrlRegVal); 

}

/* Convert from sampleRateNs(nano seconds) to timer ticks in 24 bits */
uint32_t sigGenConvertSampleTime2Ticks(uint32_t sampleTimeNs )
{
    uint32_t ticks;
    uint64_t temp;
    /* Limiting the sample time to the maximum sample rate of DAC */
    if (sampleTimeNs < SIGGEN_MINUM_SAMPLE_TIME_SUPPORTED_NS)
    {
       sampleTimeNs = SIGGEN_MINUM_SAMPLE_TIME_SUPPORTED_NS; 
    }
    temp = (uint64_t)sampleTimeNs*TIMER_SIGGEN_CLOCK_FREQ_MHZ;
    ticks = (uint32_t)(temp/1000);
    ticks = ticks - 1;
    
    /* Limiting the maximum number of ticks to the size of the timer */
    if ( ticks > SIGGEN_MAX_TIMER_TICK)
    {
       ticks = SIGGEN_MAX_TIMER_TICK;
    }
    
    return ticks;
}


/* This function reconfigures the timer and DMA to transmit the data.
   timerPeriodTicks - raw value that will be writen in the timer 
   The assuption is that sigGenBuff[SIGGEN_MAX_BUF_SIZE] already
   contains the signal to transmit
*/
int32_t reconfigureSigGenWave(uint32_t timerPeriodTicks, uint16_t numSamples)
{
    int32_t error = ERC_OK;
    if (numSamples > SIGGEN_MAX_BUF_SIZE)
    {
        error = ERC_ERROR;
    }
    else
    {
       sigGenTimerDisable(); /*Disable timer */
       if ( timerPeriodTicks != sigGenTimerPeriodTicks)
       {
          TimerSigGen_WritePeriod(timerPeriodTicks); /* For 64 Mhz don't use something below 15 */
          sigGenTimerPeriodTicks = timerPeriodTicks;
       }
    
       /* DMA Configuration for Signal Generator */
       if (sigGenDmaNumSamples != numSamples)
       {        
         CyDmaChDisable(DMA_SigGen_Chan);         
         CyDmaTdSetConfiguration(DMA_SigGen_TD[0], numSamples, DMA_END_CHAIN_TD,  DMA_SigGen__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR | CY_DMA_TD_AUTO_EXEC_NEXT);
         CyDmaTdSetAddress(DMA_SigGen_TD[0], LO16((uint32)sigGenBuff), LO16((uint32)VDAC8_SigGen_Data_PTR));
         CyDmaChSetInitialTd(DMA_SigGen_Chan, DMA_SigGen_TD[0]);
         CyDmaChEnable(DMA_SigGen_Chan, 1);
        
         sigGenDmaNumSamples = numSamples;
       }
    
       sigGenTimerEnable();
    
    }
    return error;
}

/* This function reconfigures the timer and DMA to transmit the data.
   The waveform information provided as parameter will be used to configure
   timer and DMA.
   timerPeriodTicks - raw value that will be writen in the timer.
   sigType - It's the type of wave to transmit.
   numSamples - It's the number of samples that a wave cycle will contain.
   
*/
int32_t reconfigureSigGenWaveManual(tSigGenWaveType sigType, uint32_t timerPeriodTicks, uint16_t numSamples)
{
    int32_t error = ERC_OK;

    error = fillSignalBufferFromNumSamples(sigType, numSamples, sigGenBuff); 
    
    if (error == OK_RESULT)
    {
       sigGenTimerDisable(); /*Disable timer */
            
       if ( timerPeriodTicks != sigGenTimerPeriodTicks)
       {
          TimerSigGen_WritePeriod(timerPeriodTicks); /* For 64 Mhz don't use something below 15 */
          sigGenTimerPeriodTicks = timerPeriodTicks;
       }
    
       /* DMA Configuration for Signal Generator */
       if (sigGenDmaNumSamples != numSamples)
       {        
         CyDmaChDisable(DMA_SigGen_Chan);         
         CyDmaTdSetConfiguration(DMA_SigGen_TD[0], numSamples, DMA_END_CHAIN_TD,  DMA_SigGen__TD_TERMOUT_EN | CY_DMA_TD_INC_SRC_ADR | CY_DMA_TD_AUTO_EXEC_NEXT);
         CyDmaTdSetAddress(DMA_SigGen_TD[0], LO16((uint32)sigGenBuff), LO16((uint32)VDAC8_SigGen_Data_PTR));
         CyDmaChSetInitialTd(DMA_SigGen_Chan, DMA_SigGen_TD[0]);
         CyDmaChEnable(DMA_SigGen_Chan, 1);
        
         sigGenDmaNumSamples = numSamples;
       }
    
       sigGenTimerEnable();
    
    }
    return error;
}

/* Reconfigure the waveform from sigType and frequency getting the number of
   samples automatically to get the best number.*/
int32_t reconfigureSigGenWaveAuto(tSigGenWaveType sigType, uint32_t freqHz)
{
    int32_t retVal;
    tsigWaveInfo signalInfo;
    uint32_t timerPeriodTicks;
     
    /* Disable timer */
    sigGenTimerDisable();
    sigGenDigitalDisable();
    
    /* If Square Digital signal, then disable analog channel */
    if ( sigType == SIG_GEN_WAVE_SQUARE_DIGITAL)
    {        
      CyDmaChDisable(DMA_SigGen_Chan); 
    }
    
    /* Get signal information */
    retVal = getWaveSamplesForSignal(sigType, freqHz, &signalInfo, sigGenBuff);
   
    if (retVal == OK_RESULT)
    {        
       timerPeriodTicks = sigGenConvertSampleTime2Ticks(signalInfo.sampleTimeNs);
       /*printf("Ticks: %d \n", timerPeriodTicks);*/
       if (sigType == SIG_GEN_WAVE_SQUARE_DIGITAL )
       {
         /* Reconfigure the timer */
         sigGenTimerDisable(); /*Disable timer */
         if ( timerPeriodTicks != sigGenTimerPeriodTicks)
         {
           TimerSigGen_WritePeriod(timerPeriodTicks); /* For 64 Mhz don't use something below 15 */
           sigGenTimerPeriodTicks = timerPeriodTicks;
         }
       }
       else 
       {
         retVal = reconfigureSigGenWave( timerPeriodTicks, signalInfo.numSamples);         
       }
    }
    
    /* Enabling timer */
    sigGenTimerEnable();
    
    if (sigType == SIG_GEN_WAVE_SQUARE_DIGITAL )
    {
      sigGenDigitalEnable();
    }
    
    return retVal;
}


void sigGenDemo()
{
            
    printf("Default Signal \r\n");
    sigGenTimerEnable();  /*Enable signal generator timer */

    
    tSigGenWaveType waveType[] = {SIG_GEN_WAVE_SAW, SIG_GEN_WAVE_INV_SAW, SIG_GEN_WAVE_TRIANGLE, SIG_GEN_WAVE_SINE, SIG_GEN_WAVE_SQUARE, SIG_GEN_WAVE_SQUARE_DIGITAL};
    /*tSigGenWaveType waveType[] = { SIG_GEN_WAVE_SQUARE, SIG_GEN_WAVE_SQUARE_DIGITAL};*/
    /*uint32_t freq[] = {1000, 10000, 40000, 60000, 80000, 100000, 200000};*/
    uint32_t freq[] = {20000, 50000, 100000, 500000};
    
    uint8_t freqIter;
    uint8_t WaveIter;
    uint8_t numFreqItem = sizeof(freq)/sizeof(freq[0]);
    uint8_t numWaveItem = sizeof(waveType)/sizeof(waveType[0]);
    
    pSOCdelyMS(1000);
    
    /* Manual signal genearator for 16 samples, sine wave, sample time 32000 ticks (1 ms ) */
    printf("Manual Waveform \r\n");
    (void)reconfigureSigGenWaveManual(SIG_GEN_WAVE_SINE, 32000, 16);
    pSOCdelyMS(5000);
    
    printf("Automatic Waveform \r\n");
    for (freqIter = 0; freqIter<numFreqItem; freqIter++)
    {
      for (WaveIter=0; WaveIter<numWaveItem; WaveIter++)
      {
        printf("Signal: %d  freq: %d \r\n", waveType[WaveIter], freq[freqIter]);
        (void)reconfigureSigGenWaveAuto(waveType[WaveIter], freq[freqIter]);    
        pSOCdelyMS(3000);
      }
    }
    
}


/* [] END OF FILE */

/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "adcSampleState.h"
#include <stdlib.h>
#include <stdio.h>

static bool calculateSetInfo(void);

static tAdcSampleStateSt adcSampleStateStatus;


bool adcSampleStateInit(tAdcSampleStateCfgInit *cfgIn, tAdcSampleSetInfo *adcSampleSetInfo )
{
    bool status = false;
    uint8_t chnlIdx;
    adcSampleStateStatus.numMaxChannels = cfgIn->numMaxChannels;
    adcSampleStateStatus.numChannelsEnabled = cfgIn->numChannelsEnabled;
    adcSampleStateStatus.sampleTimeNs = cfgIn->sampleTimeNs;
  
    adcSampleStateStatus.windowRefreshTimeNs = cfgIn->windowRefreshTimeNs;  
    adcSampleStateStatus.sampleProcessTimeNs = cfgIn->convertTimePerSampleNs +
                                             cfgIn->plotTimePerSampleNs;
  
    adcSampleStateStatus.timeToPrepareForNextSetNs = cfgIn->timeToPrepareForNextSetNs;
    
    adcSampleStateStatus.pSampleStateBuffer = calloc(adcSampleStateStatus.numMaxChannels,
                                                   sizeof(tAdcSampleStateBuffer));
    
    
    printf("@adcSampleStateInit() Sample State Buffer Size: %d \n", sizeof(tAdcSampleStateBuffer));
    
    if ((adcSampleStateStatus.pSampleStateBuffer != NULL) &&
        (adcSampleStateStatus.numMaxChannels != 0) &&
        (adcSampleStateStatus.numChannelsEnabled != 0))
    {
        adcSampleStateStatus.adcSampleStateInitDone = true;
     
        /* Clear state channel for each one of the channels. Need to be done here
           as the adcSampleClearAllSetStateBuffer expects that adcSampleStateInitDone
           is set to true.
        */
        for(chnlIdx = 0; chnlIdx < adcSampleStateStatus.numMaxChannels; chnlIdx++)
        {
          adcSampleClearAllSetStateBuffer(chnlIdx);
        }
        status = calculateSetInfo();
        *adcSampleSetInfo = adcSampleStateStatus.adcSampleSetInfo;
    }
    return status;
 }

/* Just retrieve the current Set information */
void adcSampleStateGetSetsInfo(  tAdcSampleSetInfo *adcSampleSetInfo  )
{
   adcSampleSetInfo->numSampleSets = adcSampleStateStatus.adcSampleSetInfo.numSampleSets; 
   adcSampleSetInfo->numSamplesPerSet = adcSampleStateStatus.adcSampleSetInfo.numSamplesPerSet; 
}


bool adcSampleStateRecalculateSetInfo(uint8_t newNumChannelsEnabled, uint32_t newSampleTimeNs, tAdcSampleSetInfo *adcSampleSetInfo  )
{
    uint8_t idxChannel;
    uint16_t idxSampleSet;
    bool status = false;
    
    if ((adcSampleStateStatus.adcSampleStateInitDone) && (newNumChannelsEnabled <= adcSampleStateStatus.numMaxChannels))
    {
      /* When this is called, all the Sets states in all channels are set to Invalid */
      for (idxChannel = 0; idxChannel < adcSampleStateStatus.numChannelsEnabled; idxChannel++)
      {
        adcSampleClearAllSetStateBuffer(idxChannel);
      }
    
      /* Now update the value of the status fields with the new information */
      adcSampleStateStatus.numChannelsEnabled = newNumChannelsEnabled;
      adcSampleStateStatus.sampleTimeNs = newSampleTimeNs;
      status = calculateSetInfo();
      *adcSampleSetInfo = adcSampleStateStatus.adcSampleSetInfo;
    }
    
    return status;
}

/* Calculate Number of Sets of samples and number of samples in a Set.
   The assumption here is that the adcSampleStateStatus already include
   the new information. The new group information is update in the
   adcSampleStateStatus status structure.
   If everything is correct it returns true.
*/
bool calculateSetInfo(void)
{
    bool status = true;
    uint32_t limitHighSampleRate;
    uint32_t temp;
    uint8_t index;
    if (adcSampleStateStatus.adcSampleStateInitDone)
    {
        /* If sample time is lower than the time needed to prepare for the next set we will have
           only one set */
        if ( adcSampleStateStatus.sampleTimeNs < adcSampleStateStatus.timeToPrepareForNextSetNs)
        {
            adcSampleStateStatus.adcSampleSetInfo.numSampleSets = 1;
            adcSampleStateStatus.adcSampleSetInfo.numSamplesPerSet = ADC_NUM_SAMPLES_BUFF_SIZE_PER_CHANNEL;  
        }
        else
        {
            /* Calculating the maximum sample time that allow to achieve the goal of refresh
            the screen in the rate desired (windowRefreshTimeNs)
            */
            temp = adcSampleStateStatus.numChannelsEnabled*adcSampleStateStatus.sampleProcessTimeNs;
            limitHighSampleRate = adcSampleStateStatus.windowRefreshTimeNs/ADC_MIN_SIZE_SAMPLE_SET;
            if (limitHighSampleRate > temp )
            {
                limitHighSampleRate -= temp;
                if (adcSampleStateStatus.sampleTimeNs > limitHighSampleRate )
                {
                    adcSampleStateStatus.adcSampleSetInfo.numSamplesPerSet = ADC_MIN_SIZE_SAMPLE_SET;          
                }
                else
                {
                    adcSampleStateStatus.adcSampleSetInfo.numSamplesPerSet =  adcSampleStateStatus.windowRefreshTimeNs /
                                                                          (adcSampleStateStatus.sampleTimeNs + temp );
                    /* Round the number of samples in the set to a 2 power */
                    if (adcSampleStateStatus.adcSampleSetInfo.numSamplesPerSet <= ADC_MIN_SIZE_SAMPLE_SET)
                    {
                        adcSampleStateStatus.adcSampleSetInfo.numSamplesPerSet = ADC_MIN_SIZE_SAMPLE_SET;            
                    }
                    else
                    {
                        index = 0;
                        temp = adcSampleStateStatus.adcSampleSetInfo.numSamplesPerSet;
                        while ( ( temp >> index ) != 1 )
                        {
                            index++;                
                        }
                        adcSampleStateStatus.adcSampleSetInfo.numSamplesPerSet = 1 << index;            
                    }                                                                  
                }
                /* After getting the number of samples per set, calculate the numner of sets */
                adcSampleStateStatus.adcSampleSetInfo.numSampleSets = ADC_NUM_SAMPLES_BUFF_SIZE_PER_CHANNEL/adcSampleStateStatus.adcSampleSetInfo.numSamplesPerSet; 
            }
            else
            {
                status = false;
            }
        }
    }
    else
    {
        status = false;
    }    
  return status;
}



uint8_t getSampleSetState(uint8_t channelNum, uint16_t numSampleSet)
{
    uint16_t idx;
    uint8_t  lowBit;
    uint8_t  state = 0;        
    tAdcSampleStateBuffer *adcSampleSetState = adcSampleStateStatus.pSampleStateBuffer;
    
    if (adcSampleStateStatus.adcSampleStateInitDone)
    {
      idx = numSampleSet/ADC_SAMPLE_SET_STATE_ITEMS_PER_WORD;
      lowBit = (numSampleSet % ADC_SAMPLE_SET_STATE_ITEMS_PER_WORD)*ADC_SAMPLE_SET_STATE_NUM_NUM_BITS;    
      state = (adcSampleSetState[channelNum][idx] >> lowBit) & ADC_SAMPLE_SET_STATE_MASK;
    }
    return state;
}
void setSampleSetState(uint8_t channelNum, uint16_t numSampleSet, uint8_t state)
{
   uint16_t idx;
   uint8_t  lowBit;
   tSampleStateVar stateWord;
   tSampleStateVar stateWordMask;
   tAdcSampleStateBuffer *adcSampleSetState = adcSampleStateStatus.pSampleStateBuffer; 
    
   if (adcSampleStateStatus.adcSampleStateInitDone)
   {
     /* Make sure the state is limited to the mask bits */ 
     state &= ADC_SAMPLE_SET_STATE_MASK; 
     idx = numSampleSet/ADC_SAMPLE_SET_STATE_ITEMS_PER_WORD;
     stateWord = adcSampleSetState[channelNum][idx];
     lowBit = (numSampleSet % ADC_SAMPLE_SET_STATE_ITEMS_PER_WORD)*ADC_SAMPLE_SET_STATE_NUM_NUM_BITS;
   
     /* Getting a mask to clear the bits in current state */
     stateWordMask = ADC_SAMPLE_SET_STATE_MASK << lowBit;
     stateWordMask = ~stateWordMask;
     stateWord = stateWord & stateWordMask;
     /* Changing the state */
     stateWord |= (state << lowBit);
     adcSampleSetState[channelNum][idx] = stateWord;
   }
}

void setSampleSetInvalid(uint8_t channelNum, uint16_t numSampleSet)
{    
    setSampleSetState(channelNum, numSampleSet, ADC_SAMPLE_INVALID_ST);
}

bool  isSampleSetReadyToConvert(uint8_t channelNum, uint16_t numSampleSet)
{
   uint8_t state;
   bool    ret = false;
    
   if (adcSampleStateStatus.adcSampleStateInitDone)
   { 
     state = getSampleSetState(channelNum, numSampleSet);
     ret = (state == ADC_SAMPLE_AQUIRED_ST);
   }

   return ret;
}

/* Check if the set state is in a scpecific state for all of the channels enabled
   The number of set is from 0 to  adcSampleStateStatus.adcSampleSetInfo.numSampleSets -1 */


bool isSampleSetReadyAllChannels( uint16_t numSampleSet, uint8_t readyState)
{
   bool ret = false;
   uint8_t  channelIdx;
   uint16_t idx;
   uint8_t  lowBit;
   uint8_t  state = 0;        
   tAdcSampleStateBuffer *adcSampleSetState = adcSampleStateStatus.pSampleStateBuffer;
    
   if ((adcSampleStateStatus.adcSampleStateInitDone) && 
       (numSampleSet < adcSampleStateStatus.adcSampleSetInfo.numSampleSets))
   {
      ret = true;
      readyState &= ADC_SAMPLE_SET_STATE_MASK; 
      idx = numSampleSet/ADC_SAMPLE_SET_STATE_ITEMS_PER_WORD;
      lowBit = (numSampleSet % ADC_SAMPLE_SET_STATE_ITEMS_PER_WORD)*ADC_SAMPLE_SET_STATE_NUM_NUM_BITS;
      for (channelIdx = 0; channelIdx < adcSampleStateStatus.numChannelsEnabled; channelIdx++)
      {  
        state = (adcSampleSetState[channelIdx][idx] >> lowBit) & ADC_SAMPLE_SET_STATE_MASK;
        if ( state != readyState)
        { 
           ret = false; 
           break;
        }
      }
   }
    
   return ret;
}

bool isAllSetReadyAllChannels( uint8_t readyState )
{
   bool ret; 
   ret = isSampleSetReadyAllChannels( adcSampleStateStatus.adcSampleSetInfo.numSampleSets-1,
                                      readyState);
   return ret;
}


bool  isSampleSetReadyToPlot(uint8_t channelNum, uint16_t numSampleSet)
{
   uint8_t state;
   bool    ret = false;
    
   if (adcSampleStateStatus.adcSampleStateInitDone)
   { 
     state = getSampleSetState(channelNum, numSampleSet);
     ret = (state == ADC_SAMPLE_CONVERT_ST);
   }

   return ret;
}

void adcSampleClearAllSetStateBuffer(uint8_t channelNum)
{
   
   tAdcSampleStateBuffer *adcSampleSetState = adcSampleStateStatus.pSampleStateBuffer; 
   
    /*
   uint16_t index;
   if (adcSampleStateStatus.adcSampleStateInitDone)
   { 
     for (index = 0; index < ADC_SAMPLE_SET_NUM_STATE_WORDS; index++)
     {
        adcSampleSetState[channelNum][index] = 0;
     }
    }
   */
    if (adcSampleStateStatus.adcSampleStateInitDone )
    {
        memset(adcSampleSetState[channelNum], 0, sizeof(tAdcSampleStateBuffer));
    }
}


/* [] END OF FILE */

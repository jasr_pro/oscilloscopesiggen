/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef OSCILLOSCOPE_H

#define OSCILLOSCOPE_H
#include "psocUtilities.h"
#include "adcSampleState.h"
    
    
#define ADC_TIMER_EN_CTRL_NUM_SIG  0
#define ADC_TIMER_EN_CTRL_MASK  (uint8_t)(1 << ADC_TIMER_EN_CTRL_NUM_SIG);
    
#define ADC_TIMER_EN_SET_VAL   1    /* Value that releases from reset */  
#define ADC_TIMER_EN_CLEAR_VAL 0    /* Value that releases from reset */
    
#define ADC_TIMER_EN_SET_VAL_MASK   (uint8_t)(ADC_TIMER_EN_SET_VAL << ADC_TIMER_EN_CTRL_NUM_SIG)
#define ADC_TIMER_EN_CLEAR_VAL_MASK (uint8_t)(ADC_TIMER_EN_CLEAR_VAL << ADC_TIMER_EN_CTRL_NUM_SIG)    
    
#define TIMER_ADC_CLOCK_FREQ 16000000U
#define TIMER_ADC_CLOCK_FREQ_MHZ (TIMER_ADC_CLOCK_FREQ/1000000U)
 
#define OSC_ADC_NUM_CHANNELS 2 
#define ADC_RESOLUTION 8    

#define ADC_NUM_SAMPLES_BUFF_SIZE ADC_NUM_SAMPLES_BUFF_SIZE_PER_CHANNEL    
    
#define OSC_TIMER_RESOLUTION 24
#define OSC_MAX_TIMER_TICK   ((((uint32_t)1) << OSC_TIMER_RESOLUTION) - 1)

/* This value should be recalculated based on the settings in the ADC */   
#define OSC_MINIMUM_SAMPLE_TIME_SUPPORTED_NS 1100  

#define OSC_DEFAULT_SAMPLE_TIME_NS 10000    
#define OSC_NUM_CHANNELS_EN_DEFAULT 2  

/* Time required to configure the sampling of the next set of samples*/
#define OSC_SETUP_TIME_SAMPLE_NEXT_SET_NS 10000
 
/* Time needed to convert the value sampled by ADC to a value that can be ploted in screen */
#define OSC_CONVERT_SAMPLE_TO_PLOT_TIME_NS 2000    

/* Time needed to convert the value sampled by ADC to a value that can be ploted in screen */
#define OSC_PLOT_TIME_PER_SAMPLE_NS 100000
    
/* Time in which the plot screen should be updated (will do the best effort)  */    
#define OSC_PLOT_REFRESH_TIME_NS 300000000
    
#define ADC_CHNL_1_IDX  0
#define ADC_CHNL_2_IDX  1

#if (ADC_RESOLUTION == 8)
  typedef uint8_t tAdcDataType;
#else
  typedef uint16_t tAdcDataType;
#endif
    
typedef struct{    
  tAdcSampleStateCfgInit cfgIn;
  tAdcSampleSetInfo setInfo;
  uint32_t samplePeriodTicks;
  bool adcTimerState;
  tAdcDataType *sampleBuffAddr[OSC_ADC_NUM_CHANNELS];
  uint8  ADC_DMA_Chan[OSC_ADC_NUM_CHANNELS];
  uint8 ADC_DMA_TD[OSC_ADC_NUM_CHANNELS];
  uint16_t setAquiredIdx[OSC_ADC_NUM_CHANNELS];
  uint16_t setConvertIdx[OSC_ADC_NUM_CHANNELS];
  bool allSetsAquired[OSC_ADC_NUM_CHANNELS];
  bool reconfigFlag;   /* Used to abort any sampling process to reconfigure the parameters */
  bool pauseSamplingFlag; /* Used to prevent sampling a new cycle as current samples may be still used */
  bool activateOscFlag;   /* Used to indicate that oscilloscope process is active or not */
}tOscStatus;

void oscilloscopeInit();
void adcTimerEnable();
void oscContinousProcess();

uint8_t getSampleSetState(uint8_t channelNum, uint16_t numSampleSet);
void setSampleSetState(uint8_t channelNum, uint16_t numSampleSet, uint8_t state);
void setSampleSetInvalid(uint8_t channelNum, uint16_t numSampleSet);
bool  isSampleSetReadyToPlot(uint8_t channelNum, uint16_t numSampleSet);
void activateOscProcess();
void deactivateOscProcess();

#endif    


/* [] END OF FILE */

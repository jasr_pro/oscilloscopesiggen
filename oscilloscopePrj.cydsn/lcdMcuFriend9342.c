/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "lcdMcuFriend9342.h"
#include "psocUtilities.h"
#include "lcdMcuFriend9342Wrap.h"
#include "displayFont5x7.h"
#include <stdio.h>
#include <stdlib.h>

#define LCD_CAPABILITIES (AUTO_READINC | MIPI_DCS_REV1 | MV_AXIS | READ_24BITS | INVERT_GS | REV_SCREEN)

 uint16_t _lcd_capable = LCD_CAPABILITIES;
 uint16_t  _lcd_xor = 0;
 uint16_t _lcd_rev = (( (LCD_CAPABILITIES) & REV_SCREEN) != 0);
 uint16_t _MC, _MP, _MW, _SC, _EC, _SP, _EP;
 uint16_t _lcd_madctl;

 int16_t _width;       ///< Display width as modified by current rotation
 int16_t _height;
 int16_t cursor_x = 1;
 int16_t cursor_y = 1;
 uint16_t textcolor = COLOR_WHITE;
 uint16_t textbgcolor = COLOR_BLACK;
 uint8_t textsize = 1;

static const uint8_t ILI9342_regValues_CPT24[] = {     /*CPT 2.4" */
            (0xB9), 3, 0xFF, 0x93, 0x42, /*[00 00 00]  Set EXTC */
            (0xC0), 2, 0x1D, 0x0A,    /*[26 09] Power Control 1 */
            (0xC1), 1, 0x02,          /*[10] Power Control 2 */
            (0xC5), 2, 0x2F, 0x2F,    /*[31 3C] VCOM Control 1 */
            (0xC7), 1, 0xC3,          /*[C0] VCOM Control 2 */
            (0xB8), 1, 0x0B,          /*[07] Oscillator Control */
            (0xE0), 15, 0x0F, 0x33, 0x30, 0x0C, 0x0F, 0x08, 0x5D, 0x66, 0x4A, 0x07, 0x13, 0x05, 0x1B, 0x0E, 0x08, /* Positive Gamma Correction */
            (0xE1), 15, 0x08, 0x0E, 0x11, 0x02, 0x0E, 0x02, 0x24, 0x33, 0x37, 0x03, 0x0A, 0x09, 0x26, 0x33, 0x0F, /* Negative Gamma Correction */
};
        

#if 0
static const uint8_t ILI9342_regValues_Tianma23[] = {     /*Tianma 2.3"*/
            (0xB9), 3, 0xFF, 0x93, 0x42, /* Set EXTC */
            (0xC0), 2, 0x1D, 0x0A,  /* Power Control 1 */
            (0xC1), 1, 0x01,        /* Power Control 2 */
            (0xC5), 2, 0x2C, 0x2C,  /*VCOM Control 1 */
            (0xC7), 1, 0xC6,        /*VCOM Control 2 */
            (0xB8), 1, 0x09,        /* Oscillator Control */ 
            (0xE0), 15, 0x0F, 0x26, 0x21, 0x07, 0x0A, 0x03, 0x4E, 0x62, 0x3E, 0x0B, 0x11, 0x00, 0x08, 0x02, 0x00,  /* Positive Gamma Correction */
            (0xE1), 15, 0x00, 0x19, 0x1E, 0x03, 0x0E, 0x03, 0x30, 0x23, 0x41, 0x03, 0x0B, 0x07, 0x2F, 0x36, 0x0F,  /* Negative Gamma Correction */
};

static const uint8_t ILI9342_regValues_HSD23[] = {     //HSD 2.3"
            (0xB9), 3, 0xFF, 0x93, 0x42,  /* Set EXTC */
            (0xC0), 2, 0x1D, 0x0A,  /* Power Control 1 */ 
            (0xC1), 1, 0x02,        /* Power Control 2 */ 
            (0xC5), 2, 0x2F, 0x27,  /* VCOM Control 1 */
            (0xC7), 1, 0xA4,        /* VCOM Control 2 */
            (0xB8), 1, 0x0B,        /* Oscillator Control */ 
            (0xE0), 15, 0x0F, 0x24, 0x21, 0x0C, 0x0F, 0x06, 0x50, 0x75, 0x3F, 0x07, 0x12, 0x05, 0x11, 0x0B, 0x08, /* Positive Gamma Correction */
            (0xE1), 15, 0x08, 0x1D, 0x20, 0x02, 0x0E, 0x04, 0x31, 0x24, 0x42, 0x03, 0x0B, 0x09, 0x30, 0x36, 0x0F, /* Negative Gamma Correction */
        };
#endif        
static const uint8_t reset_off[] = {
            0x01, 0,            //Soft Reset
            TFTLCD_DELAY8, 150,  // .kbv will power up with ONLY reset, sleep out, display on
            0x28, 0,            //Display Off
            0x3A, 1, 0x55,      //Pixel read=565, write=565.
        };
        
static const uint8_t wake_on[] = {
            0x11, 0,            //Sleep Out
            TFTLCD_DELAY8, 150,
            0x29, 0,            //Display On
        };

void _swap_int16_t(int16_t *val1, int16_t *val2);
void writeLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color);

void lcdMcuFriend9342Start()
{
  lcdReset();
  
  uint32_t regVal = lcdReadRegister32(0xD3);
  printf("@lcdInitTop() - ID: 0x%lx \r\n", regVal);

  lcdInit();

  
}

/* Reset the LCD */
void lcdReset(void)
{  
  /* Configura data ports as output */
  lcdSetDataPortWrite();
  
  
  /* Set all control signals as IDLE */
  CS_SET
  READ_SET
  WRITE_SET
  RESET_SET 
  DATA_CMD_SET
  
  /* Applying and removing reset */
  pSOCdelyMS(50);
  RESET_CLEAR;
  pSOCdelyMS(100);
  RESET_SET 
  pSOCdelyMS(100);

   /* After reset the orientation will match*/
   _width = LCD_WIDTH;   
   _height = LCD_HEIGHT;
    
}


/* Simply read 8 bits from the data port */


/* Send 1 byte to port and triggering the write. 
   It's assumed that the port is writeable 
   Assumed that display is seleccted.
*/
void lcdWrite8ToPort(uint8_t val)
{
   lcdWriteDataToPort( val );
   WRITE_CLEAR  /* PWLW=TWRL=50ns */
   WRITE_SET   
}

/* Read 8 bit from port. It controls the RD signal to triger the read. 
   It`s assumed that port is in read mode. 
   Assumed that display is seleccted.
*/
uint8_t lcdRead8FromPort(void)
{
  uint8_t retVal;

  /* PWLR=TRDL=150ns, tDDR=100ns */
  READ_SET
  READ_CLEAR
  READ_CLEAR
  READ_CLEAR
  retVal = lcdReadDataFromPort();
  READ_SET

  return retVal;  
}

/* Send 2 byte to port with the write triger mechanism. 
   The High part is sent first. 
   It's assumed that the port is writeable 
   Assumed that display is seleccted.
*/
void lcdWrite16ToPort(uint16_t val)
{
  uint8_t valH;
  uint8_t valL;
  valH = val >> 8;
  valL = val & 0xFF;
  lcdWrite8ToPort(valH);
  lcdWrite8ToPort(valL);
}


/*
 Read 16 bit from Data port. High part is read first.
 It's assumed that port is already in Read mode.
 It's assumed that display is already selected.
*/
uint16_t lcdRead16bitsFromPort(void)
{
  uint16_t retVal;
  uint8_t  val8;
  
  /* Wait */
  pSOCdelayUS(1);  /* Original delay 1 ms */
  val8 = lcdRead8FromPort();
  retVal = val8 << 8;

  pSOCdelayUS(1);  /* Original delay 1 ms */
  val8 = lcdRead8FromPort();
  retVal |= val8;
  
  return retVal;
}

/* Send a command (16 bit). This control the data/command signal. 
   It's assumed that port is already in write mode.
*/
void lcdSendCommand(uint16_t command)
{     
  DATA_CMD_CLEAR          /* Select command */
  lcdWrite16ToPort(command); /* Send command */
  DATA_CMD_SET           /* Select data */   
}

/* Send a data command */
void writeCommandData( uint16_t command, uint16_t data)
{
  CS_CLEAR
  /* Sending 16 bit command */
  lcdSendCommand(command);
  /*Sending 16  bit data */
  lcdWrite16ToPort(data);
  CS_SET
}

/* Send a 16 bit command followed by a list o N 8bit parameters */
void WriteCmdParamN(uint16_t cmd, int8_t N, uint8_t *block)
{
  uint8_t u8; 
  
  CS_CLEAR
  lcdSendCommand(cmd);
  while (N-- > 0)
  {
    u8 = *block++;
    lcdWrite8ToPort(u8);
  }
  CS_SET;
}

/* Send a command (16 bit, high part is 0 followedd by 4 byte parameters */
void WriteCmdParam4(uint8_t cmd, uint8_t d1, uint8_t d2, uint8_t d3, uint8_t d4)
{
    uint8_t d[4];
    d[0] = d1, d[1] = d2, d[2] = d3, d[3] = d4;
    WriteCmdParamN(cmd, 4, d);
}

/* Read a register (16 bits). It selects the display.
   Assumed that port is in write mode.
*/
uint16_t lcdReadRegister( uint16_t reg, int8_t index)
{
  uint16_t regOut;

  /* Select the current LCD */
  CS_CLEAR 
  
  /* Send the command (register to be accessed) */
  lcdSendCommand(reg);

  /* Switch port to read */
  lcdSetDataPortRead();
  pSOCdelayUS(1);    /* Original delay 1ms */

  do {
    regOut = lcdRead16bitsFromPort(); 
  }while (--index >= 0);  

  /* Retrun read singal ti idle. Unselect this chip */
  READ_SET
  CS_SET

  /* Switch data port to Write*/
  lcdSetDataPortWrite();
  
  return regOut;
}

/*------------------------------ Up to here it's hardware dependent ---------------------------*/

/* Read 32 bit data from the register. */
uint32_t lcdReadRegister32( uint16_t reg)
{
  uint16_t regH;
  uint16_t regL;
  uint32_t regOut;

  regH = lcdReadRegister( reg, 0);  
  regL = lcdReadRegister( reg, 1);  
  regOut = regL | (((uint32_t)regH) << 16) ;
  return regOut;
}

static void init_table(const void *table, int16_t size)
{
  uint8_t *p = (uint8_t *) table;
  uint8_t cmd, len;
  uint8_t iter, data;
  while (size > 0) 
  {
     cmd = readByteFromFlash(p); p++; /* Read from Flash 1st data command */
     len = readByteFromFlash(p); p++; /* Read from Flash 2nd data length */
     /* If the command is pSOCdelyMS, then length is the duration */
     if (cmd == TFTLCD_DELAY8) 
     {
        pSOCdelyMS(len);
        len = 0;  /* len is set to 0 as size will be updated later using this parameter */
     } else 
     {
       /* If not, this is a command to send. Select LCD */
        CS_CLEAR
        /* Indicate this is a command */
        DATA_CMD_CLEAR
        
        /* Send 8 bit command */
        lcdWrite8ToPort(cmd);

        /* Send the data */
        for (iter = 0; iter++ < len; ) 
        {
          data = readByteFromFlash(p); p++;
          /* Select Data */
          DATA_CMD_SET
          lcdWrite8ToPort(data);   
        }
        CS_SET;
      }
      size -= len + 2;  /* The new size is obtained substracting the number of data, length and command */
  }
}


/* Initialize the LCD */
void lcdInit(void)
{
   //int16_t *p16;               /*so we can "write" to protected variable HEIGHT and WIDTH from Adafruit_GFX.*/
   const uint8_t *table8_ads = ILI9342_regValues_CPT24;
   int16_t table_size = sizeof(ILI9342_regValues_CPT24);

#if  0
   /* Update the HEIGHT and WIDTH from Adafruit_GFX class */
   p16 = (int16_t *)&HEIGHT;
   *p16 = LCD_HEIGHT;
   p16 = (int16_t *) &WIDTH;
   *p16 = LCD_WIDTH;
#endif
   /* Reset the LCD */
   lcdReset();
   
   /* Initialize the LCD with the configuration in the table */
   init_table(reset_off, sizeof(reset_off));
   init_table(table8_ads, table_size);
   init_table(wake_on, sizeof(wake_on));

   /* Set default rotation */
   setRotation(0); 
   /* No Invert display*/
   invertDisplay(false);
  
}

/* Set the size of the Window in the LCD (with column and page) */
void setAddrWindow(int16_t x, int16_t y, int16_t x1, int16_t y1)
{
  WriteCmdParam4(_SC, x >> 8, x, x1 >> 8, x1);   //Start column instead of _MC
  WriteCmdParam4(_SP, y >> 8, y, y1 >> 8, y1); 
}

/* Configure Vertical Scroll */
void vertScroll(int16_t top, int16_t scrollines, int16_t offset)
{
  int16_t bfa = LCD_HEIGHT - top - scrollines;  // bottom fixed area
  int16_t vsp;
  int16_t sea = top;
  uint8_t d[6]; /* for multi-byte parameters */

  if ((offset <= -scrollines) || (offset >= scrollines))
  {
    offset = 0; /* define a valid offset */
  }
  vsp = top + offset; /*vertical start position */
  
  if (offset < 0)
  {
    vsp += scrollines;          //keep in unsigned range
  }
  
  sea = top + scrollines - 1;
  if (_lcd_capable & MIPI_DCS_REV1) 
  {
    d[0] = top >> 8;        /* TFA */
    d[1] = top;
    d[2] = scrollines >> 8; /* VSA */
    d[3] = scrollines;
    d[4] = bfa >> 8;        /* BFA */
    d[5] = bfa;
    WriteCmdParamN(0x33, 6, d); /* Vertical Scrolling Definition register */

    d[0] = vsp >> 8;        /* VSP */
    d[1] = vsp; 
    WriteCmdParamN(0x37, 2, d);  /* Vertical Scrolling Start Address register */
    
    if ((offset == 0) && (_lcd_capable & MIPI_DCS_REV1))
    {
      WriteCmdParamN(0x13, 0, NULL);    /*NORMAL i.e. disable scroll*/
    }    
  }                
}

/* Invert display */
void invertDisplay(bool i)
{
  
  _lcd_rev = ((_lcd_capable & REV_SCREEN) != 0) ^ i; /* Invert the state if parameter is true and iversion is supported */

  if (_lcd_capable & MIPI_DCS_REV1) 
  {
    /* Reg 0x20 Display Inversion OFF */
    /* Reg 0x21 Display Inversion ON */
    WriteCmdParamN(_lcd_rev ? 0x21 : 0x20, 0, NULL); 
  }    
}

/* Rotate display */
void setRotation(uint8_t r)
{
    
    uint8_t val;
    uint8_t rotLocal = r & 3;
    uint16_t x;
    
    //rotation = r & 3; /* rotation in gfx */
    _width = (rotLocal & 1) ? LCD_HEIGHT : LCD_WIDTH;
    _height = (rotLocal & 1) ? LCD_WIDTH : LCD_HEIGHT;

     switch (rotLocal)
     {
       case 0:                    //PORTRAIT:
         val = 0x48;             //MY=0, MX=1, MV=0, ML=0, BGR=1
         break;
       case 1:                    //LANDSCAPE: 90 degrees
         val = 0x28;             //MY=0, MX=0, MV=1, ML=0, BGR=1
         break;
       case 2:                    //PORTRAIT_REV: 180 degrees
         val = 0x98;             //MY=1, MX=0, MV=0, ML=1, BGR=1
          break;
       case 3:                    //LANDSCAPE_REV: 270 degrees
          val = 0xF8;             //MY=1, MX=1, MV=1, ML=1, BGR=1
        break;
    }

    if (_lcd_capable & INVERT_GS)
    {
      val ^= 0x80;
    }

    /* Column Addres Set reg _SC(0x2A) - Page Address Set reg _SP(0x2B)  -  Memory Write Reg _MW(0x2C)  */
    _MC = 0x2A, _MP = 0x2B, _MW = 0x2C, _SC = 0x2A, _EC = 0x2A, _SP = 0x2B, _EP = 0x2B;
    WriteCmdParamN( 0x36, 1, &val);  /* Memory access control */
    _lcd_madctl = val;

    if ((rotLocal & 1) && ((_lcd_capable & MV_AXIS) == 0))
    {    
      x = _MC, _MC = _MP, _MP = x;
      x = _SC, _SC = _SP, _SP = x;    //.kbv check 0139
      x = _EC, _EC = _EP, _EP = x;    //.kbv check 0139
    }
    setAddrWindow(0, 0, _width - 1, _height - 1);
    vertScroll(0, LCD_HEIGHT, 0);   //reset scrolling after a rotation

}

void drawPixel(int16_t x, int16_t y, uint16_t color)
{
  /* MCUFRIEND just plots at edge if you try to write outside of the box */
  if (!((x < 0) || (y < 0) || (x >= _width) || (y >= _height)))
  {
    setAddrWindow(x, y, x, y);
    writeCommandData(_MW, color);  /* Write to Memory data*/
  }
}

/* Fill a rectangle based on the top left coordenate and the width and heigth*/
void drawRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color)
{
    int16_t xM = x+w-1;
    int16_t yM = y+h-1;
    drawLine(x, y, xM, y, color);
    drawLine(x, yM, xM, yM, color);
    drawLine(x, y, x, yM, color);
    drawLine(xM, y, xM, yM, color);    
}

/* Fill a rectangle based on the top left coordenate and the width and heigth*/
void fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color)
{
  int16_t end;
  uint8_t hi, lo;
  
  /* If the width is negative */
  if (w < 0) 
  {
        w = -w;
        x -= w;
  }                           //+ve w
  end = x + w;
  /* Limit the x coordenate to have a minimum value of 0 */
  if (x < 0)
  {
     x = 0;
  }

  /* Limit the final end coordinate to be less than the width od the screen */
  if (end > _width )
  {
     end = _width;
  }  
  /* new width */
  w = end - x;

  /* If height is negative*/
  if (h < 0) 
  {
    h = -h;
    y -= h;
  }                           //+ve h
  end = y + h;
  /* The minmum y coordentat can be 0 */
  if (y < 0)
  {
    y = 0;
  }
  /* Limiting the bottom coordenate to the height of the LCD */
  if (end >_height)
  {
    end = _height;
  }
  /* Getting the new height */
  h = end - y;

  /* Define address window */
  setAddrWindow(x, y, x + w - 1, y + h - 1);

  /* Fill the rectangle taking advantage of sending one command followed by only Write strobe taking advantage of auto increment of coordenate */
  CS_CLEAR
  lcdSendCommand(_MW); /* Memory Write */

  /* Getting what is larger width or height. Width will get the larger */
  if (h > w)
  {
    end = h;
    h = w;
    w = end;
  }

  /* Color high and color low */
  hi = color >> 8;
  lo = color & 0xFF;
  while (h-- > 0) 
  {
    end = w;
    do 
    {
      lcdWrite8ToPort(hi);
      lcdWrite8ToPort(lo);
    }while (--end != 0);
  }
  CS_SET;

  /* Restore current window */
  if (!(_lcd_capable & MIPI_DCS_REV1) )
  {
    setAddrWindow(0, 0, _width - 1, _height - 1);
  }
}

/* w(Width) is the number of pixels of the line (including first coordenate)*/
void drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color) 
{ 
  fillRect(x, y, 1, h, color); 
}
/* w(Width) is the number of pixels of the line (including first coordenate)*/
void drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color) 
{ 
  fillRect(x, y, w, 1, color); 
}

void  fillScreen(uint16_t color) 
{ 
  fillRect(0, 0, _width, _height, color);
}

void drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color)
{
    if (x0 == x1) 
    {
      if (y0 > y1)
      {
       _swap_int16_t(&y0, &y1);
      }
      drawFastVLine(x0, y0, y1 - y0 + 1, color);
    }
    else if (y0 == y1) 
    {
      if (x0 > x1)
      {
        _swap_int16_t(&x0, &x1);
      }
      drawFastHLine(x0, y0, x1 - x0 + 1, color);
    } else 
    {
      writeLine(x0, y0, x1, y1, color);    
    }
}

/**************************************************************************/
/*!
   @brief    Write a line.  Bresenham's algorithm - thx wikpedia
    @param    x0  Start point x coordinate
    @param    y0  Start point y coordinate
    @param    x1  End point x coordinate
    @param    y1  End point y coordinate
    @param    color 16-bit 5-6-5 Color to draw with
*/
/**************************************************************************/
void writeLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1,
                             uint16_t color) {

  int16_t dx, dy;
  int16_t err;
  int16_t ystep;
  int16_t steep = abs(y1 - y0) > abs(x1 - x0);

  if (steep) 
  {
    _swap_int16_t(&x0, &y0);
    _swap_int16_t(&x1, &y1);
  }

  if (x0 > x1) 
  {
    _swap_int16_t(&x0, &x1);
    _swap_int16_t(&y0, &y1);
  }
  
  dx = x1 - x0;
  dy = abs(y1 - y0);
  err = dx / 2;
  
  if (y0 < y1) 
  {
    ystep = 1;
  } 
  else 
  {
    ystep = -1;
  }

  for (; x0 <= x1; x0++) 
  {
    if (steep) 
    {
      drawPixel(y0, x0, color);
    } 
    else 
    {
      drawPixel(x0, y0, color);
    }
    err -= dy;
    if (err < 0) 
    {
      y0 += ystep;
      err += dx;
    }
  }
}
                            
void _swap_int16_t(int16_t *val1, int16_t *val2){    
   int16_t temp;
    
   temp = *val1;
   *val1 =  *val2;
   *val2 = temp;
}

/* Set cursor used to write text */
void displaySetTextCursor(int16_t x, int16_t y) 
{
  cursor_x = x;
  cursor_y = y;
}

/* Get current cursor used to write text */
void displayGetTextCursor(int16_t *x, int16_t *y) 
{
  *x = cursor_x;
  *y = cursor_y;
}

/* Set Text Color */
void displaySetTextSize(uint8_t s) 
{
  textsize = (s > 0) ? s : 1;
}

/* Set the color of the text and background */
void displaySetTextColor(uint16_t c, uint16_t b) 
{
   textcolor = c;
   textbgcolor = b; 
}

/* Draw a character setting the color, background color and size */
void displayDrawChar(int16_t x, int16_t y, unsigned char c,
			  uint16_t color, uint16_t bg, uint8_t size) 
{
  bool cont = true;
  uint8_t line;
  int8_t i,j;
    
   if((x >= _width)		|| // Clip right
      (y >= _height)		|| // Clip bottom
      ((x + SIZE_OF_LETTER_X_PLUS_1 * size - 1) < 0)	|| // Clip left
      ((y + SIZE_OF_LETTER_Y_PLUS_1 * size - 1) < 0))     // Clip top
   {
     cont = false;
   }

   if (cont)
   {
     for (i=0; i<SIZE_OF_LETTER_X_PLUS_1; i++ )
     {    
       if (i == SIZE_OF_LETTER_X)
       {
         line = 0x0;
       }
       else
       {
         line = font[(c*SIZE_OF_LETTER_X)+i];
       }
    
       for (j=0; j<SIZE_OF_LETTER_Y_PLUS_1; j++) 
       {
         if (line & 0x1) 
         {
           if (size == 1) /* default size */
           {
             drawPixel(x+i, y+j, color);
           }
           else  /* big size */
           {            
             fillRect(x+(i*size), y+(j*size), size, size, color);
           }
         }  
         else if (bg != color)
         {
           if (size == 1) /* default size */
           {
             drawPixel(x+i, y+j, bg);
           }
           else
           { /* big size */
             fillRect(x+i*size, y+j*size, size, size, bg);
           } 	
         }
         line >>= 1;
       }
     }
   }
}

/* Display a character using the global parameters */
void displayWrite(uint8_t c)
{

  if (c == '\n') 
  {
    cursor_y += textsize*(SIZE_OF_LETTER_Y_PLUS_1);
    cursor_x = 0;
  } else if (c == '\r')
  {
     /* skip em */
  } 
  else 
  {
    displayDrawChar(cursor_x, cursor_y, c, textcolor, textbgcolor, textsize);
    cursor_x += textsize*(SIZE_OF_LETTER_X_PLUS_1);    
  }
}

/* Display a string of chars using global parameters */
void displayPrint(const uint8_t *text)
{
   uint8_t idx = 0;
   while(text[idx] != '\0' )
   {
     displayWrite((uint8_t)text[idx]);
     idx++; 
   }
      
}

/* [] END OF FILE */

/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef LCD_MCU_FRIEND_9342_H
            
#define LCD_MCU_FRIEND_9342_H

#include "psocUtilities.h"
    
#define LCD_HEIGHT 240
#define LCD_WIDTH  320

#define TFTLCD_DELAY 0xFFFFU
#define TFTLCD_DELAY8 0x7FU


#define readByteFromFlash(x) (*x);

#define MIPI_DCS_REV1   (1<<0)
#define AUTO_READINC    (1<<1)
#define READ_BGR        (1<<2)
#define READ_LOWHIGH    (1<<3)
#define READ_24BITS     (1<<4)
#define XSA_XEA_16BIT   (1<<5)
#define READ_NODUMMY    (1<<6)
#define INVERT_GS       (1<<8)
#define INVERT_SS       (1<<9)
#define MV_AXIS         (1<<10)
#define INVERT_RGB      (1<<11)
#define REV_SCREEN      (1<<12)
#define FLIP_VERT       (1<<13)
#define FLIP_HORIZ      (1<<14)
    
#define color565RedMask(r)    ((((uint16_t)(r)) & 0x1F) << 11)
#define color565GreenMask(g)  ((((uint16_t)(g)) & 0x3F) << 5 )
#define color565BlueMask(b)   (((uint16_t)(b)) & 0x1F)  
#define color565(r,g,b) (color565RedMask(r) | color565GreenMask(g) | color565BlueMask(b)) 
 
#define COLOR_BLACK   color565(0x0,0x0,0x0)    
#define COLOR_YELLOW  color565(0x1,0x1f,0x0)
#define COLOR_WHITE   color565(0x1f,0x3f,0x1F)
#define COLOR_BLUE   color565(0x0,0x0,0x1F)
#define COLOR_RED   color565(0x1F,0x0,0x0)
#define COLOR_GREEN   color565(0x0,0x1F,0x0)    
    
void lcdReset(void);
void lcdSetDataPortRead(void);
void lcdSetDataPortWrite(void);
void lcdWriteDataToPort(uint8_t data);
uint8_t lcdReadDataFromPort(void);
void lcdWrite8ToPort(uint8_t val);
uint8_t lcdRead8FromPort(void);
void lcdWrite16ToPort(uint16_t val);
uint16_t lcdRead16bitsFromPort(void);
void lcdSendCommand(uint16_t command);
void WriteCmdParamN(uint16_t cmd, int8_t N, uint8_t *block);
void WriteCmdParam4(uint8_t cmd, uint8_t d1, uint8_t d2, uint8_t d3, uint8_t d4);
uint16_t lcdReadRegister( uint16_t reg, int8_t index);
uint32_t lcdReadRegister32( uint16_t reg);
void lcdMcuFriend9342Start();
void lcdInit(void);
void setAddrWindow(int16_t x, int16_t y, int16_t x1, int16_t y1);
void vertScroll(int16_t top, int16_t scrollines, int16_t offset);
void setRotation(uint8_t r);
void invertDisplay(bool i);
void drawPixel(int16_t x, int16_t y, uint16_t color);
void drawRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
void fillRect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);
void drawFastVLine(int16_t x, int16_t y, int16_t h, uint16_t color);
void drawFastHLine(int16_t x, int16_t y, int16_t w, uint16_t color);
void fillScreen(uint16_t color);
void drawLine(int16_t x0, int16_t y0, int16_t x1, int16_t y1, uint16_t color);
void displaySetTextCursor(int16_t x, int16_t y) ;
void displayGetTextCursor(int16_t *x, int16_t *y);
void displaySetTextSize(uint8_t s);
void displaySetTextColor(uint16_t c, uint16_t b);
void displayDrawChar(int16_t x, int16_t y, unsigned char c, uint16_t color, uint16_t bg, uint8_t size);
void displayWrite(uint8_t c);
void displayPrint(const uint8_t *text);


#endif
/* [] END OF FILE */

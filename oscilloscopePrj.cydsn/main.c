/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <stdio.h>
#include "project.h"
#include "common.h"
#include "psocUtilities.h"
#include "signalGen.h"
#include "projectDisplay.h"
#include "rotEncoder.h"

#include "plotWave.h"
#include "lcdMcuFriend9342.h"
#include "projectApp.h"

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    /* Enabling UART */
    UART_1_Start();
    UART_1_PutString("UART Full Duplex and printf() support Code Example Project \r\n");
            
    /* ------------ Application code -------------*/
   
    prjAppInit();

   
    prjAppEntryPoint();
    

    /* ------------ To here -------------*/
#if 0
    drawBackPlane();
    int16_t cursorX, cursorY;
    uint8_t num;
    uint8_t prevNum;
    char numStr[2];
    
    
    cursorX = 50;
    cursorY = 50;
    displaySetTextCursor(cursorX, cursorY);
    displaySetTextSize(3);
    displaySetTextColor(COLOR_YELLOW, COLOR_BLACK);        
    displayPrint("Frequency: ");
    /* get the cursors after the last print */
    displayGetTextCursor(&cursorX, &cursorY);
    
    
    
    num = 0;
    prevNum = num;
    sprintf(numStr,"%d", num);
    displayPrint(numStr);
    
    for(;;)
    {
       if (isSetRotEncoderRightFlag())
       {
          num = (num+1)%10;
          clearEncodedStatus(ROT_ENC_TURN_RIGHT_FLAG_MASK);
       }
       if (isSetRotEncoderLeftFlag())
       {
          num = (num == 0) ? 9 : num - 1;
          clearEncodedStatus(ROT_ENC_TURN_LEFT_FLAG_MASK);
       }
       if (num != prevNum)
       {
         displaySetTextCursor(cursorX, cursorY);
         sprintf(numStr,"%d", num);
         displayPrint(numStr);
         prevNum = num;         
       }
    
        pSOCdelyMS(200);
    }
    #endif
}

/* [] END OF FILE */

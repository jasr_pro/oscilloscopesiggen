/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "lcdMcuFriend9342Wrap.h"
#include "LCD_DATA.h"

/* Configure Data port as input */
void lcdSetDataPortRead(void)
{
  LCD_DATA_SetDriveMode(LCD_DATA_DM_DIG_HIZ);  
    
}

/* Configure Data port as output */
void lcdSetDataPortWrite(void)
{
  LCD_DATA_SetDriveMode(LCD_DATA_DM_STRONG); 
}


void lcdWriteDataToPort(uint8_t dataVal)
{
    LCD_DATA_Write(dataVal);
}

uint8_t lcdReadDataFromPort(void) 
{
   uint8_t dataVal;    
    
   dataVal = LCD_DATA_Read();

   return dataVal; 
}



/* [] END OF FILE */

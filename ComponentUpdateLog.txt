Last Modified Date & Time: 09/17/2021 19:05:55

oscilloscopePrj: oscilloscopePrj.cydwr
	cy_boot [v6.0] to [v6.10]

oscilloscopePrj: TopDesign.cysch
	adc1_dma_complete [v1.70] to [v1.71]
	ADC_complete [v1.70] to [v1.71]
	ROT_ENC_ISR_A [v1.70] to [v1.71]
	ROT_ENC_ISR_B [v1.70] to [v1.71]
	ROT_ENC_ISR_SW [v1.70] to [v1.71]
	TimerISR_1 [v1.70] to [v1.71]


